<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text logo-mini">{{ __('BD') }}</a>
            <a href="#" class="simple-text logo-normal">{{ __('CUBIQUE Solutions') }}</a>
        </div>
        {{-- <ul class="nav">
            <li @if ($pageSlug == 'dashboard') class="active " @endif>
                <a href="{{ route('home') }}">
        <i class="tim-icons icon-chart-pie-36"></i>
        <p>{{ __('Dashboard') }}</p>
        </a>
        </li>
        <li>
            <a data-toggle="collapse" href="#laravel-examples" aria-expanded="true">
                <i class="fab fa-laravel"></i>
                <span class="nav-link-text">{{ __('Laravel Examples') }}</span>
                <b class="caret mt-1"></b>
            </a>

            <div class="collapse show" id="laravel-examples">
                <ul class="nav pl-4">
                    <li @if ($pageSlug=='profile' ) class="active " @endif>
                        <a href="{{ route('profile.edit')  }}">
                            <i class="tim-icons icon-single-02"></i>
                            <p>{{ __('User Profile') }}</p>
                        </a>
                    </li>
                    <li @if ($pageSlug=='users' ) class="active " @endif>
                        <a href="{{ route('user.index')  }}">
                            <i class="tim-icons icon-bullet-list-67"></i>
                            <p>{{ __('User Management') }}</p>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li @if ($pageSlug=='icons' ) class="active " @endif>
            <a href="{{ route('pages.icons') }}">
                <i class="tim-icons icon-atom"></i>
                <p>{{ __('Icons') }}</p>
            </a>
        </li>
        <li @if ($pageSlug=='maps' ) class="active " @endif>
            <a href="{{ route('pages.maps') }}">
                <i class="tim-icons icon-pin"></i>
                <p>{{ __('Maps') }}</p>
            </a>
        </li>
        <li @if ($pageSlug=='notifications' ) class="active " @endif>
            <a href="{{ route('pages.notifications') }}">
                <i class="tim-icons icon-bell-55"></i>
                <p>{{ __('Notifications') }}</p>
            </a>
        </li>
        <li @if ($pageSlug=='tables' ) class="active " @endif>
            <a href="{{ route('pages.tables') }}">
                <i class="tim-icons icon-puzzle-10"></i>
                <p>{{ __('Table List') }}</p>
            </a>
        </li>
        <li @if ($pageSlug=='typography' ) class="active " @endif>
            <a href="{{ route('pages.typography') }}">
                <i class="tim-icons icon-align-center"></i>
                <p>{{ __('Typography') }}</p>
            </a>
        </li>
        <li @if ($pageSlug=='rtl' ) class="active " @endif>
            <a href="{{ route('pages.rtl') }}">
                <i class="tim-icons icon-world"></i>
                <p>{{ __('RTL Support') }}</p>
            </a>
        </li>
        <li class=" {{ $pageSlug == 'upgrade' ? 'active' : '' }}">
            <a href="{{ route('pages.upgrade') }}">
                <i class="tim-icons icon-spaceship"></i>
                <p>{{ __('Upgrade to PRO') }}</p>
            </a>
        </li>
        </ul> --}}



        {{-- ***************************************** --}}

        <ul class="nav">
            {{-- <li @if ($pageSlug == 'Home') class="active " @endif>
                <a href="{{ route('home') }}">
            <i class="tim-icons icon-chart-pie-36"></i>
            <p>{{ __('Dashboard') }}</p>
            </a>
            </li> --}}

            {{-- CLIENTES --}}

            {{-- <li @if ($pageSlug=='particularCustomers' ) class="active " @endif>
                <a href="{{ route('pages.particularCustomers') }}">
            <i class="fas fa-address-book"></i>
            <p>{{ __('Clientes - Particulares') }}</p>
            </a>
            </li> --}}

            <li @if ($pageSlug=='resaleCustomers' ) class="active " @endif>
                <a href="{{ route('pages.resaleCustomers') }}">
                    <i class="fas fa-address-book"></i>
                    <p>{{ __('Clientes - Revenda') }}</p>
                </a>
            </li>

            <li @if ($pageSlug=='segmentedCustomers' ) class="active " @endif>
                <a href="{{ route('pages.segmentedCustomers') }}">
                    <i class="fas fa-address-book"></i>
                    <p>{{ __('Clientes - Segmentada') }}</p>
                </a>
            </li>

            {{-- DEFINIÇÕES DE UTILIZADOR --}}
            <li>
                <a data-toggle="collapse" href="#user-management" aria-expanded="false">
                    <i class="fas fa-cogs"></i>
                    <span class="nav-link-text">{{ __('Definições de Utilizador') }}</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse" id="user-management">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug=='profile' ) class="active " @endif>
                            <a href="{{ route('profile.edit')  }}">
                                <i class="fas fa-id-card"></i>
                                <p>{{ __('Perfil') }}</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            {{-- ATRIBUTOS --}}
            <li>
                <a data-toggle="collapse" href="#attributes" aria-expanded="false">
                    <i class="fas fa-cogs"></i>
                    <span class="nav-link-text">{{ __('Definições da Página') }}</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse" id="attributes">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug=='attributes' ) class="active " @endif>
                            <a href="{{ route('pages.attributes')  }}">
                                <i class="fas fa-sliders-h"></i>
                                <p>{{ __('Gestor de Atributos') }}</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            {{-- WEBSERVICE --}}
            <li @if ($pageSlug=='webService' ) class="active " @endif>
                <a href="{{ route('pages.webservice') }}">
                    <i class="fab fa-cloudversify"></i>
                    <p>{{ __('Webservice - Fornecedores') }}</p>
                </a>
            </li>

            {{-- OfficeGest --}}
            <li @if ($pageSlug=='OfficeGest_home' ) class="active " @endif>
                <a href="{{ route('pages.OfficeGest_home') }}">
                    <i class="fab fa-cloudversify"></i>
                    <p>{{ __('OfficeGest') }}</p>
                </a>
            </li>



        </ul>



    </div>
</div>