<footer class="footer">
    <div class="container-fluid">
        <ul class="nav">
            <li class="copyright">
                <a href="https://www.facebook.com/Cubique.pt/" target="_blank">{{ __('CUBIQUE | SOLUTIONS') }}</a>
            </li>
        </ul>
        <div class="copyright">
            &copy; {{ now()->year }} {{ __('feito por') }}
            <a href="https://www.linkedin.com/in/jo%C3%A3o-domingues-849119144/" target="_blank">{{ __('João Domingues') }}</a> &amp;
            <a href="https://www.cubique.pt/" target="_blank">{{ __('CUBIQUE | Solutions') }}</a>.
        </div>
    </div>
</footer>
