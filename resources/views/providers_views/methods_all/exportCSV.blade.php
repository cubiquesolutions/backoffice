<script>
    function exportToCSV(pathTo) {

        axios.get('./' + pathTo + '/generateCSV', {})
            .then(function(response) {
                console.log("Ficheiro: " + response.data);

                Swal.fire({
                    icon: 'success',
                    title: 'Os artigos de todos os fornecedores foram combinados numa só tabela!',
                    text: 'Pode fazer o download do ficheiro!',
                    confirmButtonText: 'Download',
                    allowEscapeKey: false,
                    allowOutsideClick: false,

                    preConfirm: () => {
                        window.location.href = pathTo + '/download' + '?fileName_from_client=' + response.data;
                    }
                })


                .then((result) => {

                    if (result.value) {

                        Swal.fire({
                            icon: 'success',
                            title: 'Arquivo: Produtos via Webservice',
                            text: 'O arquivo foi baixado com sucesso!',
                            confirmButtonText: 'OK',
                            allowEscapeKey: false,
                            allowOutsideClick: false,

                            preConfirm: () => {
                                axios.get(pathTo + '/removefile' + '?fileName_from_client=' + response.data);
                            }
                        })

                    }

                })


            })
            .catch(function(error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Erro!',
                    text: 'Não foi possivel exportar. Tente novamente mais tarde.',
                })
                console.log(error);
            });

    };
</script>