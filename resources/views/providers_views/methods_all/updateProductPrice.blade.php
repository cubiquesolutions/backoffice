<script>
    function bulkUpdateHermida() {
        Swal.fire({
            title: 'Atualizar preços: Hermida',
            text: "Não feche a página do navegador enquanto a atualização!",

            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, atualizar!',

            showLoaderOnConfirm: true,

            preConfirm: () => {

                return fetch('../webservice/hermida/bulkUpdate/')
                    .then(response => {

                        Swal.fire({
                            icon: 'success',
                            title: 'Atualizado Hermida!',
                            showConfirmButton: true,
                            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Recarregar a página!',
                            preConfirm: () => {
                                window.location.href = '../webservice/';
                            }
                        })

                    })

                .catch(error => {
                    Swal.fire(
                        'Erro',
                        'Os produtos do fornecedor não foram atualizados!"',
                        'error'
                    );
                })

            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    }

    function bulkUpdateGmbh() {
        Swal.fire({
            title: 'Atualizar preços: Gmbh',
            text: "Não feche a página do navegador enquanto a atualização!",

            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, atualizar!',

            showLoaderOnConfirm: true,

            preConfirm: () => {

                axios.get('../webservice/gmbh/bulkUpdate/')
                    .then(response => {

                        Swal.fire({
                            icon: 'success',
                            title: 'Atualizado Gmbh!',
                            showConfirmButton: true,
                            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Recarregar a página!',
                            preConfirm: () => {
                                window.location.href = '../webservice/';
                            }
                        })

                    })

                .catch(error => {
                    Swal.fire(
                        'Erro',
                        'Os produtos do fornecedor não foram atualizados!"',
                        'error'
                    );
                })

            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    }

    function bulkUpdateExadi() {
        Swal.fire({
            title: 'Atualizar preços: Exadi',
            text: "Não feche a página do navegador enquanto a atualização!",

            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, atualizar!',

            showLoaderOnConfirm: true,

            preConfirm: () => {

                axios.get('../webservice/exadi/bulkUpdate/')
                    .then(response => {

                        Swal.fire({
                            icon: 'success',
                            title: 'Atualizado Exadi!',
                            showConfirmButton: true,
                            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Recarregar a página!',
                            preConfirm: () => {
                                window.location.href = '../webservice/';
                            }
                        })

                    })

                .catch(error => {
                    Swal.fire(
                        'Erro',
                        'Os produtos do fornecedor não foram atualizados!"',
                        'error'
                    );
                })

            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    }
</script>