@extends('layouts.app', ['page' => __('Web service - Fornecedores'), 'pageSlug' => 'webservice'])

@extends('providers_views.methods_all.updateProductPrice');
@extends('providers_views.methods_all.exportCSV');

{{-- SCRIPTS --}}
@extends('layouts.scripts');



@section('content')

<div class="card">

    <div class="row">
        <div class="col-10">
            <div class="alert alert-default" role="alert" style="text-align: center;"><b>Webservice - Fornecedores</b>
            </div>
        </div>
        <div class="col-2">
            <div class=""><button class="btn btn-xs btn-primary" type="button"
                    onclick="exportToCSV('webservice/all')"><b>Exportar tudo
                        p/ CSV</b></button>
            </div>
        </div>
    </div>

    <div class="alert alert-warning fade show" role="alert" style="text-align: center;">
        <strong>Dica:</strong> Use o <strong>Google Chrome</strong><br>
        <strong>Dica:</strong> Tecla <strong>F11</strong> para uma melhor experiência.<br>
        <strong>Dica:</strong> Clique com o rato dentro da tabela e navegue com as <strong>SETAS do teclado</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="tim-icons icon-simple-remove"></i>
        </button>
    </div>


    {{-- Hermida --}}
    <div class="row">
        <div class="col-10">
            <div class="alert alert-danger" role="alert" style="text-align: center;"><b>Hermida</b></div>
        </div>
        <div class="col-2">
            <div class=""><button class="btn btn-xs btn-primary" type="button" onclick="bulkUpdateHermida()">Atualizar
                    <b>Hermida</b></button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table id="products_hermida" style="width:100%" class="table table-bordered display nowrap">
            <thead>
                <tr>
                    {{-- <th>id</th> --}}
                    {{-- <th>Opções</th> --}}
                    <th>ref_supplier</th>
                    <th>ref_cubique</th>
                    <th>description</th>
                    <th>stock_old</th>
                    <th>stock_new</th>
                    <th>price_old</th>
                    <th>price_new</th>
                    <th>updated_at</th>
                </tr>
            </thead>
        </table>
    </div>

    {{-- Gmbh --}}
    <div class="row">
        <div class="col-10">
            <div class="alert alert-warning" role="alert" style="text-align: center;"><b>Winterholt & Hering GmbH</b>
            </div>
        </div>
        <div class="col-2">
            <div class=""><button class="btn btn-xs btn-primary" type="button" onclick="bulkUpdateGmbh()">Atualizar
                    <b>GmbH</b></button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table id="products_gmbh" style="width:100%" class="table table-bordered display nowrap">
            <thead>
                <tr>
                    {{-- <th>id</th> --}}
                    {{-- <th>Opções</th> --}}
                    <th>ref_supplier</th>
                    <th>ref_cubique</th>
                    <th>description</th>
                    <th>stock_old</th>
                    <th>stock_new</th>
                    <th>price_old</th>
                    <th>price_new</th>
                    <th>updated_at</th>
                </tr>
            </thead>
        </table>
    </div>

    {{-- Exadi --}}
    <div class="row">
        <div class="col-10">
            <div class="alert alert-success" role="alert" style="text-align: center;"><b>Exadi</b>
            </div>
        </div>
        <div class="col-2">
            <div class=""><button class="btn btn-xs btn-primary" type="button" onclick="bulkUpdateExadi()">Atualizar
                    <b>Exadi</b></button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table id="products_exadi" style="width:100%" class="table table-bordered display nowrap">
            <thead>
                <tr>
                    {{-- <th>id</th> --}}
                    {{-- <th>Opções</th> --}}
                    <th>ref_supplier</th>
                    <th>ref_cubique</th>
                    <th>code_supplier</th>
                    <th>description</th>
                    {{-- <th>stock_old</th> --}}
                    <th>stock_new</th>
                    <th>price_old</th>
                    <th>price_new</th>
                    <th>updated_at</th>
                </tr>
            </thead>
        </table>
    </div>

</div>

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop


<script>
    $(function() {

        //hermida
        $('#products_hermida thead tr').clone(true).appendTo('#products_hermida thead');
        
        $('#products_hermida thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( tableHermida.column(i).search() !== this.value ) {
                    tableHermida
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );

        //gmbh
        $('#products_gmbh thead tr').clone(true).appendTo('#products_gmbh thead');
        
        $('#products_gmbh thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( tableGmbh.column(i).search() !== this.value ) {
                    tableGmbh
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );

        //exadi
        $('#products_exadi thead tr').clone(true).appendTo('#products_exadi thead');
        
        $('#products_exadi thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( tableExadi.column(i).search() !== this.value ) {
                    tableExadi
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );

var tableHermida = $('#products_hermida').DataTable({
        //processing: true,
        //serverSide: true,
        
        ajax: '{!! route('getAllProducts_hermida') !!}',
        
        "autoWidth": false,
        "scrollX": true,

        "order": [[ 0, "desc" ]],
        
        columns: [
            // { data: 'id', name: 'id' },
            // { data: 'actions', name: 'actions' },
            { data: 'ref_supplier', name: 'ref_supplier'},
            { data: 'ref_cubique', name: 'ref_cubique'},
            { data: 'description', name: 'description'},
            { data: 'stock_old', name: 'stock_old'},
            { data: 'stock_new', name: 'stock_new'},
            { data: 'price_old', name: 'price_old'},
            { data: 'price_new', name: 'price_new'},
            { data: 'updated_at', name: 'updated_at'}
        ],
    });

    var tableGmbh = $('#products_gmbh').DataTable({
        //processing: true,
        //serverSide: true,
        
        ajax: '{!! route('getAllProducts_gmbh') !!}',
        
        "autoWidth": false,
        "scrollX": true,

        "order": [[ 0, "desc" ]],
        
        columns: [
            // { data: 'id', name: 'id' },
            // { data: 'actions', name: 'actions' },
            { data: 'ref_supplier', name: 'ref_supplier'},
            { data: 'ref_cubique', name: 'ref_cubique'},
            { data: 'description', name: 'description'},
            { data: 'stock_old', name: 'stock_old'},
            { data: 'stock_new', name: 'stock_new'},
            { data: 'price_old', name: 'price_old'},
            { data: 'price_new', name: 'price_new'},
            { data: 'updated_at', name: 'updated_at'}
        ],
    });

    var tableExadi = $('#products_exadi').DataTable({
        //processing: true,
        //serverSide: true,
        
        ajax: '{!! route('getAllProducts_exadi') !!}',
        
        "autoWidth": false,
        "scrollX": true,

        "order": [[ 0, "desc" ]],
        
        columns: [
            // { data: 'id', name: 'id' },
            // { data: 'actions', name: 'actions' },
            { data: 'ref_supplier', name: 'ref_supplier'},
            { data: 'ref_cubique', name: 'ref_cubique'},
            { data: 'code_supplier', name: 'code_supplier'},
            { data: 'description', name: 'description'},
            // { data: 'stock_old', name: 'stock_old'},
            { data: 'stock_new', name: 'stock_new'},
            { data: 'price_old', name: 'price_old'},
            { data: 'price_new', name: 'price_new'},
            { data: 'updated_at', name: 'updated_at'}
        ],
    });

});
</script>

@stop