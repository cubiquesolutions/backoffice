@extends('layouts.app', ['page' => __('Gestor de Atributos'), 'pageSlug' => 'attributes'])

{{-- SCRIPTS --}}
@extends('layouts.scripts');

@section('content')

<div class="card">

    <div class="alert alert-default" role="alert" style="text-align: center;"><b>Gestor de Atributos</b></div>
    <div class="alert alert-warning fade show" role="alert" style="text-align: center;">
        <strong>Selecione o atributo a alterar</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="tim-icons icon-simple-remove"></i>
        </button>
    </div>

</div>

{{-- attributes1_collapse --}}
<div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#attributes1_collapse" role="button" aria-expanded="false"
            aria-controls="attributes1_collapse">Atributo: Produto</h3>
    </div>
</div>

<div class="card collapse" id="attributes1_collapse">
    <div class="card-body">
        <div class="row">

            <div class="card col-sm-10">
                <div class="card-body">
                    <table id="attributes1_table" style="width:100%; zoom:0.8; cursor:pointer"
                        class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="card col-sm-2 align-self-bottom">
                <div class="card-body bg-dark">

                    {{ Form::open(array('route' => 'attributes.createAtr1', 'method' => 'post')) }}
                    {!! Form::text('Name', $value = null, ['class' => 'form-control', 'placeholder'=>'Nome',
                    'style'=>'margin-bottom:10px']) !!}
                    {!! Form::text('Description', $value = null, ['class' => 'form-control', 'placeholder'=>'Descrição',
                    'style'=>'margin-bottom:10px']) !!}
                    {{ Form::submit('Criar novo', ['class' => 'btn btn-success mb-2']) }}
                    {{ Form::close() }}

                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

{{-- attributes2_collapse --}}
<div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#attributes2_collapse" role="button" aria-expanded="false"
            aria-controls="attributes2_collapse">Atributo: Área de Negócio</h3>
    </div>
</div>

<div class="card collapse" id="attributes2_collapse">
    <div class="card-body">
        <div class="row">

            <div class="card col-sm-10">
                <div class="card-body">
                    <table id="attributes2_table" style="width:100%; zoom:0.8; cursor:pointer"
                        class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="card col-sm-2 align-self-bottom">
                <div class="card-body bg-dark">

                    {{ Form::open(array('route' => 'attributes.createAtr2', 'method' => 'post')) }}
                    {!! Form::text('Name', $value = null, ['class' => 'form-control', 'placeholder'=>'Nome',
                    'style'=>'margin-bottom:10px']) !!}
                    {!! Form::text('Description', $value = null, ['class' => 'form-control', 'placeholder'=>'Descrição',
                    'style'=>'margin-bottom:10px']) !!}
                    {{ Form::submit('Criar novo', ['class' => 'btn btn-success mb-2']) }}
                    {{ Form::close() }}

                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

{{-- attributes3_collapse --}}
<div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#attributes3_collapse" role="button" aria-expanded="false"
            aria-controls="attributes3_collapse">Atributo: Marca Adquirida</h3>
    </div>
</div>

<div class="card collapse" id="attributes3_collapse">
    <div class="card-body">
        <div class="row">

            <div class="card col-sm-10">
                <div class="card-body">
                    <table id="attributes3_table" style="width:100%; zoom:0.8; cursor:pointer"
                        class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="card col-sm-2 align-self-bottom">
                <div class="card-body bg-dark">

                    {{ Form::open(array('route' => 'attributes.createAtr3', 'method' => 'post')) }}
                    {!! Form::text('Name', $value = null, ['class' => 'form-control', 'placeholder'=>'Nome',
                    'style'=>'margin-bottom:10px']) !!}
                    {!! Form::text('Description', $value = null, ['class' => 'form-control', 'placeholder'=>'Descrição',
                    'style'=>'margin-bottom:10px']) !!}
                    {{ Form::submit('Criar novo', ['class' => 'btn btn-success mb-2']) }}
                    {{ Form::close() }}

                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

{{-- attributes4_collapse --}}
<div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#attributes4_collapse" role="button" aria-expanded="false"
            aria-controls="attributes4_collapse">Atributo: Atributo 4</h3>
    </div>
</div>

<div class="card collapse" id="attributes4_collapse">
    <div class="card-body">
        <div class="row">

            <div class="card col-sm-10">
                <div class="card-body">
                    <table id="attributes4_table" style="width:100%; zoom:0.8; cursor:pointer"
                        class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="card col-sm-2 align-self-bottom">
                <div class="card-body bg-dark">

                    {{ Form::open(array('route' => 'attributes.createAtr4', 'method' => 'post')) }}
                    {!! Form::text('Name', $value = null, ['class' => 'form-control', 'placeholder'=>'Nome',
                    'style'=>'margin-bottom:10px']) !!}
                    {!! Form::text('Description', $value = null, ['class' => 'form-control', 'placeholder'=>'Descrição',
                    'style'=>'margin-bottom:10px']) !!}
                    {{ Form::submit('Criar novo', ['class' => 'btn btn-success mb-2']) }}
                    {{ Form::close() }}

                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

{{-- attributes5_collapse --}}
<div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#attributes5_collapse" role="button" aria-expanded="false"
            aria-controls="attributes5_collapse">Atributo: Atributo 5</h3>
    </div>
</div>

<div class="card collapse" id="attributes5_collapse">
    <div class="card-body">
        <div class="row">

            <div class="card col-sm-10">
                <div class="card-body">
                    <table id="attributes5_table" style="width:100%; zoom:0.8; cursor:pointer"
                        class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="card col-sm-2 align-self-bottom">
                <div class="card-body bg-dark">

                    {{ Form::open(array('route' => 'attributes.createAtr5', 'method' => 'post')) }}
                    {!! Form::text('Name', $value = null, ['class' => 'form-control', 'placeholder'=>'Nome',
                    'style'=>'margin-bottom:10px']) !!}
                    {!! Form::text('Description', $value = null, ['class' => 'form-control', 'placeholder'=>'Descrição',
                    'style'=>'margin-bottom:10px']) !!}
                    {{ Form::submit('Criar novo', ['class' => 'btn btn-success mb-2']) }}
                    {{ Form::close() }}

                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

{{-- attributes6_collapse --}}
<div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#attributes6_collapse" role="button" aria-expanded="false"
            aria-controls="attributes6_collapse">Atributo: Atributo 6</h3>
    </div>
</div>

<div class="card collapse" id="attributes6_collapse">
    <div class="card-body">
        <div class="row">

            <div class="card col-sm-10">
                <div class="card-body">
                    <table id="attributes6_table" style="width:100%; zoom:0.8; cursor:pointer"
                        class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="card col-sm-2 align-self-bottom">
                <div class="card-body bg-dark">

                    {{ Form::open(array('route' => 'attributes.createAtr6', 'method' => 'post')) }}
                    {!! Form::text('Name', $value = null, ['class' => 'form-control', 'placeholder'=>'Nome',
                    'style'=>'margin-bottom:10px']) !!}
                    {!! Form::text('Description', $value = null, ['class' => 'form-control', 'placeholder'=>'Descrição',
                    'style'=>'margin-bottom:10px']) !!}
                    {{ Form::submit('Criar novo', ['class' => 'btn btn-success mb-2']) }}
                    {{ Form::close() }}

                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

{{-- attributes7_collapse --}}
<div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#attributes7_collapse" role="button" aria-expanded="false"
            aria-controls="attributes7_collapse">Atributo: Setor de Atividade</h3>
    </div>
</div>

<div class="card collapse" id="attributes7_collapse">
    <div class="card-body">
        <div class="row">

            <div class="card col-sm-10">
                <div class="card-body">
                    <table id="attributes7_table" style="width:100%; zoom:0.8; cursor:pointer"
                        class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="card col-sm-2 align-self-bottom">
                <div class="card-body bg-dark">

                    {{ Form::open(array('route' => 'attributes.createAtr7', 'method' => 'post')) }}
                    {!! Form::text('Name', $value = null, ['class' => 'form-control', 'placeholder'=>'Nome',
                    'style'=>'margin-bottom:10px']) !!}
                    {!! Form::text('Description', $value = null, ['class' => 'form-control', 'placeholder'=>'Descrição',
                    'style'=>'margin-bottom:10px']) !!}
                    {{ Form::submit('Criar novo', ['class' => 'btn btn-success mb-2']) }}
                    {{ Form::close() }}

                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

{{-- attributes8_collapse --}}
<div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#attributes8_collapse" role="button" aria-expanded="false"
            aria-controls="attributes8_collapse">Atributo: Estado de Contacto</h3>
    </div>
</div>

<div class="card collapse" id="attributes8_collapse">

    <div class="card-body">

        <div class="row">

            <div class="card col-sm-10">
                <div class="card-body">
                    <table id="attributes8_table" style="width:100%; zoom:0.8; cursor:pointer"
                        class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="card col-sm-2 align-self-bottom">
                <div class="card-body bg-dark">

                    {{ Form::open(array('route' => 'attributes.createAtr8', 'method' => 'post')) }}
                    {!! Form::text('Name', $value = null, ['class' => 'form-control', 'placeholder'=>'Nome',
                    'style'=>'margin-bottom:10px']) !!}
                    {!! Form::text('Description', $value = null, ['class' => 'form-control', 'placeholder'=>'Descrição',
                    'style'=>'margin-bottom:10px']) !!}
                    {{ Form::submit('Criar novo', ['class' => 'btn btn-success mb-2']) }}
                    {{ Form::close() }}

                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </div>



        </div>


    </div>
</div>

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

{{-- POPULAR TABELAS --}}
<script>
    //FILTROS COSTUM NAS TABELAS
        /*
        $('#search-costum-form-atrributes-1').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
        */

        // POPULAR AS TABELAS
        $(function() {
        $('#attributes1_table').DataTable({
            //processing: true,
            //serverSide: true,
            ajax: {
                url: '{!! route('getAllAttributes_1') !!}',
                data: function (d) {
                    d.name = $('input[name=name]').val();
                    d.description = $('input[name=description]').val();
                }
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false},
            ],
            
        });

        $('#attributes2_table').DataTable({
            //processing: true,
            //serverSide: true,
            ajax: '{!! route('getAllAttributes_2') !!}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'actions', name: 'actions' },
            ]
        });
        $('#attributes3_table').DataTable({
            //processing: true,
            //serverSide: true,
            ajax: '{!! route('getAllAttributes_3') !!}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'actions', name: 'actions' },
            ]
        });

        $('#attributes4_table').DataTable({
            //processing: true,
            //serverSide: true,
            ajax: '{!! route('getAllAttributes_4') !!}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'actions', name: 'actions' },
            ]
        });

        $('#attributes5_table').DataTable({
            //processing: true,
            //serverSide: true,
            ajax: '{!! route('getAllAttributes_5') !!}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'actions', name: 'actions' },
            ]
        });

        $('#attributes6_table').DataTable({
            //processing: true,
            //serverSide: true,
            ajax: '{!! route('getAllAttributes_6') !!}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'actions', name: 'actions' },
            ]
        });

        $('#attributes7_table').DataTable({
            //processing: true,
            //serverSide: true,
            ajax: '{!! route('getAllAttributes_7') !!}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'actions', name: 'actions' },
            ]
        });

        $('#attributes8_table').DataTable({
            //processing: true,
            //serverSide: true,
            ajax: '{!! route('getAllAttributes_8') !!}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'actions', name: 'actions' },
            ]
        });
        
    });


function deleteAtr1Modal($id) {

    Swal.fire({
            title: "Tem a certeza?",
            text: "Uma vez apagado, não pode recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, apagar!',
        })
        .then((result) => {
            if (result.value) {
                axios.delete('/settings/attributes/deleteAtr1/' + $id)
                    .then(response => {
                        Swal.fire(
                        'Apagado',
                        'O Atributo escolhido foi apagado!"',
                        'success'
                        );
                    })
                    .catch(error => {
                        Swal.fire(
                        'Erro',
                        'O Atributo escolhido não foi apagado!"',
                        'error'
                        );
                    });
            }
        });
    };

function deleteAtr2Modal($id) {

    Swal.fire({
            title: "Tem a certeza?",
            text: "Uma vez apagado, não pode recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, apagar!',
        })
        .then((result) => {
            if (result.value) {
                axios.delete('/settings/attributes/deleteAtr2/' + $id)
                    .then(response => {
                        Swal.fire(
                        'Apagado',
                        'O Atributo escolhido foi apagado!"',
                        'success'
                        );
                    })
                    .catch(error => {
                        Swal.fire(
                        'Erro',
                        'O Atributo escolhido não foi apagado!"',
                        'error'
                        );
                    });
            }
        });
    };

function deleteAtr3Modal($id) {

    Swal.fire({
            title: "Tem a certeza?",
            text: "Uma vez apagado, não pode recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, apagar!',
        })
        .then((result) => {
            if (result.value) {
                axios.delete('/settings/attributes/deleteAtr3/' + $id)
                    .then(response => {
                        Swal.fire(
                        'Apagado',
                        'O Atributo escolhido foi apagado!"',
                        'success'
                        );
                    })
                    .catch(error => {
                        Swal.fire(
                        'Erro',
                        'O Atributo escolhido não foi apagado!"',
                        'error'
                        );
                    });
            }
        });
};

function deleteAtr4Modal($id) {

    Swal.fire({
            title: "Tem a certeza?",
            text: "Uma vez apagado, não pode recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, apagar!',
        })
        .then((result) => {
            if (result.value) {
                axios.delete('/settings/attributes/deleteAtr4/' + $id)
                    .then(response => {
                        Swal.fire(
                        'Apagado',
                        'O Atributo escolhido foi apagado!"',
                        'success'
                        );
                    })
                    .catch(error => {
                        Swal.fire(
                        'Erro',
                        'O Atributo escolhido não foi apagado!"',
                        'error'
                        );
                    });
            }
        });
};

function deleteAtr5Modal($id) {

    Swal.fire({
            title: "Tem a certeza?",
            text: "Uma vez apagado, não pode recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, apagar!',
        })
        .then((result) => {
            if (result.value) {
                axios.delete('/settings/attributes/deleteAtr5/' + $id)
                    .then(response => {
                        Swal.fire(
                        'Apagado',
                        'O Atributo escolhido foi apagado!"',
                        'success'
                        );
                    })
                    .catch(error => {
                        Swal.fire(
                        'Erro',
                        'O Atributo escolhido não foi apagado!"',
                        'error'
                        );
                    });
            }
        });
};

function deleteAtr6Modal($id) {

    Swal.fire({
            title: "Tem a certeza?",
            text: "Uma vez apagado, não pode recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, apagar!',
        })
        .then((result) => {
            if (result.value) {
                axios.delete('/settings/attributes/deleteAtr6/' + $id)
                    .then(response => {
                        Swal.fire(
                        'Apagado',
                        'O Atributo escolhido foi apagado!"',
                        'success'
                        );
                    })
                    .catch(error => {
                        Swal.fire(
                        'Erro',
                        'O Atributo escolhido não foi apagado!"',
                        'error'
                        );
                    });
            }
        });
};

function deleteAtr7Modal($id) {

    Swal.fire({
            title: "Tem a certeza?",
            text: "Uma vez apagado, não pode recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, apagar!',
        })
        .then((result) => {
            if (result.value) {
                axios.delete('/settings/attributes/deleteAtr7/' + $id)
                    .then(response => {
                        Swal.fire(
                        'Apagado',
                        'O Atributo escolhido foi apagado!"',
                        'success'
                        );
                    })
                    .catch(error => {
                        Swal.fire(
                        'Erro',
                        'O Atributo escolhido não foi apagado!"',
                        'error'
                        );
                    });
            }
        });
};

function deleteAtr8Modal($id) {

    Swal.fire({
            title: "Tem a certeza?",
            text: "Uma vez apagado, não pode recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, apagar!',
        })
        .then((result) => {
            if (result.value) {
                axios.delete('/settings/attributes/deleteAtr8/' + $id)
                    .then(response => {
                        Swal.fire(
                        'Apagado',
                        'O Atributo escolhido foi apagado!"',
                        'success'
                        );
                    })
                    .catch(error => {
                        Swal.fire(
                        'Erro',
                        'O Atributo escolhido não foi apagado!"',
                        'error'
                        );
                    });
            }
        });
    };
</script>

@endsection