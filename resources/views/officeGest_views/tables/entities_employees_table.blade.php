<script>
    $(function() {

        $('#entities_employees_table thead tr').clone(true).appendTo('#entities_employees_table thead');

        $('#entities_employees_table thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

    var table = $('#entities_employees_table').DataTable({

        ajax: '{!! route('get_entities_employees') !!}',
        "autoWidth": false,
        "scrollX": true,
        "order": [
            [0, "desc"]
        ],

        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'address', name: 'address' },
            { data: 'city', name: 'city' },
            { data: 'zipcode', name: 'zipcode' },
            { data: 'country', name: 'country' },
            { data: 'phone1', name: 'phone1' },
            { data: 'phone2', name: 'phone2' },
            { data: 'mobilephone', name: 'mobilephone' },
            { data: 'email', name: 'email' },
            { data: 'active', name: 'active' },
            { data: 'login', name: 'login' },
            { data: 'teamcode', name: 'teamcode' },
            { data: 'privilegecode', name: 'privilegecode' },
            { data: 'warehouse_id', name: 'warehouse_id' }
        ],

    });

    var oldThis;
    
    $('#entities_employees_table tbody').on( 'click', 'tr', function () {

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

});


</script>