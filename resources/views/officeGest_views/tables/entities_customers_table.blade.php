<script>
    $(function() {

        $('#entities_customers_table thead tr').clone(true).appendTo('#entities_customers_table thead');

        $('#entities_customers_table thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

    var table = $('#entities_customers_table').DataTable({

        ajax: '{!! route('get_entities_customers') !!}',
        "autoWidth": false,
        "scrollX": true,
        "order": [
            [0, "desc"]
        ],

        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'address', name: 'address' },
            { data: 'city', name: 'city' },
            { data: 'zipcode', name: 'zipcode' },
            { data: 'country', name: 'country' },
            { data: 'customertaxid', name: 'customertaxid' },
            { data: 'phone1', name: 'phone1' },
            { data: 'phone2', name: 'phone2' },
            { data: 'mobilephone', name: 'mobilephone' },
            { data: 'fax', name: 'fax' },
            { data: 'email', name: 'email' },
            { data: 'webpage', name: 'webpage' },
            { data: 'vendorcode', name: 'vendorcode' },
            { data: 'active', name: 'active' },
            { data: 'entitytype', name: 'entitytype' },
            { data: 'classifcode', name: 'classifcode' },
            { data: 'campaigncode', name: 'campaigncode' },
            { data: 'customertype', name: 'customertype' },
            { data: 'id_client_web', name: 'id_client_web' }
        ],

    });

    var oldThis;
    
    $('#entities_customers_table tbody').on( 'click', 'tr', function () {

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

});


</script>