<script>
    $(function() {

        $('#docs_reports_sales_documents_table thead tr').clone(true).appendTo('#docs_reports_sales_documents_table thead');

        $('#docs_reports_sales_documents_table thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

    var table = $('#docs_reports_sales_documents_table').DataTable({

        ajax: '{!! route('get_reports_sales_documents') !!}',
        "autoWidth": false,
        "scrollX": true,
        "order": [
            [0, "desc"]
        ],

        columns: [
            { data: 'serie', name: 'serie' },
            { data: 'employeecode', name: 'employeecode' },
            { data: 'vendorcode', name: 'vendorcode' },
            { data: 'customertaxid', name: 'customertaxid' },
            { data: 'documentnumber', name: 'documentnumber' },
            { data: 'canceled', name: 'canceled' },
            { data: 'updatedstocks', name: 'updatedstocks' },
            { data: 'updatedcurrentaccount', name: 'updatedcurrentaccount' },
            { data: 'debit', name: 'debit' },
            { data: 'credit', name: 'credit' },
            { data: 'documentname', name: 'documentname' },
            { data: 'documenttype', name: 'documenttype' },
            { data: 'generaltype', name: 'generaltype' },
            { data: 'number', name: 'number' },
            { data: 'idcustomer', name: 'idcustomer' },
            { data: 'customername', name: 'customername' },
            { data: 'employeename', name: 'employeename' },
            { data: 'vendorname', name: 'vendorname' },
            { data: 'date', name: 'date' },
            { data: 'datedue', name: 'datedue' },
            { data: 'total', name: 'total' },
            { data: 'totalwithoutvat', name: 'totalwithoutvat' },
            { data: 'valuevat', name: 'valuevat' },
            { data: 'valuepaid', name: 'valuepaid' },
            { data: 'valuediscount', name: 'valuediscount' },
        ],

    });

    var oldThis;
    
    $('#docs_reports_sales_documents_table tbody').on( 'click', 'tr', function () {

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

});


</script>