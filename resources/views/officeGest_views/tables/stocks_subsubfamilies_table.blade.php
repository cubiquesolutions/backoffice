<script>
    $(function() {

        $('#stocks_subsubfamilies_table thead tr').clone(true).appendTo('#stocks_subsubfamilies_table thead');

        $('#stocks_subsubfamilies_table thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

    var table = $('#stocks_subsubfamilies_table').DataTable({

        ajax: '{!! route('get_stocks_subsubfamilies') !!}',
        "autoWidth": false,
        "scrollX": true,
        "order": [
            [0, "desc"]
        ],

        columns: [
            { data: 'subsubfamilyid', name: 'subsubfamilyid' },
            { data: 'description', name: 'description' },
            { data: 'familyid', name: 'familyid' },
            { data: 'subfamilyid', name: 'subfamilyid' }
        ],

    });

    var oldThis;
    
    $('#stocks_subsubfamilies_table tbody').on( 'click', 'tr', function () {

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

});


</script>