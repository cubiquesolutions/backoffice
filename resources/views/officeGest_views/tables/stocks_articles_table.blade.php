<script>
    $(function() {

        $('#stocks_articles_table thead tr').clone(true).appendTo('#stocks_articles_table thead');

        $('#stocks_articles_table thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

    var table = $('#stocks_articles_table').DataTable({

        ajax: '{!! route('get_stocks_articles') !!}',
        "autoWidth": false,
        "scrollX": true,
        "order": [
            [0, "desc"]
        ],

        columns: [
            { data: 'id', name: 'id' },
            { data: 'description', name: 'description' },
            { data: 'articletype', name: 'articletype' },
            { data: 'purchasingprice', name: 'purchasingprice' },
            { data: 'sellingprice', name: 'sellingprice' },
            { data: 'vatid', name: 'vatid' },
            { data: 'unit', name: 'unit' },
            { data: 'stock_quantity', name: 'stock_quantity' },
            { data: 'family', name: 'family' },
            { data: 'subfamily', name: 'subfamily' },
            { data: 'barcode', name: 'barcode' },
            { data: 'brand', name: 'brand' },
            { data: 'active', name: 'active' },
            { data: 'spaces_dimensions', name: 'spaces_dimensions' },
            { data: 'activeforweb', name: 'activeforweb' },
            { data: 'alterationdate', name: 'alterationdate' },
            { data: 're', name: 're' },
            { data: 'idforweb', name: 'idforweb' },
            { data: 'priceforweb', name: 'priceforweb' },
            { data: 'referenceforweb', name: 'referenceforweb' },
            { data: 'long_description', name: 'long_description' },
            { data: 'short_description', name: 'short_description' }
        ],

    });

    var oldThis;
    
    $('#stocks_articles_table tbody').on( 'click', 'tr', function () {

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

});


</script>