<script>
    $(function() {

        $('#businessopportunities_table thead tr').clone(true).appendTo('#businessopportunities_table thead');

        $('#businessopportunities_table thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

    var table = $('#businessopportunities_table').DataTable({

        ajax: '{!! route('get_crm_businessopportunities') !!}',
        "autoWidth": false,
        "scrollX": true,
        "order": [
            [0, "desc"]
        ],

        columns: [
            { data: 'id', name: 'id' },
            { data: 'description', name: 'description' },
            { data: 'phase', name: 'phase' },
            { data: 'businessareas', name: 'businessareas' },
            { data: 'date_start', name: 'date_start' },
            { data: 'date_end_expected', name: 'date_end_expected' },
            { data: 'employeecode', name: 'employeecode' },
            { data: 'vendorcode', name: 'vendorcode' },
            { data: 'entitycode', name: 'entitycode' },
            { data: 'entitytype', name: 'entitytype' },
            { data: 'campaigncode', name: 'campaigncode' },
            { data: 'done', name: 'done' },
        ],

    });

    var oldThis;
    
    $('#businessopportunities_table tbody').on( 'click', 'tr', function () {

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

});


</script>