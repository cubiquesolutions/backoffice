<script>
    $(function() {

        $('#crm_appointments_table thead tr').clone(true).appendTo('#crm_appointments_table thead');

        $('#crm_appointments_table thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

    var table = $('#crm_appointments_table').DataTable({

        ajax: '{!! route('get_crm_appointments') !!}',
        "autoWidth": false,
        "scrollX": true,
        "order": [
            [0, "desc"]
        ],

        columns: [
            { data: 'id', name: 'id' },
            { data: 'description', name: 'description' },
            { data: 'local', name: 'local' },
            { data: 'entitytype', name: 'entitytype' },
            { data: 'entitycode', name: 'entitycode' },
            { data: 'employeecode', name: 'employeecode' },
            { data: 'campaigncode', name: 'campaigncode' },
            { data: 'businessopportunity', name: 'businessopportunity' },
            { data: 'done', name: 'done' },
            { data: 'equipment', name: 'equipment' },
            { data: 'priority', name: 'priority' },
            { data: 'startdate', name: 'startdate' },
            { data: 'enddate', name: 'enddate' },
        ],

    });

    var oldThis;
    
    $('#crm_appointments_table tbody').on( 'click', 'tr', function () {

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

});


</script>