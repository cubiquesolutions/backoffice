@extends('layouts.app', ['page' => __('OfficeGest'), 'pageSlug' => 'OfficeGest_entities'])

@extends('officeGest_views.tables.entities_customers_table');
@extends('officeGest_views.tables.entities_prospects_table');
@extends('officeGest_views.tables.entities_suppliers_table');
@extends('officeGest_views.tables.entities_employees_table');
@extends('officeGest_views.tables.entities_commercials_table');

@extends('officeGest_views.methods.loading');

{{-- SCRIPTS --}}
@extends('layouts.scripts');

@section('content')

<div class="card">

    <div class="alert alert-default" role="alert" style="text-align: center;"><b>OfficeGest</b></div>
    <div class="alert alert-warning fade show" role="alert" style="text-align: center;">
        <strong>Dica:</strong> Use o <strong>Google Chrome</strong><br>
        <strong>Dica:</strong> Tecla <strong>F11</strong> para uma melhor experiência.<br>
        <strong>Dica:</strong> Clique com o rato dentro da tabela e navegue com as <strong>SETAS do teclado</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="tim-icons icon-simple-remove"></i>
        </button>
    </div>

</div>


{{-- entities_customers_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#entities_customers_collapse" role="button" aria-expanded="false" aria-controls="entities_customers_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os Clientes</h3>
    </div>
</div>

<div class="card collapse" id="entities_customers_collapse">
    <div class="card-body">
        <table id="entities_customers_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>address</th>
                    <th>city</th>
                    <th>zipcode</th>
                    <th>country</th>
                    <th>customertaxid</th>
                    <th>phone1</th>
                    <th>phone2</th>
                    <th>mobilephone</th>
                    <th>fax</th>
                    <th>email</th>
                    <th>webpage</th>
                    <th>vendorcode</th>
                    <th>active</th>
                    <th>entitytype</th>
                    <th>classifcode</th>
                    <th>campaigncode</th>
                    <th>customertype</th>
                    <th>id_client_web</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- entities_prospects_collapse --}}
<div class="card" data-toggle="collapse" href="#entities_prospects_collapse" role="button" aria-expanded="false" aria-controls="entities_prospects_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Prospecções</h3>
    </div>
</div>

<div class="card collapse" id="entities_prospects_collapse">
    <div class="card-body">
        <table id="entities_prospects_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>address</th>
                    <th>city</th>
                    <th>zipcode</th>
                    <th>country</th>
                    <th>customertaxid</th>
                    <th>phone1</th>
                    <th>phone2</th>
                    <th>mobilephone</th>
                    <th>fax</th>
                    <th>email</th>
                    <th>webpage</th>
                    <th>vendorcode</th>
                    <th>active</th>
                    <th>entitytype</th>
                    <th>classifcode</th>
                    <th>campaigncode</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- entities_suppliers_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#entities_suppliers_collapse" role="button" aria-expanded="false" aria-controls="entities_suppliers_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os Fornecedores</h3>
    </div>
</div>

<div class="card collapse" id="entities_suppliers_collapse">
    <div class="card-body">
        <table id="entities_suppliers_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>address</th>
                    <th>city</th>
                    <th>zipcode</th>
                    <th>country</th>
                    <th>customertaxid</th>
                    <th>phone1</th>
                    <th>phone2</th>
                    <th>mobilephone</th>
                    <th>fax</th>
                    <th>email</th>
                    <th>webpage</th>
                    <th>active</th>
                    <th>entitytype</th>
                    <th>classifcode</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- entities_employees_collapse --}}
<div class="card" data-toggle="collapse" href="#entities_employees_collapse" role="button" aria-expanded="false" aria-controls="entities_employees_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os Empregados</h3>
    </div>
</div>

<div class="card collapse" id="entities_employees_collapse">
    <div class="card-body">
        <table id="entities_employees_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>address</th>
                    <th>city</th>
                    <th>zipcode</th>
                    <th>country</th>
                    <th>phone1</th>
                    <th>phone2</th>
                    <th>mobilephone</th>
                    <th>email</th>
                    <th>active</th>
                    <th>login</th>
                    <th>teamcode</th>
                    <th>privilegecode</th>
                    <th>warehouse_id</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- entities_commercials_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#entities_commercials_collapse" role="button" aria-expanded="false" aria-controls="entities_commercials_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os Comerciais</h3>
    </div>
</div>

<div class="card collapse" id="entities_commercials_collapse">
    <div class="card-body">
        <table id="entities_commercials_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>address</th>
                    <th>city</th>
                    <th>zipcode</th>
                    <th>country</th>
                    <th>customertaxid</th>
                    <th>phone1</th>
                    <th>phone2</th>
                    <th>mobilephone</th>
                    <th>email</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop




@stop