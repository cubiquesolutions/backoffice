@extends('layouts.app', ['page' => __('OfficeGest'), 'pageSlug' => 'OfficeGest_docs'])

@extends('officeGest_views.tables.docs_sales_documents_table');
@extends('officeGest_views.tables.docs_sales_receipts_table');
@extends('officeGest_views.tables.docs_purchases_documents_table');
@extends('officeGest_views.tables.docs_reports_sales_documents_table');

@extends('officeGest_views.methods.loading');

{{-- SCRIPTS --}}
@extends('layouts.scripts');

@section('content')

<div class="card">

    <div class="alert alert-default" role="alert" style="text-align: center;"><b>OfficeGest</b></div>
    <div class="alert alert-warning fade show" role="alert" style="text-align: center;">
        <strong>Dica:</strong> Use o <strong>Google Chrome</strong><br>
        <strong>Dica:</strong> Tecla <strong>F11</strong> para uma melhor experiência.<br>
        <strong>Dica:</strong> Clique com o rato dentro da tabela e navegue com as <strong>SETAS do teclado</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="tim-icons icon-simple-remove"></i>
        </button>
    </div>

</div>

{{-- docs_sales_documents_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#docs_sales_documents_collapse" role="button" aria-expanded="false" aria-controls="docs_sales_documents_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os documentos de Venda</h3>
    </div>
</div>

<div class="card collapse" id="docs_sales_documents_collapse">
    <div class="card-body">
        <table id="docs_sales_documents_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>type</th>
                    <th>description</th>
                    <th>payment_document</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- docs_sales_receipts_collapse --}}
<div class="card" data-toggle="collapse" href="#docs_sales_receipts_collapse" role="button" aria-expanded="false" aria-controls="docs_sales_receipts_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os recibos de Venda</h3>
    </div>
</div>

<div class="card collapse" id="docs_sales_receipts_collapse">
    <div class="card-body">
        <table id="docs_sales_receipts_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>type</th>
                    <th>description</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- docs_purchases_documents_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#docs_purchases_documents_collapse" role="button" aria-expanded="false" aria-controls="docs_purchases_documents_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os documentos de Compra</h3>
    </div>
</div>

<div class="card collapse" id="docs_purchases_documents_collapse">
    <div class="card-body">
        <table id="docs_purchases_documents_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>type</th>
                    <th>description</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- docs_reports_sales_documents_collapse --}}
<div class="card" data-toggle="collapse" href="#docs_reports_sales_documents_collapse" role="button" aria-expanded="false" aria-controls="docs_reports_sales_documents_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os Relatórios de Vendas</h3>
    </div>
</div>

<div class="card collapse" id="docs_reports_sales_documents_collapse">
    <div class="card-body">
        <table id="docs_reports_sales_documents_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>serie</th>
                    <th>employeecode</th>
                    <th>vendorcode</th>
                    <th>customertaxid</th>
                    <th>documentnumber</th>
                    <th>canceled</th>
                    <th>updatedstocks</th>
                    <th>updatedcurrentaccount</th>
                    <th>debit</th>
                    <th>credit</th>
                    <th>documentname</th>
                    <th>documenttype</th>
                    <th>generaltype</th>
                    <th>number</th>
                    <th>idcustomer</th>
                    <th>customername</th>
                    <th>employeename</th>
                    <th>vendorname</th>
                    <th>date</th>
                    <th>datedue</th>
                    <th>total</th>
                    <th>totalwithoutvat</th>
                    <th>valuevat</th>
                    <th>valuepaid</th>
                    <th>valuediscount</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop




@stop