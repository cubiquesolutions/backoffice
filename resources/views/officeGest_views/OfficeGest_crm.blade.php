@extends('layouts.app', ['page' => __('OfficeGest'), 'pageSlug' => 'OfficeGest_crm'])

@extends('officeGest_views.tables.crm_appointments_table');
@extends('officeGest_views.tables.crm_businessareas_table');
@extends('officeGest_views.tables.crm_businessopportunities_table');
@extends('officeGest_views.tables.crm_campaigns_table');
@extends('officeGest_views.tables.crm_phases_table');

@extends('officeGest_views.methods.loading');

{{-- SCRIPTS --}}
@extends('layouts.scripts');

@section('content')

<div class="card">

    <div class="alert alert-default" role="alert" style="text-align: center;"><b>OfficeGest</b></div>
    <div class="alert alert-warning fade show" role="alert" style="text-align: center;">
        <strong>Dica:</strong> Use o <strong>Google Chrome</strong><br>
        <strong>Dica:</strong> Tecla <strong>F11</strong> para uma melhor experiência.<br>
        <strong>Dica:</strong> Clique com o rato dentro da tabela e navegue com as <strong>SETAS do teclado</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="tim-icons icon-simple-remove"></i>
        </button>
    </div>

</div>

{{-- businessopportunities_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#businessopportunities_collapse" role="button" aria-expanded="false" aria-controls="businessopportunities_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Oportunidades de Negócio</h3>
    </div>
</div>

<div class="card collapse" id="businessopportunities_collapse">
    <div class="card-body">
        <table id="businessopportunities_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                    <th>phase</th>
                    <th>businessareas</th>
                    <th>date_start</th>
                    <th>date_end_expected</th>
                    <th>employeecode</th>
                    <th>vendorcode</th>
                    <th>entitycode</th>
                    <th>entitytype</th>
                    <th>campaigncode</th>
                    <th>done</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- crm_businessareas_collapse --}}
<div class="card" data-toggle="collapse" href="#crm_businessareas_collapse" role="button" aria-expanded="false" aria-controls="crm_businessareas_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Áreas de Negócio</h3>
    </div>
</div>

<div class="card collapse" id="crm_businessareas_collapse">
    <div class="card-body">
        <table id="crm_businessareas_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- crm_phases_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#crm_phases_collapse" role="button" aria-expanded="false" aria-controls="crm_phases_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Fases de Negócio</h3>
    </div>
</div>

<div class="card collapse" id="crm_phases_collapse">
    <div class="card-body">
        <table id="crm_phases_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- crm_appointments_collapse --}}
<div class="card" data-toggle="collapse" href="#crm_appointments_collapse" role="button" aria-expanded="false" aria-controls="crm_appointments_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Agendamentos</h3>
    </div>
</div>

<div class="card collapse" id="crm_appointments_collapse">
    <div class="card-body">
        <table id="crm_appointments_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                    <th>local</th>
                    <th>entitytype</th>
                    <th>entitycode</th>
                    <th>employeecode</th>
                    <th>campaigncode</th>
                    <th>businessopportunity</th>
                    <th>done</th>
                    <th>equipment</th>
                    <th>priority</th>
                    <th>startdate</th>
                    <th>enddate</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- crm_campaigns_collapse --}}
{{-- <div class="card">
    <div class="card-body" style="cursor: pointer;">
        <h3 class="card-title" data-toggle="collapse" href="#crm_campaigns_collapse" role="button" aria-expanded="false"
            aria-controls="crm_campaigns_collapse">Listar as Campanhas</h3>
    </div>
</div>

<div class="card collapse" id="crm_campaigns_collapse">
    <div class="card-body">
        <table id="crm_campaigns_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>Sem campanhas</th>
                </tr>
            </thead>
        </table>
    </div>
</div> --}}

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop




@stop