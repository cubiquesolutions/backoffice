<script>
function deleteEquipKey($id) {

Swal.fire({
        title: "Tem a certeza?",
        text: "Uma vez apagado, não pode recuperar!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, apagar!',
    })
    .then((result) => {
        if (result.value) {
            axios.delete('/equipment_keys/delete/' + $id)
                .then(response => {

                    Swal.fire({
                        icon: 'success',
                        title: 'Apagado!',

                        allowOutsideClick: false,
                        showConfirmButton: true,
                        confirmButtonText: '<i class="fa fa-thumbs-up"></i> Obrigado!',

                        preConfirm: () => {
                            window.location.href = '/officegest/equipment_keys/';
                        }
                    })

                })
                .catch(error => {
                    Swal.fire(
                        'Erro',
                        'A chave escolhida não foi apagado!"',
                        'error'
                    );
                });
        }
    });
};

function createEquipKey() {

var html_content = '<label for="number_of_keys">Número de chaves a criar: (max. 200)</label><br>' +
    '<input type="number" id="number_of_keys" name="number_of_keys" min="1" max="200" value="1">';

Swal.fire({
        title: "Tem a certeza?",
        text: "A seguinte ação irá gerar uma nova chave!",
        icon: "warning",

        html: html_content,

        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, criar!',
    })
    .then((result) => {
        if (result.value) {
            if (document.getElementById("number_of_keys").value > 0 && document.getElementById("number_of_keys").value <= 200) {

                axios.get('/equipment_keys/create/', {
                        params: {
                            dataFromClient: document.getElementById("number_of_keys").value,
                        }
                    })
                    .then(response => {

                        Swal.fire({
                            icon: 'success',
                            title: 'Chave criada!',

                            allowOutsideClick: false,
                            showConfirmButton: true,
                            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Obrigado!',

                            preConfirm: () => {
                                window.location.href = '/officegest/equipment_keys/';
                            }
                        })

                    })
                    .catch(error => {
                        Swal.fire(
                            'Erro',
                            'Uma nova chave não foi criada!',
                            'error'
                        );
                    });
            } else {
                Swal.fire(
                    'Erro',
                    'Intervalo não respeitado!',
                    'error'
                );
            }
        }
    });
};

function removeNotUsedKeys() {

var html_content = '<label for="pass">Palavra Ultra Mega Secreta: </label>' +
    '<input type="text" id="pass" name="pass"><br><br>';

Swal.fire({
        title: "Tem a certeza?",
        text: "Uma vez apagado, não pode recuperar!",
        icon: "warning",

        html: html_content,

        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, apagar não utilizados!',
    })
    .then((result) => {
        if (result.value) {
            if (document.getElementById("pass").value == "ultramegasecreta") {
                axios.get('/equipment_keys/delete_unused/')
                    .then(response => {

                        Swal.fire({
                            icon: 'success',
                            title: 'Apagado!',

                            allowOutsideClick: false,
                            showConfirmButton: true,
                            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Obrigado!',

                            preConfirm: () => {
                                window.location.href = '/officegest/equipment_keys/';
                            }
                        })

                    })
                    .catch(error => {
                        Swal.fire(
                            'Erro',
                            'As chaves não foram apagadas!"',
                            'error'
                        );
                    });
            } else {
                Swal.fire(
                    'Erro',
                    'Problema de palavra chave!',
                    'error'
                );
            }
        }
    });
};

function exportCSV() {
window.open(
    '/equipment_keys/exportCSV',
    '_blank'
);
};

function exportCSV_quebra_de_linha() {
window.open(
    '/equipment_keys/exportCSV_quebra_de_linha',
    '_blank'
);
};

function update_isUsed() {
axios.get('/equipment_keys/update_isUsed/')
    .then(response => {

        Swal.fire({
            icon: 'success',
            title: 'Estado das Chaves Atualizado!',

            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Obrigado!',

            preConfirm: () => {
                window.location.href = '/officegest/equipment_keys/';
            }
        })

    })
    .catch(error => {
        Swal.fire(
            'Erro',
            'As chaves não foram apagadas!"',
            'error'
        );
    });
};
</script>