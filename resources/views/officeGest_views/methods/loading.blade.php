<script>

    function timer(){
    
        let timerInterval
        Swal.fire({
        title: 'A Sincronizar',
        html: 'Carregando os dados do OfficeGest...',
        timer: 10000,
        timerProgressBar: true,
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal.showLoading()
            // timerInterval = setInterval(() => {
            // Swal.getContent().querySelector('b').textContent = Swal.getTimerLeft()
            // }, 100)
        },
        onClose: () => {
            clearInterval(timerInterval)
        }
        }).then((result) => {
        if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.timer
        ) {
            // console.log('I was closed by the timer');
        }
        })
    };
    
    window.onload = timer;
    
</script>