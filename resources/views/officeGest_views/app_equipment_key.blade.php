@extends('layouts.app', ['page' => __('Gerador de Chaves'), 'pageSlug' => 'equipmentKeys'])

@extends('officeGest_views.methods.equip_key');

{{-- SCRIPTS --}}
@extends('layouts.scripts');

@section('content')

<div class="card">

    <div class="alert alert-default" role="alert" style="text-align: center;"><b>Gerador de Chaves Únicas</b></div>

    <div class="row">
        <div class="col-sm-4" onclick="createEquipKey()" style="cursor:pointer">
            <div class="card">
                <div class="card-body bg-success">
                    <h2 class="card-title">Criar novas chaves Únicas</h2>
                    <p class="card-text">Código numérico de 8 caractéres.</p>
                </div>
            </div>
        </div>

        <div class="col-sm-4" onclick="update_isUsed()" style="cursor:pointer">
            <div class="card">
                <div class="card-body bg-warning">
                    <h2 class="card-title">Atualizar as Chaves Utilizadas</h2>
                    <p class="card-text">Consulta a B.D. do OfficeGest e atuliza o ESTADO das chaves.</p>
                </div>
            </div>
        </div>

        <div class="col-sm-4" onclick="removeNotUsedKeys()" style="cursor:pointer">
            <div class="card">
                <div class="card-body bg-danger">
                    <h2 class="card-title">Remover Chaves não utilizadas</h2>
                    <p class="card-text">Consulta a B.D. do OfficeGest e limpa as chaves não utilizadas.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6" onclick="exportCSV()" style="cursor:pointer">
            <div class="card">
                <div class="card-body bg-primary">
                    <h2 class="card-title">Exporta todas as chaves Geradas</h2>
                    <p class="card-text">Consulta a B.D. do Backoffice e exporta para CSV.</p>
                </div>
            </div>
        </div>

        <div class="col-sm-6" onclick="exportCSV_quebra_de_linha()" style="cursor:pointer">
            <div class="card">
                <div class="card-body bg-primary">
                    <h2 class="card-title">Exporta Chaves sem Equipamento</h2>
                    <p class="card-text">Gera um ficheiro CSV com Quebra de Linha entre cada chave livre.</p>
                </div>
            </div>
        </div>

    </div>

    <div class="card-body">
        <table id="equipment_keys" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>unique_key</th>
                    <th>is_used</th>
                    <th>created_by</th>
                    <th>created_at</th>
                </tr>
            </thead>

        </table>
    </div>

</div>

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop


<script>
    $(function() {
        $('#equipment_keys thead tr').clone(true).appendTo('#equipment_keys thead');
        
        $('#equipment_keys thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function (e) {

            if(e.keyCode == 13){
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
            }

            } );
        } );

var table = $('#equipment_keys').DataTable({
    
        // processing: true,
        // serverSide: true,
        
        ajax: '{!! route('getAllEquipment_Keys') !!}',
        
        "autoWidth": false,
        "scrollX": true,

        "order": [[ 1, "desc" ]],
        
        columns: [
            { data: 'unique_key', name: 'unique_key' },
            { data: 'isUsed', name: 'isUsed' },
            { data: 'created_by', name: 'created_by' },
            { data: 'created_at', name: 'created_at' },
        ],
    });
    
    var oldThis;
    var table2 = $('#equipment_keys').DataTable();
    $('#equipment_keys tbody').on( 'click', 'tr', function () {

        equipment_key = table2.row( this ).data();
        equipment_keyId = equipment_key["id"];

        rowId = table2.row( this ).index();

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table2.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

    // $('#equipment_keys tbody').on('dblclick', 'tr', function () {
    //     $pathTo = '/segmentedCustomers/';
    //     $myTable = "#equipment_keys";

    //     return editCustomer(equipment_keyId, $pathTo, $myTable);

    // } );

});


</script>

<style>
    /* Realçar as linhas da tabela quando passa com o rato */

    td.highlight {
        background-color: #ff8d72 !important;
    }

    /* fim realçar */
</style>

@stop