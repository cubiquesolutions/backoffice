@extends('layouts.app', ['page' => __('OfficeGest'), 'pageSlug' => 'OfficeGest_stocks'])

@extends('officeGest_views.tables.stocks_articles_table');
@extends('officeGest_views.tables.stocks_families_table');
@extends('officeGest_views.tables.stocks_subfamilies_table');
@extends('officeGest_views.tables.stocks_subsubfamilies_table');
@extends('officeGest_views.tables.stocks_brands_table');
@extends('officeGest_views.tables.stocks_units_table');

@extends('officeGest_views.methods.loading');

{{-- SCRIPTS --}}
@extends('layouts.scripts');

@section('content')

<div class="card">

    <div class="alert alert-default" role="alert" style="text-align: center;"><b>OfficeGest</b></div>
    <div class="alert alert-warning fade show" role="alert" style="text-align: center;">
        <strong>Dica:</strong> Use o <strong>Google Chrome</strong><br>
        <strong>Dica:</strong> Tecla <strong>F11</strong> para uma melhor experiência.<br>
        <strong>Dica:</strong> Clique com o rato dentro da tabela e navegue com as <strong>SETAS do teclado</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="tim-icons icon-simple-remove"></i>
        </button>
    </div>

</div>

{{-- stocks_articles_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#stocks_articles_collapse" role="button" aria-expanded="false" aria-controls="stocks_articles_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar os Artigos</h3>
    </div>
</div>

<div class="card collapse" id="stocks_articles_collapse">
    <div class="card-body">
        <table id="stocks_articles_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                    <th>articletype</th>
                    <th>purchasingprice</th>
                    <th>sellingprice</th>
                    <th>vatid</th>
                    <th>unit</th>
                    <th>stock_quantity</th>
                    <th>family</th>
                    <th>subfamily</th>
                    <th>barcode</th>
                    <th>brand</th>
                    <th>active</th>
                    <th>spaces_dimensions</th>
                    <th>activeforweb</th>
                    <th>alterationdate</th>
                    <th>re</th>
                    <th>idforweb</th>
                    <th>priceforweb</th>
                    <th>referenceforweb</th>
                    <th>long_description</th>
                    <th>short_description</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- stocks_families_collapse --}}
<div class="card" data-toggle="collapse" href="#stocks_families_collapse" role="button" aria-expanded="false" aria-controls="stocks_families_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Famílias</h3>
    </div>
</div>

<div class="card collapse" id="stocks_families_collapse">
    <div class="card-body">
        <table id="stocks_families_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- stocks_subfamilies_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#stocks_subfamilies_collapse" role="button" aria-expanded="false" aria-controls="stocks_subfamilies_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Sub Famílias</h3>
    </div>
</div>

<div class="card collapse" id="stocks_subfamilies_collapse">
    <div class="card-body">
        <table id="stocks_subfamilies_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                    <th>familyid</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- stocks_subsubfamilies_collapse --}}
<div class="card" data-toggle="collapse" href="#stocks_subsubfamilies_collapse" role="button" aria-expanded="false" aria-controls="stocks_subsubfamilies_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Sub Sub Famílias</h3>
    </div>
</div>

<div class="card collapse" id="stocks_subsubfamilies_collapse">
    <div class="card-body">
        <table id="stocks_subsubfamilies_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>subsubfamilyid</th>
                    <th>description</th>
                    <th>familyid</th>
                    <th>subfamilyid</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- stocks_brands_collapse --}}
<div class="card bg-info" data-toggle="collapse" href="#stocks_brands_collapse" role="button" aria-expanded="false" aria-controls="stocks_brands_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Marcas</h3>
    </div>
</div>

<div class="card collapse" id="stocks_brands_collapse">
    <div class="card-body">
        <table id="stocks_brands_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                    <th>observations</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- stocks_units_collapse --}}
<div class="card" data-toggle="collapse" href="#stocks_units_collapse" role="button" aria-expanded="false" aria-controls="stocks_units_collapse" style="cursor: pointer;">
    <div class="card-body">
        <h3 class="card-title">Listar as Unidades</h3>
    </div>
</div>

<div class="card collapse" id="stocks_units_collapse">
    <div class="card-body">
        <table id="stocks_units_table" style="width:100%; zoom:0.8; cursor:pointer"
            class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>id</th>
                    <th>description</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@stop