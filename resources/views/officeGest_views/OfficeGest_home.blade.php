@extends('layouts.app', ['page' => __('OfficeGest'), 'pageSlug' => 'OfficeGest_home'])

{{-- SCRIPTS --}}
@extends('layouts.scripts');

@section('content')

<div class="card">

    <div class="alert alert-default" role="alert" style="text-align: center;"><b>OfficeGest</b></div>
    <div class="alert alert-warning fade show" role="alert" style="text-align: center;">
        <strong>Dica:</strong> Use o <strong>Google Chrome</strong><br>
        <strong>Dica:</strong> Tecla <strong>F11</strong> para uma melhor experiência.<br>
        <strong>Dica:</strong> Clique com o rato dentro da tabela e navegue com as <strong>SETAS do teclado</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="tim-icons icon-simple-remove"></i>
        </button>
    </div>

</div>



<div class="row">
    <div class="col-sm-6">
        <a href="{{ route('pages.OfficeGest_entities') }}">
            <div class="card">
                <div class="card-body bg-info">
                    <h2 class="card-title">Entidades</h2>
                    <p class="card-text">Acesso aos Clientes, Prospecções, Fornecedores, Empregados e Comerciais.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6">
        <a href="{{ route('pages.OfficeGest_stocks') }}">
            <div class="card">
                <div class="card-body bg-dark">
                    <h2 class="card-title">Stocks</h2>
                    <p class="card-text">Acesso aos Stocks, Famílias, Marcas e Unidades</p>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <a href="{{ route('pages.OfficeGest_crm') }}">
            <div class="card">
                <div class="card-body bg-dark">
                    <h2 class="card-title">CRM</h2>
                    <p class="card-text">Acesso às Oportundiades de Negócio, Áreas de Negócio, Fases, Agendamentos e Campanhas.</p>
                </div>
            </div>
        </a>
    </div>
    
    <div class="col-sm-6">
        <a href="{{ route('pages.OfficeGest_docs') }}">
            <div class="card">
                <div class="card-body bg-info">
                    <h2 class="card-title">Documentos</h2>
                    <p class="card-text">Acessos aos Documentos de Compra, Venda e Recibos.</p>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <a href="{{ route('pages.equipmentKeys') }}">
            <div class="card">
                <div class="card-body bg-danger">
                    <h2 class="card-title">Gerador de Chaves</h2>
                    <p class="card-text">Cria chaves únicas para os equipamentos.</p>
                </div>
            </div>
        </a>
    </div>

</div>



@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop


@stop