Olá <strong>{{ $to_name }}</strong>,
<br>
<p>Está agendado uma nova reunião com o cliente <strong>{{  $arr["Entity_name"]  }}</strong> em <strong>{{  $arr["Date_meeting"]  }}</strong></p>
<table>
    <tr bgcolor="#acd4e8">
        <th align="left">Comercial</th>
        <td>{{  $arr["Cubique_Employee"]  }}</td>
    </tr>
    <tr bgcolor="#92cbe8">
        <th align="left">Data do primeiro contacto</th>
        <td>{{  $arr["Date_first_approach"]  }}</td>
    </tr>
    <tr bgcolor="#92cbe8">
        <th align="left">Data do agendamento do contacto</th>
        <td>{{  $arr["Date_postponed_approach"]  }}</td>
    </tr>
    <tr bgcolor="#6ebde6">
        <th align="left">Nome da Entidade</th>
        <td>{{  $arr["Entity_name"]  }}</td>
    </tr>
    <tr bgcolor="#6ebde6">
        <th align="left">Email da Entidade</th>
        <td>{{  $arr["Entity_email"]  }}</td>
    </tr>
    <tr bgcolor="#6ebde6">
        <th align="left">Telefone da Entidade</th>
        <td>{{  $arr["Entity_phone_number"]  }}</td>
    </tr>
    <tr bgcolor="#47afe6">
        <th align="left">Nome do Cliente</th>
        <td>{{  $arr["Customer_name"]  }}</td>
    </tr>
    <tr bgcolor="#47afe6">
        <th align="left">Email do Cliente</th>
        <td>{{  $arr["Customer_email"]  }}</td>
    </tr>
    <tr bgcolor="#47afe6">
        <th align="left">Telefone do Cliente</th>
        <td>{{  $arr["Customer_phone_number"]  }}</td>
    </tr>
    <tr bgcolor="#56aad6">
        <th align="left">Distrito</th>
        <td>{{  $arr["District"]  }}</td>
    </tr>
    <tr bgcolor="#56aad6">
        <th align="left">Cidade</th>
        <td>{{  $arr["City"]  }}</td>
    </tr>
    <tr bgcolor="#56aad6">
        <th align="left">Localidade</th>
        <td>{{  $arr["Place"]  }}</td>
    </tr>
    <tr bgcolor="#0098e8">
        <th align="left">Produto</th>
        <td>{{  $arr["attributes_1"]  }}</td>
    </tr>
    <tr bgcolor="#0098e8">
        <th align="left">Área de negócio</th>
        <td>{{  $arr["attributes_2"]  }}</td>
    </tr>
    <tr bgcolor="#0098e8">
        <th align="left">Marca</th>
        <td>{{  $arr["attributes_3"]  }}</td>
    </tr>
    <tr bgcolor="#0098e8">
        <th align="left">Atributo 4</th>
        <td>{{  $arr["attributes_4"]  }}</td>
    </tr>
    <tr bgcolor="#0098e8">
        <th align="left">Atributo 5</th>
        <td>{{  $arr["attributes_5"]  }}</td>
    </tr>
    <tr bgcolor="#0098e8">
        <th align="left">Atributo 6</th>
        <td>{{  $arr["attributes_6"]  }}</td>
    </tr>
    <tr bgcolor="#0098e8">
        <th align="left">Setor de Atividade</th>
        <td>{{  $arr["attributes_7"]  }}</td>
    </tr>
    <tr bgcolor="#0098e8">
        <th align="left">Estado do contacto</th>
        <td>{{  $arr["attributes_8"]  }}</td>
    </tr>
    <tr bgcolor="#22668a">
        <th align="left">Comentário</th>
        <td>{{  $arr["Comments"]  }}</td>
    </tr>
    <tr bgcolor="#eba834">
        <th align="left">Data da reunião</th>
        <td>{{  $arr["Date_meeting"]  }}</td>
    </tr>
</table>

<br>
<p>------------</p>
<p>Mensagem automática enviada por <strong>{{ $from_name }}</strong></p>