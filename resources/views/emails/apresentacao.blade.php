@if ($current_hours_Time >= 6 && $current_hours_Time <= 12)
    <p>Bom dia,</p>

@elseif ($current_hours_Time > 12 && $current_hours_Time < 19)
    <p>Boa tarde,</p>
@else
    <p>Boa noite,</p>
@endif

<p>No seguimento do meu contacto telefónico e conforme combinado, envio em anexo uma breve apresentação da empresa Cubique.</p>
<p>Somos uma empresa especializada em soluções de impressão profissional à medida das suas necessidades, trabalhamos com as principais marcas e fabricantes de tecnologias de impressão.</p>
<p>Dispomos de consumíveis originais, assim como de alternativos da gama Standart e Premium, das mais conceituadas marcas do mercado.</p>
<p>Apresento-lhe também o nosso catálogo de Economato clicando na imagem seguinte, de modo a complementar o seu escritório. (151 megas!)

<a href="https://cubique.pt/wp-content/uploads/2019/07/Catalogo_Cubique_Economato_2020.pdf">
<img src="data:image/png;base64,{{base64_encode(file_get_contents($pdf_icon))}}" alt="Catalogo Economato - Cubique (151 megas!)">
</a>

</p>


<br>
<p>Desde já, agradeço a sua atenção e fico ao dispor para qualquer esclarecimento.</p>
<p>Com os melhores cumprimentos.</p>

<br>
<img src="data:image/png;base64,{{base64_encode(file_get_contents($signature_path))}}" alt="assinatura">