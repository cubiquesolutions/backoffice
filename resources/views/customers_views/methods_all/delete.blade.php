<script>
    function deleteCustomer($id, pathTo) {
        
            Swal.fire({
                    title: "Tem a certeza?",
                    text: "Uma vez apagado, não pode recuperar!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim, apagar!',
                })
                .then((result) => {
                    if (result.value) {
                        axios.delete(pathTo + 'delete/' + $id)
                            .then(response => {

                                Swal.fire({
                                icon: 'success',
                                title: 'Apagado!',
                                showConfirmButton: true,
                                confirmButtonText:
                                '<i class="fa fa-thumbs-up"></i> Recarregar a página!',
                                    preConfirm: () => {
                                            window.location.href = pathTo;
                                    }
                                }
                                )

                            })
                            .catch(error => {
                                Swal.fire(
                                'Erro',
                                'O Cliente escolhido não foi apagado!"',
                                'error'
                                );
                            });
                    }
                });
        };
</script>