<script>
    function createCustomer(pathTo) {

        htmlCode = '';
        optionsListCode = '';

        optionsListAttributes1 = '';
        optionsListAttributes2 = '';
        optionsListAttributes3 = '';
        optionsListAttributes4 = '';
        optionsListAttributes5 = '';
        optionsListAttributes6 = '';
        optionsListAttributes7 = '';
        optionsListAttributes8 = '';

        optionsListCountries = '';


        var options = {!! json_encode($cubiqueUsersList->toArray()) !!};
        var allCountries = {!! json_encode($countriesList->toArray()) !!};

        var allAttributes1 = {!! json_encode($attributesList['atr1']->toArray()) !!};
        var allAttributes2 = {!! json_encode($attributesList['atr2']->toArray()) !!};
        var allAttributes3 = {!! json_encode($attributesList['atr3']->toArray()) !!};
        var allAttributes4 = {!! json_encode($attributesList['atr4']->toArray()) !!};
        var allAttributes5 = {!! json_encode($attributesList['atr5']->toArray()) !!};
        var allAttributes6 = {!! json_encode($attributesList['atr6']->toArray()) !!};
        var allAttributes7 = {!! json_encode($attributesList['atr7']->toArray()) !!};
        var allAttributes8 = {!! json_encode($attributesList['atr8']->toArray()) !!};

        var logged_user_name = {!! json_encode($logged_user_name) !!};

        options.forEach(function(element) {
                optionsListCode += '<option value="' + element.name + '">' + element.name + '</option>';
        });

        optionsListCode += '<option selected="selected" value="' + logged_user_name + '">' + logged_user_name + '</option>'; 

        /* POPULAR SELECTS DE PAÍSES */
        allCountries.forEach(function(element) {
                    optionsListCountries += '<option value="' + element.name + '">' + element.name + '</option>';
            });

        /* POPULAR SELECTS DE ATRIBUTOS */
            allAttributes1.forEach(function(element) {
                    optionsListAttributes1 += '<option value="' + element.name + '">' + element.name + '</option>';
            });

            allAttributes2.forEach(function(element) {
                    optionsListAttributes2 += '<option value="' + element.name + '">' + element.name + '</option>';
            });

            allAttributes3.forEach(function(element) {
                    optionsListAttributes3 += '<option value="' + element.name + '">' + element.name + '</option>';
            });

            allAttributes4.forEach(function(element) {
                    optionsListAttributes4 += '<option value="' + element.name + '">' + element.name + '</option>';
            });

            allAttributes5.forEach(function(element) {
                    optionsListAttributes5 += '<option value="' + element.name + '">' + element.name + '</option>';
            });

            allAttributes6.forEach(function(element) {
                    optionsListAttributes6 += '<option value="' + element.name + '">' + element.name + '</option>';
            });

            allAttributes7.forEach(function(element) {
                    optionsListAttributes7 += '<option value="' + element.name + '">' + element.name + '</option>';
            });

            allAttributes8.forEach(function(element) {
                    optionsListAttributes8 += '<option value="' + element.name + '">' + element.name + '</option>';
            });
        
        Swal.fire({

        // icon: 'info',
        title: 'Criar Contacto',
        text: 'Preencha os campos',

        width: 1000,

        confirmButtonText: 'Criar novo contacto',
        
        showConfirmButton: true,
        showCloseButton: true,
        showCancelButton: true,

        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        background: '#1e1e2f',

        html:
        '<div style="overflow-y: auto; height: 600px; zoom:0.6">' +

'<div class="row">' +

    '<div class="col fundoA">' +

        '<div class="row">' +
            '<label class="labelTitles" for="swal-Cubique_Employee">Comercial</label>' +
            '<select class="swal2-input inputForms" id="swal-Cubique_Employee">' +
                    optionsListCode +
            '</select>' +
        '</div>' +

        '<div class="row">' +
            '<label class="labelTitles" for="swal-Date_first_approach">Data do Primeiro Contacto</label>' +
            '<input id="swal-Date_first_approach" class="swal2-input inputForms" placeholder="Data do primeiro contacto" type="datetime-local">' +
        '</div>' +
        
        '<div class="row">' +
            '<label class="labelTitles" for="swal-Date_postponed_approach">Data agendada</label>' +
            '<input id="swal-Date_postponed_approach" class="swal2-input inputForms" placeholder="Data agendada" type="datetime-local">' +
        '</div>' +

 '</div>' +



'<div class="col fundoA">' +

    '<div class="row">' +
        '<label class="labelTitles" for="swal-Entity_name">Nome da Entidade</label>' +
        '<input id="swal-Entity_name" class="swal2-input inputForms" placeholder="Nome da Entidade">' +
    '</div>' +

    '<div class="row">' + 
        '<label class="labelTitles" for="swal-Entity_phone_number">Telefone da Entidade</label>' +
        '<input id="swal-Entity_phone_number" class="swal2-input inputForms" placeholder="Telefone da Entidade">' +
    '</div>' +

    '<div class="row">' +
            '<label class="labelTitles" for="swal-Entity_email">Email da Entidade</label>' +
            '<input id="swal-Entity_email" class="swal2-input inputForms" placeholder="Email da Entidade" type="email">' +
    '</div>' +

'</div>' +


'<div class="col fundoA">' +

    '<div class="row">' +
        '<label class="labelTitles" for="swal-Customer_name">Nome do Cliente</label>' +
        '<input id="swal-Customer_name" class="swal2-input inputForms" placeholder="Nome do Cliente">' +
    '</div>' +

    '<div class="row">' + 
        '<label class="labelTitles" for="swal-Customer_phone_number">Telefone do Cliente</label>' +
        '<input id="swal-Customer_phone_number" class="swal2-input inputForms" placeholder="Telefone do Cliente">' +
    '</div>' +

    '<div class="row">' +
            '<label class="labelTitles" for="swal-Customer_email">Email do Cliente</label>' +
            '<input id="swal-Customer_email" class="swal2-input inputForms" placeholder="Email do Cliente" type="email">' +
    '</div>' +

'</div>' +


'</div>' +



/* ENDEREÇO */
'<div class="row fundoB">' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-Country">País</label>' +
    '<select class="swal2-input inputForms" id="swal-Country">' +
            optionsListCountries +
    '</select>' +
'</div>' +

'<div class="col">' +
'<label class="labelTitles" for="swal-District">Distrito</label>' +
'<input id="swal-District" class="swal2-input inputForms" placeholder="Distrito">' +
'</div>' +

'<div class="col">' +
'<label class="labelTitles" for="swal-City">Cidade</label>' +
'<input id="swal-City" class="swal2-input inputForms" placeholder="Cidade">' +
'</div>' +
'<div class="col">' +
'<label class="labelTitles" for="swal-Place">Localidade</label>' +
'<input id="swal-Place" class="swal2-input inputForms" placeholder="Localidade">' +
'</div>' +
'<div class="col">' +
'<label class="labelTitles" for="swal-Postal_code">Código Postal</label>' +
'<input id="swal-Postal_code" class="swal2-input inputForms" placeholder="Código Postal" pattern="[0-9]{4}-[0-9]{3}">' +
'</div>' +
'</div>' +

// comentarios

'<div class="row fundoB">' +
'<div class="col">' +
'<label class="labelTitles" for="swal-Comments">Comentários</label>' +
'<textarea style="height: 200px;" id="swal-Comments" class="swal2-input inputForms" placeholder="Comentários"></textarea>' +
'</div>' +
'</div>' +

// reunião com o cliente

'<div class="row fundoC">' +
'<div class="col">' +
'<label class="labelTitles" for="swal-Date_meeting">Reunião com o Cliente</label>' +
'<input id="swal-Date_meeting" class="swal2-input inputForms" placeholder="Reunião com o Cliente<" type="datetime-local">' +
'</div>' +
'</div>' +

/* ATRIBUTOS 1-4 */
'<div class="row fundoA">' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-attributes_1">Produto</label>' +
    '<select class="swal2-input inputForms" id="swal-attributes_1">' +
            optionsListAttributes1 +
    '</select>' +
'</div>' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-attributes_2">Área de Negócio</label>' +
    '<select class="swal2-input inputForms" id="swal-attributes_2">' +
            optionsListAttributes2 +
    '</select>' +
'</div>' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-attributes_3">Marca Adquirida</label>' +
    '<select class="swal2-input inputForms" id="swal-attributes_3">' +
            optionsListAttributes3 +
    '</select>' +
'</div>' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-attributes_4">Atributo 4</label>' +
    '<select class="swal2-input inputForms" id="swal-attributes_4">' +
            optionsListAttributes4 +
    '</select>' +
'</div>' +
'</div>' +

/* ATRIBUTOS 5-8 */
'<div class="row fundoA">' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-attributes_5">Atributo 5</label>' +
    '<select class="swal2-input inputForms" id="swal-attributes_5">' +
            optionsListAttributes5 +
    '</select>' +
'</div>' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-attributes_6">Atributo 6</label>' +
    '<select class="swal2-input inputForms" id="swal-attributes_6">' +
            optionsListAttributes6 +
    '</select>' +
'</div>' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-attributes_7">Setor de Atividade</label>' +
    '<select class="swal2-input inputForms" id="swal-attributes_7">' +
            optionsListAttributes7 +
    '</select>' +
'</div>' +
'<div class="col">' +
    '<label class="labelTitles" for="swal-attributes_8">Estado do Contacto</label>' +
    '<select class="swal2-input inputForms" id="swal-attributes_8">' +
            optionsListAttributes8 +
    '</select>' +
'</div>' +
'</div>' +

'</div>',
            

        
        
        preConfirm: () => {

            axios({
            method: 'post',
            url: pathTo + '/create',
            data: {
                'Cubique_Employee':             document.getElementById("swal-Cubique_Employee").value,
                'Date_first_approach':          document.getElementById("swal-Date_first_approach").value,
                'Date_postponed_approach':      document.getElementById("swal-Date_postponed_approach").value,
                'Date_meeting':                 document.getElementById("swal-Date_meeting").value,
                'Entity_name':                  document.getElementById("swal-Entity_name").value,
                'Entity_email':                 document.getElementById("swal-Entity_email").value,
                'Entity_phone_number':          document.getElementById("swal-Entity_phone_number").value,
                'Customer_name':                document.getElementById("swal-Customer_name").value,
                'Customer_email':               document.getElementById("swal-Customer_email").value,
                'Customer_phone_number':        document.getElementById("swal-Customer_phone_number").value,
                'Country':                      document.getElementById("swal-Country").value,
                'District':                     document.getElementById("swal-District").value,
                'City':                         document.getElementById("swal-City").value,
                'Place':                        document.getElementById("swal-Place").value,
                'Postal_code':                  document.getElementById("swal-Postal_code").value,
                'Comments':                     document.getElementById("swal-Comments").value,
                'attributes_1':                 document.getElementById("swal-attributes_1").value,
                'attributes_2':                 document.getElementById("swal-attributes_2").value,
                'attributes_3':                 document.getElementById("swal-attributes_3").value,
                'attributes_4':                 document.getElementById("swal-attributes_4").value,
                'attributes_5':                 document.getElementById("swal-attributes_5").value,
                'attributes_6':                 document.getElementById("swal-attributes_6").value,
                'attributes_7':                 document.getElementById("swal-attributes_7").value,
                'attributes_8':                 document.getElementById("swal-attributes_8").value,
            },
            }).then(function (response) {
                Swal.fire({
                    icon: 'success',
                    title: 'Criado!',
                    showConfirmButton: true,
                    confirmButtonText:
                                '<i class="fa fa-thumbs-up"></i> Recarregar a página!',
                        preConfirm: () => {
                                window.location.href = pathTo;
                        }
                    }
                    )
            })
            .catch(function (error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo correu mal!',
                    })
                console.log(error);
            });
            },
        })

};
</script>

<style>
.inputForms {
    background-color: #d8c5eb;
}

.labelTitles {
    color: white;
}

/* .fundoA {
    background-color: teal;
}
.fundoB {
    background-color: LightSlateGrey;
} */

.fundoC {
        background-color: #eba834;
    }
</style>