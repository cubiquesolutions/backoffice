<script>
    function editCustomer($id, pathTo, myTable) {

        htmlCode = '';
        optionsListCode = '';

        optionsListAttributes1 = '';
        optionsListAttributes2 = '';
        optionsListAttributes3 = '';
        optionsListAttributes4 = '';
        optionsListAttributes5 = '';
        optionsListAttributes6 = '';
        optionsListAttributes7 = '';
        optionsListAttributes8 = '';

        optionsListCountries = '';

        var options = {!! json_encode($cubiqueUsersList->toArray()) !!};
        var allCountries = {!! json_encode($countriesList->toArray()) !!};

        var allAttributes1 = {!! json_encode($attributesList['atr1']->toArray()) !!};
        var allAttributes2 = {!! json_encode($attributesList['atr2']->toArray()) !!};
        var allAttributes3 = {!! json_encode($attributesList['atr3']->toArray()) !!};
        var allAttributes4 = {!! json_encode($attributesList['atr4']->toArray()) !!};
        var allAttributes5 = {!! json_encode($attributesList['atr5']->toArray()) !!};
        var allAttributes6 = {!! json_encode($attributesList['atr6']->toArray()) !!};
        var allAttributes7 = {!! json_encode($attributesList['atr7']->toArray()) !!};
        var allAttributes8 = {!! json_encode($attributesList['atr8']->toArray()) !!};

        updatedRow = '';
        dropdownEmailToCubique = '';

        var logged_user_name = {!! json_encode($logged_user_name) !!};
        var user_definido_na_lista = 0;

    axios.get(pathTo + 'edit/'  + $id)
    .then(response => {
        customerInfo = response.data;

    /* check null values */
            if (customerInfo.Cubique_Employee == null || customerInfo.Cubique_Employee == "null") {
                customerInfo.Cubique_Employee = '';
            };
            if (customerInfo.Date_first_approach == null || customerInfo.Date_first_approach == "null") {
                customerInfo.Date_first_approach = '';
            };
            if (customerInfo.Date_postponed_approach == null || customerInfo.Date_postponed_approach == "null") {
                customerInfo.Date_postponed_approach = '';
            };
            if (customerInfo.Date_meeting == null || customerInfo.Date_meeting == "null") {
                customerInfo.Date_meeting = '';
            };
            if (customerInfo.Entity_name == null || customerInfo.Entity_name == "null") {
                customerInfo.Entity_name = '';
            };
            if (customerInfo.Entity_email == null || customerInfo.Entity_email == "null") {
                customerInfo.Entity_email = '';
            };
            if (customerInfo.Entity_phone_number == null || customerInfo.Entity_phone_number == "null") {
                customerInfo.Entity_phone_number = '';
            };
            if (customerInfo.Customer_name == null || customerInfo.Customer_name == "null") {
                customerInfo.Customer_name = '';
            };
            if (customerInfo.Customer_email == null || customerInfo.Customer_email == "null") {
                customerInfo.Customer_email = '';
            };
            if (customerInfo.Customer_phone_number == null || customerInfo.Customer_phone_number == "null") {
                customerInfo.Customer_phone_number = '';
            };
            if (customerInfo.Country == null || customerInfo.Country == "null") {
                customerInfo.Country = '';
            };
            if (customerInfo.District == null || customerInfo.District == "null") {
                customerInfo.District = '';
            };
            if (customerInfo.City == null || customerInfo.City == "null") {
                customerInfo.City = '';
            };
            if (customerInfo.Place == null || customerInfo.Place == "null") {
                customerInfo.Place = '';
            };
            if (customerInfo.Postal_code == null || customerInfo.Postal_code == "null") {
                customerInfo.Postal_code = '';
            };
            if (customerInfo.Contact_is_in_process == null || customerInfo.Contact_is_in_process == "null") {
                customerInfo.Contact_is_in_process = '';
            };
            if (customerInfo.Comments == null || customerInfo.Comments == "null") {
                customerInfo.Comments = '';
            };
            if (customerInfo.attributes_1 == null || customerInfo.attributes_1 == "null") {
                customerInfo.attributes_1 = '';
            };
            if (customerInfo.attributes_2 == null || customerInfo.attributes_2 == "null") {
                customerInfo.attributes_2 = '';
            };
            if (customerInfo.attributes_3 == null || customerInfo.attributes_3 == "null") {
                customerInfo.attributes_3 = '';
            };
            if (customerInfo.attributes_4 == null || customerInfo.attributes_4 == "null") {
                customerInfo.attributes_4 = '';
            };
            if (customerInfo.attributes_5 == null || customerInfo.attributes_5 == "null") {
                customerInfo.attributes_5 = '';
            };
            if (customerInfo.attributes_6 == null || customerInfo.attributes_6 == "null") {
                customerInfo.attributes_6 = '';
            };
            if (customerInfo.attributes_7 == null || customerInfo.attributes_7 == "null") {
                customerInfo.attributes_7 = '';
            };
            if (customerInfo.attributes_8 == null || customerInfo.attributes_8 == "null") {
                customerInfo.attributes_8 = '';
            };

    /* end */

        if(customerInfo.Date_first_approach != null){
            customerInfo.Date_first_approach = moment(customerInfo.Date_first_approach).format("YYYY-MM-DDTkk:mm");
        }

        if(customerInfo.Date_postponed_approach != null){
            customerInfo.Date_postponed_approach = moment(customerInfo.Date_postponed_approach).format("YYYY-MM-DDTkk:mm");
        }

        if(customerInfo.Date_meeting != null){
            customerInfo.Date_meeting = moment(customerInfo.Date_meeting).format("YYYY-MM-DDTkk:mm");
        }

         /* POPULAR SELECTS DE COMERCIAIS CUBIQUE */
        options.forEach(function(element) {
            if(element.name != customerInfo.Cubique_Employee){
                optionsListCode += '<option value="' + element.name + '">' + element.name + '</option>'; 
            }
            if(element.name == customerInfo.Cubique_Employee){
                optionsListCode += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                user_definido_na_lista = 1;
            }
        });
        if(user_definido_na_lista == 0){
                optionsListCode += '<option selected="selected" value="' + logged_user_name + '">' + logged_user_name + '</option>'; 
        }


        options.forEach(function(element) {
            dropdownEmailToCubique += '<option value="' + element.name + '">' + element.name + '</option>';
        });

        /* POPULAR SELECTS DE PAÍSES */
        allCountries.forEach(function(element) {
                if(element.name != customerInfo.Country){
                    optionsListCountries += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListCountries += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });

        /* POPULAR SELECTS DE ATRIBUTOS */
            allAttributes1.forEach(function(element) {
                if(element.name != customerInfo.attributes_1){
                    optionsListAttributes1 += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListAttributes1 += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });

            allAttributes2.forEach(function(element) {
                if(element.name != customerInfo.attributes_2){
                    optionsListAttributes2 += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListAttributes2 += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });

            allAttributes3.forEach(function(element) {
                if(element.name != customerInfo.attributes_3){
                    optionsListAttributes3 += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListAttributes3 += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });

            allAttributes4.forEach(function(element) {
                if(element.name != customerInfo.attributes_4){
                    optionsListAttributes4 += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListAttributes4 += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });

            allAttributes5.forEach(function(element) {
                if(element.name != customerInfo.attributes_5){
                    optionsListAttributes5 += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListAttributes5 += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });

            allAttributes6.forEach(function(element) {
                if(element.name != customerInfo.attributes_6){
                    optionsListAttributes6 += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListAttributes6 += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });

            allAttributes7.forEach(function(element) {
                if(element.name != customerInfo.attributes_7){
                    optionsListAttributes7 += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListAttributes7 += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });

            allAttributes8.forEach(function(element) {
                if(element.name != customerInfo.attributes_8){
                    optionsListAttributes8 += '<option value="' + element.name + '">' + element.name + '</option>';
                }else{
                    optionsListAttributes8 += '<option selected="selected" value="' + element.name + '">' + element.name + '</option>'; 
                }
            });
        


    })

    .then(function(response) {


        Swal.fire({

        // icon: 'info',
        title: 'Editar Contacto',
        text: customerInfo.Entity_name,

        width: 1000,

        confirmButtonText: 'Gravar',
        
        showConfirmButton: true,
        showCloseButton: true,
        showCancelButton: true,

        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        background: '#1e1e2f',

        html:
                '<div style="overflow-y: auto; height: 600px; zoom:0.6">' +

                    '<div class="row">' +

                        '<div class="col fundoA">' +

                            '<div class="row">' +
                                '<label class="labelTitles" for="swal-Cubique_Employee">Comercial</label>' +
                                '<select class="swal2-input inputForms" id="swal-Cubique_Employee">' +
                                        optionsListCode +
                                '</select>' +
                            '</div>' +

                            '<div class="row">' +
                                '<label class="labelTitles" for="swal-Date_first_approach">Data do Primeiro Contacto</label>' +
                                '<input id="swal-Date_first_approach" class="swal2-input inputForms" placeholder="Data do primeiro contacto" type="datetime-local" value="'+ customerInfo.Date_first_approach +'">' +
                            '</div>' +
                            
                            '<div class="row">' +
                                '<label class="labelTitles" for="swal-Date_postponed_approach">Data agendada</label>' +
                                '<input id="swal-Date_postponed_approach" class="swal2-input inputForms" placeholder="Data agendada" type="datetime-local" value="'+ customerInfo.Date_postponed_approach +'">' +
                            '</div>' +

                     '</div>' +



                    '<div class="col fundoA">' +

                        '<div class="row">' +
                            '<label class="labelTitles" for="swal-Entity_name">Nome da Entidade</label>' +
                            '<input id="swal-Entity_name" class="swal2-input inputForms" placeholder="Nome da Entidade" value="'+ customerInfo.Entity_name +'">' +
                        '</div>' +
                    
                        '<div class="row">' + 
                            '<label class="labelTitles" for="swal-Entity_phone_number">Telefone da Entidade</label>' +
                            '<input id="swal-Entity_phone_number" class="swal2-input inputForms" placeholder="Telefone da Entidade" value="'+ customerInfo.Entity_phone_number +'">' +
                        '</div>' +
                
                        '<div class="row">' +
                                '<label class="labelTitles" for="swal-Entity_email">Email da Entidade</label>' +
                                '<input id="swal-Entity_email" class="swal2-input inputForms" placeholder="Email da Entidade" type="email" value="'+ customerInfo.Entity_email +'">' +
                        '</div>' +

                    '</div>' +


                    '<div class="col fundoA">' +

                        '<div class="row">' +
                            '<label class="labelTitles" for="swal-Customer_name">Nome do Cliente</label>' +
                            '<input id="swal-Customer_name" class="swal2-input inputForms" placeholder="Nome do Cliente" value="'+ customerInfo.Customer_name +'">' +
                        '</div>' +

                        '<div class="row">' + 
                            '<label class="labelTitles" for="swal-Customer_phone_number">Telefone do Cliente</label>' +
                            '<input id="swal-Customer_phone_number" class="swal2-input inputForms" placeholder="Telefone do Cliente" value="'+ customerInfo.Customer_phone_number +'">' +
                        '</div>' +

                        '<div class="row">' +
                                '<label class="labelTitles" for="swal-Customer_email">Email do Cliente</label>' +
                                '<input id="swal-Customer_email" class="swal2-input inputForms" placeholder="Email do Cliente" type="email" value="'+ customerInfo.Customer_email +'">' +
                        '</div>' +

                    '</div>' +


                    '</div>' +



                /* ENDEREÇO */
                '<div class="row fundoB">' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-Country">País</label>' +
                        '<select class="swal2-input inputForms" id="swal-Country">' +
                                optionsListCountries +
                        '</select>' +
                    '</div>' +

                    '<div class="col">' +
                    '<label class="labelTitles" for="swal-District">Distrito</label>' +
                    '<input id="swal-District" class="swal2-input inputForms" placeholder="Distrito" value="'+ customerInfo.District +'">' +
                    '</div>' +

                    '<div class="col">' +
                    '<label class="labelTitles" for="swal-City">Cidade</label>' +
                    '<input id="swal-City" class="swal2-input inputForms" placeholder="Cidade" value="'+ customerInfo.City +'">' +
                    '</div>' +
                    '<div class="col">' +
                    '<label class="labelTitles" for="swal-Place">Localidade</label>' +
                    '<input id="swal-Place" class="swal2-input inputForms" placeholder="Localidade" value="'+ customerInfo.Place +'">' +
                    '</div>' +
                    '<div class="col">' +
                    '<label class="labelTitles" for="swal-Postal_code">Código Postal</label>' +
                    '<input id="swal-Postal_code" class="swal2-input inputForms" placeholder="Código Postal" pattern="[0-9]{4}-[0-9]{3}" value="'+ customerInfo.Postal_code +'">' +
                    '</div>' +
                '</div>' +

                // comentarios

                '<div class="row fundoB">' +
                    '<div class="col">' +
                    '<label class="labelTitles" for="swal-Comments">Comentários</label>' +
                    '<textarea style="height: 200px;" id="swal-Comments" class="swal2-input inputForms" placeholder="Comentários">'+ customerInfo.Comments +'</textarea>' +
                    '</div>' +
                '</div>' +

                // reunião com o cliente

                '<div class="row fundoC">' +
                    '<div class="col">' +
                    '<label class="labelTitles" for="swal-Date_meeting">Reunião com o Cliente</label>' +
                    '<input id="swal-Date_meeting" class="swal2-input inputForms" placeholder="Reunião com o Cliente" type="datetime-local" value="'+ customerInfo.Date_meeting + '">' +
                    '</div>' +
                '</div>' +

                /* ATRIBUTOS 1-4 */
                '<div class="row fundoA">' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-attributes_1">Produto</label>' +
                        '<select class="swal2-input inputForms" id="swal-attributes_1">' +
                                optionsListAttributes1 +
                        '</select>' +
                    '</div>' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-attributes_2">Área de Negócio</label>' +
                        '<select class="swal2-input inputForms" id="swal-attributes_2">' +
                                optionsListAttributes2 +
                        '</select>' +
                    '</div>' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-attributes_3">Marca Adquirida</label>' +
                        '<select class="swal2-input inputForms" id="swal-attributes_3">' +
                                optionsListAttributes3 +
                        '</select>' +
                    '</div>' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-attributes_4">Atributo 4</label>' +
                        '<select class="swal2-input inputForms" id="swal-attributes_4">' +
                                optionsListAttributes4 +
                        '</select>' +
                    '</div>' +
                '</div>' +

                /* ATRIBUTOS 5-8 */
                '<div class="row fundoA">' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-attributes_5">Atributo 5</label>' +
                        '<select class="swal2-input inputForms" id="swal-attributes_5">' +
                                optionsListAttributes5 +
                        '</select>' +
                    '</div>' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-attributes_6">Atributo 6</label>' +
                        '<select class="swal2-input inputForms" id="swal-attributes_6">' +
                                optionsListAttributes6 +
                        '</select>' +
                    '</div>' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-attributes_7">Setor de Atividade</label>' +
                        '<select class="swal2-input inputForms" id="swal-attributes_7">' +
                                optionsListAttributes7 +
                        '</select>' +
                    '</div>' +
                    '<div class="col">' +
                        '<label class="labelTitles" for="swal-attributes_8">Estado do Contacto</label>' +
                        '<select class="swal2-input inputForms" id="swal-attributes_8">' +
                                optionsListAttributes8 +
                        '</select>' +
                    '</div>' +
                '</div>' +

                '</div>',
                

        
        
        preConfirm: () => {

            //atualizar somente a linha alterada
            updatedRow = $(myTable).DataTable().row(rowId).data();

                updatedRow.Cubique_Employee =              document.getElementById("swal-Cubique_Employee").value,
                updatedRow.Date_first_approach =           document.getElementById("swal-Date_first_approach").value,
                updatedRow.Date_postponed_approach =       document.getElementById("swal-Date_postponed_approach").value,
                updatedRow.Date_meeting =                  document.getElementById("swal-Date_meeting").value,
                updatedRow.Entity_name =                   document.getElementById("swal-Entity_name").value,
                updatedRow.Entity_email =                  document.getElementById("swal-Entity_email").value,
                updatedRow.Entity_phone_number =           document.getElementById("swal-Entity_phone_number").value,
                updatedRow.Customer_name =                 document.getElementById("swal-Customer_name").value,
                updatedRow.Customer_email =                document.getElementById("swal-Customer_email").value,
                updatedRow.Customer_phone_number =         document.getElementById("swal-Customer_phone_number").value,
                updatedRow.Country =                       document.getElementById("swal-Country").value,
                updatedRow.District =                      document.getElementById("swal-District").value,
                updatedRow.City =                          document.getElementById("swal-City").value,
                updatedRow.Place =                         document.getElementById("swal-Place").value,
                updatedRow.Postal_code =                   document.getElementById("swal-Postal_code").value,
                updatedRow.Comments =                      document.getElementById("swal-Comments").value,
                updatedRow.attributes_1 =                  document.getElementById("swal-attributes_1").value,
                updatedRow.attributes_2 =                  document.getElementById("swal-attributes_2").value,
                updatedRow.attributes_3 =                  document.getElementById("swal-attributes_3").value,
                updatedRow.attributes_4 =                  document.getElementById("swal-attributes_4").value,
                updatedRow.attributes_5 =                  document.getElementById("swal-attributes_5").value,
                updatedRow.attributes_6 =                  document.getElementById("swal-attributes_6").value,
                updatedRow.attributes_7 =                  document.getElementById("swal-attributes_7").value,
                updatedRow.attributes_8 =                  document.getElementById("swal-attributes_8").value,

            //atualizar a base de dados
            axios({
            method: 'post',
            url: pathTo + 'edit/' + $id + '/update',
            data: {
                'Cubique_Employee':             document.getElementById("swal-Cubique_Employee").value,
                'Date_first_approach':          document.getElementById("swal-Date_first_approach").value,
                'Date_postponed_approach':      document.getElementById("swal-Date_postponed_approach").value,
                'Date_meeting':                 document.getElementById("swal-Date_meeting").value,
                'Entity_name':                  document.getElementById("swal-Entity_name").value,
                'Entity_email':                 document.getElementById("swal-Entity_email").value,
                'Entity_phone_number':          document.getElementById("swal-Entity_phone_number").value,
                'Customer_name':                document.getElementById("swal-Customer_name").value,
                'Customer_email':               document.getElementById("swal-Customer_email").value,
                'Customer_phone_number':        document.getElementById("swal-Customer_phone_number").value,
                'Country':                      document.getElementById("swal-Country").value,
                'District':                     document.getElementById("swal-District").value,
                'City':                         document.getElementById("swal-City").value,
                'Place':                        document.getElementById("swal-Place").value,
                'Postal_code':                  document.getElementById("swal-Postal_code").value,
                'Comments':                     document.getElementById("swal-Comments").value,
                'attributes_1':                 document.getElementById("swal-attributes_1").value,
                'attributes_2':                 document.getElementById("swal-attributes_2").value,
                'attributes_3':                 document.getElementById("swal-attributes_3").value,
                'attributes_4':                 document.getElementById("swal-attributes_4").value,
                'attributes_5':                 document.getElementById("swal-attributes_5").value,
                'attributes_6':                 document.getElementById("swal-attributes_6").value,
                'attributes_7':                 document.getElementById("swal-attributes_7").value,
                'attributes_8':                 document.getElementById("swal-attributes_8").value,
            },
            }).then(function (response) {

                Swal.fire({
                    icon: 'success',
                    title: 'Atualizado!',

                    allowOutsideClick: false,
                    showConfirmButton: true,

                    confirmButtonText:
                                '<i class="fa fa-thumbs-up"></i> Fechar!',

                    footer:

                    '<div class="row">' +
                        '<div class="col">' +
                            '<select class="swal2-input" style="margin: auto;" id="swal-dropdownEmailToCubique">' +
                                                dropdownEmailToCubique +
                            '</select>' +
                        '</div>' +
                        '<div class="col">'+
                            '<button class="btn btn-xs btn-warning" type="button" onclick="send_this_contact_info_by_Email(updatedRow)">Email p/ Comercial</button></div>' +
                        '</div>' +
                        '</div>',

                    preConfirm: () => {
                            //atualizar a linha da tabela
                            $(myTable).DataTable().row(rowId).data(updatedRow).invalidate();
                        }
                    }
                    )
            })
            .catch(function (error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo correu mal!',
                    })
                console.log(error);
            });

            }
        })
        
    })
    .catch(function(error) {
        Swal.fire({
    icon: 'error',
    title: 'Erro!',
    text: 'Não foi possivel abrir o contacto!',
        })
        console.log(error);
    });

};


function send_this_contact_info_by_Email($updatedRow) {

    $nome_do_comercial = document.getElementById("swal-dropdownEmailToCubique").value;

axios.get('/segmentedCustomers/send_email', {
        params: {
            dataFromClient: $updatedRow,
            dataFromClient_comercial: $nome_do_comercial,
        }
    })
    .then(response => {

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 4000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
        Toast.fire({
            icon: 'success',
            title: 'Email enviado para o Comercial!'
        })

    })
    .catch(error => {
        Swal.fire(
            'Erro',
            'Não foi possivel enviar o email!',
            'error'
        );
    });

}



</script>

<style>
    /* .inputForms {
        background-color: #d8c5eb;
    } */

    .labelTitles {
        color: white;
    }

    .fundoA {
        background-color: '';
    }

    .fundoB {
        background-color: #54648b;
    }

    .fundoC {
        background-color: #eba834;
    }
</style>