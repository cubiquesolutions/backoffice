<script>

    function send_apresentacao_email($id) {

        axios.get('/send_email/apresentacao_email/' + $id)
            .then(response => {

                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 4000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
                Toast.fire({
                    icon: 'success',
                    title: 'Email enviado para o Cliente!'
                })

            })
            .catch(error => {
                Swal.fire(
                    'Erro',
                    'Não foi possivel enviar o email!',
                    'error'
                );
            });

    }

function menu_email_templates($id, $entity_name) {
    dropdownEmailTemplates = '';
    dropdownEmailTemplates += '<option value="' + 0 + '">' + "Apresentação Cubique" + '</option>';

    Swal.fire({
        // icon: 'warning',
        title: 'Enviar email p/ a Entidade',
        text: $entity_name,

        allowOutsideClick: true,

        showCancelButton: true,
        showConfirmButton: true,
        showCloseButton: true,

        confirmButtonText: '<i class="fa fa-paper-plane"> Enviar Email</i>',

        cancelButtonText: '<i class="fa fa-times"> Cancelar</i>',
        cancelButtonColor: '#d33',

        footer:

            '<div>' +
            'Escolha o modelo a enviar: ' +
            '<select class="swal2-input" style="margin: auto;" id="swal-dropdownEmailTemplates">' +
            dropdownEmailTemplates +
            '</select>' +
            '</div>',

        preConfirm: () => {
            option = document.getElementById("swal-dropdownEmailTemplates").value;

            if (option == 0) {
                send_apresentacao_email($id);
            }
        }
    });
}

</script>