<script>
        function exportToCSV(pathTo) {
        var arrJson = [];
    
        var array_of_selected_checkboxes = {
                "Countries"         : [],
                "Districts"         : [],
                "Cities"            : [],
                "Cubique_Employees" : [],
                "Attributes_1"      : [],
                "Attributes_2"      : [],
                "Attributes_3"      : [],
                "Attributes_4"      : [],
                "Attributes_5"      : [],
                "Attributes_6"      : [],
                "Attributes_7"      : [],
                "Attributes_8"      : [],
                "Contains_these_words"   : [],
        };
    
        var isAnyFilterChecked = false;
    
        /* JSON DE RESULTADOS DA SELEÇAO */
        var queryParameters = {};
        var attributes = [];
        var locations = [];
        queryParameters.attributes = attributes;
        queryParameters.locations = locations;
        /* FIM */
    
        var filterCustomerDataCountry = {!! json_encode($filterCustomerData['Country']->toArray()) !!};
        var filterCustomerDataDistrict = {!! json_encode($filterCustomerData['District']->toArray()) !!};
        var filterCustomerDataCity = {!! json_encode($filterCustomerData['City']->toArray()) !!};
        var filterCustomerDataCubiqueEmployees = {!! json_encode($filterCustomerData['Cubique_Employee']->toArray()) !!};
    
        var filterCustomerDataAttributes1 = {!! json_encode($filterCustomerData['Attributes_1']->toArray()) !!};
        var filterCustomerDataAttributes2 = {!! json_encode($filterCustomerData['Attributes_2']->toArray()) !!};
        var filterCustomerDataAttributes3 = {!! json_encode($filterCustomerData['Attributes_3']->toArray()) !!};
        var filterCustomerDataAttributes4 = {!! json_encode($filterCustomerData['Attributes_4']->toArray()) !!};
        var filterCustomerDataAttributes5 = {!! json_encode($filterCustomerData['Attributes_5']->toArray()) !!};
        var filterCustomerDataAttributes6 = {!! json_encode($filterCustomerData['Attributes_6']->toArray()) !!};
        var filterCustomerDataAttributes7 = {!! json_encode($filterCustomerData['Attributes_7']->toArray()) !!};
        var filterCustomerDataAttributes8 = {!! json_encode($filterCustomerData['Attributes_8']->toArray()) !!};
    
        var counter = 1;
        
    //==========================================================================================================================================================
    //
    //
    //
    //
    //
    //
    //
    //                                      INICIO BODY HTML DO MODAL
    //
    //
    //
    //
    //
    //
    //
    //
    //==========================================================================================================================================================
    
        var htmlCode = '<div style="overflow-y: auto; height:600px; zoom:0.8">';               // ABRE CONTAINER
    
    // FILTROS Cubique_Employee
    
        htmlCode += '<div class="row">' +                       // headers cubique employee
                        '<div class="col">' +
                        '<div class="card">' +
                        '<div class="card-header">Comercial Cubique</div>' +
                        '</div></div>' +

                        '<div class="col">' +
                        '<div class="card">' +
                        '<div class="card-header">Filtros Especiais</div>' +
                        '</div></div></div>';
    
                        htmlCode += '<div class="row">';                        // 1a fila cubique employee
    
    
    
                    htmlCode +=
                    '<div class="col scrolldiv" align="left">';
                    counter=1;
    
                    filterCustomerDataCubiqueEmployees.forEach(function(element) {
                    htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="cubique_employee_id' + counter + '"  />' + element.Cubique_Employee + '</h6><p/>';
                    counter++;
                    });
    
                    htmlCode += '</div>';                                        // FIM filtros coluna cubique employee

                    htmlCode +=
                    '<div class="col scrolldiv" align="left">';
                    counter=1;
    
                    htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="contains_geral"  />Email Geral</h6><p/>';
    
                    htmlCode += '</div>';                                        // FIM filtros coluna filtros especiais
                    
                    htmlCode += '</div>';                                        // FIM filtros linha cubique employee
    
    
    // FILTROS cubique district, city, country
       
        htmlCode += '<div class="row">' +                       // headers localidades
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">País</div>' +
                    '</div></div>' +
    
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Distrito</div>' +
                    '</div></div>' +
    
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Cidade</div>' +
                    '</div></div></div>';
    
                    htmlCode += '<div class="row">';                        // 1a fila e filtros cubique district, city, country
    
    
        //  country
        htmlCode +=
        '<div class="col scrolldiv" align="left">';              
        counter=1;
    
        filterCustomerDataCountry.forEach(function(element) {
        htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="country_id' + counter + '"  />' + element.Country + '</h6><p/>';
        counter++;
        });
    
        htmlCode += '</div>';
        // FIM
    
        // district
        htmlCode += 
        '<div class="col scrolldiv" align="left">';              
        counter=1;
    
        filterCustomerDataDistrict.forEach(function(element) {
        htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="district_id' + counter + '"  />' + element.District + '</h6><p/>';
        counter++;
        });
    
        htmlCode += '</div>';
        // FIM
    
    
        // city
        htmlCode += 
        '<div class="col scrolldiv" align="left">';              
        counter=1;
    
        filterCustomerDataCity.forEach(function(element) {
        htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="city_id' + counter + '"  />' + element.City + '</h6><p/>';
        counter++;
        });
    
        htmlCode += '</div>';
        // FIM
    
        htmlCode += '</div>';                                   // fechar 1 fila
        counter=1;                                              // reiniciar o contador usado para gerar os ids dos inputs
    
    
    
    
    
    
    
    
    
    
    
    
    // FILTROS ATRIBUTOS
    
        htmlCode += '<div class="row">' +                       // headers atributos
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Attributes 1</div>' +
                    '</div></div>' +
    
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Attributes 2</div>' +
                    '</div></div>' +
    
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Marca</div>' +
                    '</div></div></div>';
    
        htmlCode += '<div class="row">';                        // 1a fila e filtros
    
                // ATRIBUTOS 1
                counter=1;
                htmlCode +=
                '<div class="col scrolldiv" align="left">';             
    
                filterCustomerDataAttributes1.forEach(function(element) {
                htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="atr1_id' + counter + '"  />' + element.attributes_1 + '</h6><p/>';
                counter++;
                });
    
                htmlCode += '</div>';
                // FIM ATRIBUTOS 1
    
                // ATRIBUTOS 2
                counter=1;
                htmlCode += 
                '<div class="col scrolldiv" align="left">';             
    
                filterCustomerDataAttributes2.forEach(function(element) {
                htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="atr2_id' + counter + '"  />' + element.attributes_2 + '</h6><p/>';
                counter++;
                });
    
                htmlCode += '</div>';
                // FIM ATRIBUTOS 2
    
                
                // ATRIBUTOS 3
                counter=1;
                htmlCode += 
                '<div class="col scrolldiv" align="left">';             
    
                filterCustomerDataAttributes3.forEach(function(element) {
                htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="atr3_id' + counter + '"  />' + element.attributes_3 + '</h6><p/>';
                counter++;
                });
    
                htmlCode += '</div>';
                // FIM ATRIBUTOS 3
    
    
        htmlCode += '</div>';                                   // fechar 1 fila
    
    
        htmlCode += '<div class="row">' +
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Attributes 4</div>' +
                    '</div></div>' +
    
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Attributes 5</div>' +
                    '</div></div>' +
    
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Attributes 6</div>' +
                    '</div></div></div>';
    
    
    
                htmlCode += '<div class="row">';                        // 2a fila
    
                // ATRIBUTOS 4
                counter=1;
                htmlCode += 
                '<div class="col scrolldiv" align="left">';             
    
                filterCustomerDataAttributes4.forEach(function(element) {
                htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="atr4_id' + counter + '"  />' + element.attributes_4 + '</h6><p/>';
                counter++;
                });
    
                htmlCode += '</div>';
                // FIM ATRIBUTOS 4
    
                // ATRIBUTOS 5
                counter=1;
                htmlCode += 
                '<div class="col scrolldiv" align="left">';             
    
                filterCustomerDataAttributes5.forEach(function(element) {
                htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="atr5_id' + counter + '"  />' + element.attributes_5 + '</h6><p/>';
                counter++;
                });
    
                htmlCode += '</div>';
                // FIM ATRIBUTOS 5
    
    
                // ATRIBUTOS 6
                counter=1;
                htmlCode += 
                '<div class="col scrolldiv" align="left">';             
    
                filterCustomerDataAttributes6.forEach(function(element) {
                htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="atr6_id' + counter + '"  />' + element.attributes_6 + '</h6><p/>';
                counter++;
                });
    
                htmlCode += '</div>';
                // FIM ATRIBUTOS 6  
    
                htmlCode += '</div>';                                   // fechar 2fila
    
                htmlCode += '<div class="row">' +
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Setor de Atividade</div>' +
                    '</div></div>' +
    
                    '<div class="col">' +
                    '<div class="card">' +
                    '<div class="card-header">Estado do Contacto</div>' +
                    '</div></div></div>';
    
    
                htmlCode += '<div class="row">';                        // 3a fila
    
                    // ATRIBUTOS 7
                    counter=1;
                    htmlCode += 
                    '<div class="col scrolldiv" align="left">';             
    
                    filterCustomerDataAttributes7.forEach(function(element) {
                    htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="atr7_id' + counter + '"  />' + element.attributes_7 + '</h6><p/>';
                    counter++;
                    });
    
                    htmlCode += '</div>';
                // FIM ATRIBUTOS 7
    
                // ATRIBUTOS 8
                counter=1;
                    htmlCode += 
                    '<div class="col scrolldiv" align="left">';             
    
                    filterCustomerDataAttributes8.forEach(function(element) {
                    htmlCode += '<h6><input type="checkbox" style="margin-right: 10px" id="atr8_id' + counter + '"  />' + element.attributes_8 + '</h6><p/>';
                    counter++;
                    });
    
                    htmlCode += '</div>';
                // FIM ATRIBUTOS 8  
    
                htmlCode += '</div>';                                   // fechar 3 fila
        htmlCode += '</div>';                                   // fechar o <container>
    
    //==========================================================================================================================================================
    //
    //
    //
    //
    //
    //
    //
    //                                      FIM BODY HTML DO MODAL
    //
    //
    //
    //
    //
    //
    //
    //
    //==========================================================================================================================================================
    
        Swal.fire({
                // icon: 'info',
                title: 'Escolha as características do CSV a exportar!',
                width: 1000,
                html:   '<font color="white"><b>Irá exportar todos os contactos de cada categoria selecionada, somando a cada uma delas, e não devolve repetidos.</b></font>' + htmlCode, // checkboxes atributes 1
    
                confirmButtonText: 'Export',
                
                showConfirmButton: true,
                showCloseButton: true,
                showCancelButton: true,
    
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                background: '#1e1e2f',
    
                customClass: {
                    // container: 'sidebar-wrapper',
                },
    
                showLoaderOnConfirm: true,
    
    
              preConfirm: () => {
                  
                // Cubique Employee - Guarda os valores das seleções das Checkboxes no ARRAY
    
                    counter=1;
                    filterCustomerDataCubiqueEmployees.forEach(function(element) {
    
                        if(Swal.getPopup().querySelector('#cubique_employee_id' + counter).checked){
                            array_of_selected_checkboxes["Cubique_Employees"].push(element.Cubique_Employee);
                            isAnyFilterChecked = true;
                        }
    
                        counter++;
                    });
    
    
                // Country - Guarda os valores das seleções das Checkboxes no ARRAY
    
                    counter=1;
                    filterCustomerDataCountry.forEach(function(element) {
    
                            if(Swal.getPopup().querySelector('#country_id' + counter).checked){
                                array_of_selected_checkboxes["Countries"].push(element.Country);
                                isAnyFilterChecked = true;
                            }
    
                            counter++;
                        });
                
                
                // District - Guarda os valores das seleções das Checkboxes no ARRAY
    
                    counter=1;
                    filterCustomerDataDistrict.forEach(function(element) {
    
                            if(Swal.getPopup().querySelector('#district_id' + counter).checked){
                                array_of_selected_checkboxes["Districts"].push(element.District);
                                isAnyFilterChecked = true;
                            }
    
                            counter++;
                        });
                
    
                // City - Guarda os valores das seleções das Checkboxes no ARRAY
    
                    counter=1;
                    filterCustomerDataCity.forEach(function(element) {
    
                            if(Swal.getPopup().querySelector('#city_id' + counter).checked){
                                array_of_selected_checkboxes["Cities"].push(element.City);
                                isAnyFilterChecked = true;
                            }
    
                            counter++;
                        });
    
                // ATRIBUTOS - Guarda os valores das seleções das Checkboxes no ARRAY
    
                    counter=1;
                    filterCustomerDataAttributes1.forEach(function(element) {
                            if(Swal.getPopup().querySelector('#atr1_id' + counter).checked){
                                array_of_selected_checkboxes["Attributes_1"].push(element.attributes_1);
                                isAnyFilterChecked = true;
                            }
                            counter++;
                    });
    
                     counter=1;
                     filterCustomerDataAttributes2.forEach(function(element) {
                             if(Swal.getPopup().querySelector('#atr2_id' + counter).checked){
                                 array_of_selected_checkboxes["Attributes_2"].push(element.attributes_2);
                                 isAnyFilterChecked = true;
                             }
    
                             counter++;
                    });
    
                    counter=1;
                    filterCustomerDataAttributes3.forEach(function(element) {
                            if(Swal.getPopup().querySelector('#atr3_id' + counter).checked){
                                 array_of_selected_checkboxes["Attributes_3"].push(element.attributes_3);
                                 isAnyFilterChecked = true;
                             }
    
                             counter++;
                    });
    
                    counter=1;
                     filterCustomerDataAttributes4.forEach(function(element) {
                             if(Swal.getPopup().querySelector('#atr4_id' + counter).checked){
                                 array_of_selected_checkboxes["Attributes_4"].push(element.attributes_4);
                                 isAnyFilterChecked = true;
                             }
    
                             counter++;
                     });
    
                     counter=1;
                     filterCustomerDataAttributes5.forEach(function(element) {
                             if(Swal.getPopup().querySelector('#atr5_id' + counter).checked){
                                 array_of_selected_checkboxes["Attributes_5"].push(element.attributes_5);
                                 isAnyFilterChecked = true;
                             }
    
                             counter++;
                     });
    
                     counter=1;
                     filterCustomerDataAttributes6.forEach(function(element) {
                             if(Swal.getPopup().querySelector('#atr6_id' + counter).checked){
                                 array_of_selected_checkboxes["Attributes_6"].push(element.attributes_6);
                                 isAnyFilterChecked = true;
                             }
                             counter++;
                     });
    
                    
                     counter=1;
                     filterCustomerDataAttributes7.forEach(function(element) {
                             if(Swal.getPopup().querySelector('#atr7_id' + counter).checked){
                                 array_of_selected_checkboxes["Attributes_7"].push(element.attributes_7);
                                 isAnyFilterChecked = true;
                             }
                             counter++;
                     });
    
                    
                     counter=1;
                     filterCustomerDataAttributes8.forEach(function(element) {
                             if(Swal.getPopup().querySelector('#atr8_id' + counter).checked){
                                 array_of_selected_checkboxes["Attributes_8"].push(element.attributes_8);
                                 isAnyFilterChecked = true;
                             }
                             counter++;
                     });

                // FILTROS ESPECIAIS - Guardar no array
                 if(Swal.getPopup().querySelector('#contains_geral').checked){
                     array_of_selected_checkboxes["Contains_these_words"].push('Email Geral');
                     isAnyFilterChecked = true;
                 }

    
            // arrJson em formato JSON para ser lido no Controlador
    
                arrJson = JSON.stringify(array_of_selected_checkboxes);
                console.log("Filtros selecionados: " + arrJson);
    
              }
    
            }).then((result) => {
            if (result.value) {
            
                if (isAnyFilterChecked == true) {
                axios.get(pathTo + '/filterToCSV', {
                        params: {
                            dataFromClient: arrJson,
                        }
                    })
                    .then(function(response) {
                        Swal.fire({
                            icon: 'success',
                            title: 'A seleção foi exportada!',
                            text: 'Pode fazer o download com os filtros solicitados!',
                            confirmButtonText: 'Download',
    
                            preConfirm: () => {
                                window.location.href = pathTo + '/filterToCSV/download';
                            }
    
                        })
                        
                    })
                    .catch(function(error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Erro!',
                            text: 'Não foi possivel exportar. Tente novamente mais tarde.',
                        })
                        console.log(error);
                    });
    
            } // isAnyFilterChecked
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Sem opções selecionadas!',
                    text: 'Escolha no mínimo uma opção e volte a exportar.',
                });
            }        
    
            }
            })
    
    };
    </script>