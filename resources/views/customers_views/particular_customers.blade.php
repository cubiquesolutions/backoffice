@extends('layouts.app', ['page' => __('Clientes - Particulares'), 'pageSlug' => 'particularCustomers'])

@extends('customers_views.methods_all.create');
@extends('customers_views.methods_all.edit');
@extends('customers_views.methods_all.delete');
@extends('customers_views.methods_all.exportCSV');

{{-- SCRIPTS --}}
@extends('layouts.scripts');


@section('content')

<div class="card">

    <div class="alert alert-default" role="alert" style="text-align: center;"><b>Revenda - Particulares</b></div>
    <div class="alert alert-warning fade show" role="alert" style="text-align: center;">
        <strong>Dica:</strong> Use o <strong>Google Chrome</strong><br>
        <strong>Dica:</strong> Tecla <strong>F11</strong> para uma melhor experiência.<br>
        <strong>Dica:</strong> Clique com o rato dentro da tabela e navegue com as <strong>SETAS do teclado</strong><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="tim-icons icon-simple-remove"></i>
        </button>
    </div>

    <div class="row">
        <div class="col-sm-4" onclick="createCustomer('/particularCustomers')" style="cursor:pointer">
            <div class="card">
                <div class="card-body bg-info">
                    <h2 class="card-title">Criar novo Contacto</h2>
                    <p class="card-text">Reúna os dados tais como o nome, contacto telefónico, email e distrito.</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4" onclick="exportToCSV('/particularCustomers')" style="cursor:pointer">
            <div class="card">
                <div class="card-body bg-success">
                    <h2 class="card-title text-dark">Exportar</h2>
                    <p class="card-text text-dark">Exporte uma seleção no formato <b>CSV</b> com codificação de texto em <b>UTF 8</b>.</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4" onclick=open_FimContratoConcorrencia_OfficeGest() style="cursor:pointer">
            <div class="card">
                <div class="card-body bg-danger">
                    <h2 class="card-title text-dark">Fim de Contrato</h2>
                    <p class="card-text text-dark">Guardar no <b>OfficeGest</b> a data do fim de contrato do cliente com a concorrencia.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table id="customers_particular" style="width:100%; zoom:0.8; cursor:pointer" class="table table-bordered display nowrap">
            <thead>
                <tr>
                    <th>Opções</th>
                    <th>Comercial</th>
                    <th>Contacto Inicial</th>
                    <th>Agendado para...</th>
                    <th>Estado do Contacto</th> {{-- <th>Atributo 8</th> --}}
                    <th>Entidade</th>
                    <th>Telefone</th>
                    <th>Email</th>
                    <th>Comentários</th>
                    <th>Produto</th> {{-- <th>Atributo 1</th> --}}
                    <th>Área de Negócio</th> {{-- <th>Atributo 2</th> --}}
                    <th>Marca Adquirida</th> {{-- <th>Atributo 3</th> --}}
                    {{-- <th>Atributo 4</th>
                    <th>Atributo 5</th>
                    <th>Atributo 6</th> --}}
                    <th>Setor de Atividade</th>
                    <th>Cidade</th>
                    <th>Distrito</th>
                    <th>Reunião c/ cliente</th>
                </tr>
            </thead>

        </table>
    </div>

</div>

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop


<script>
    $(function() {
        $('#customers_particular thead tr').clone(true).appendTo('#customers_particular thead');
        
        $('#customers_particular thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function (e) {

            if(e.keyCode == 13){
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
            }

            } );
        } );

var table = $('#customers_particular').DataTable({

        // processing: true,
        // serverSide: true,
        ajax: '{!! route('getAllCustomers_Particular') !!}',
        
        "autoWidth": false,
        "scrollX": true,

        "order": [[ 0, "desc" ]],
        
        columns: [
            { data: 'actions', name: 'actions' },
            { data: 'Cubique_Employee', name: 'Cubique_Employee' },
            { data: 'Date_first_approach', name: 'Date_first_approach' },
            { data: 'Date_postponed_approach', name: 'Date_postponed_approach' },
            { data: 'attributes_8', name: 'attributes_8' },
            { data: 'Entity_name', name: 'Entity_name' },
            { data: 'Entity_phone_number', name: 'Entity_phone_number' },
            { data: 'Entity_email', name: 'Entity_email' },
            { data: 'Comments', name: 'Comments' },
            { data: 'attributes_1', name: 'attributes_1' },
            { data: 'attributes_2', name: 'attributes_2' },
            { data: 'attributes_3', name: 'attributes_3' },
            // { data: 'attributes_4', name: 'attributes_4' },
            // { data: 'attributes_5', name: 'attributes_5' },
            // { data: 'attributes_6', name: 'attributes_6' },
            { data: 'attributes_7', name: 'attributes_7' },
            { data: 'City', name: 'City' },
            { data: 'District', name: 'District' },
            { data: 'Date_meeting', name: 'Date_meeting' },
        ],
    });

    var oldThis;
    var table2 = $('#customers_particular').DataTable();
    $('#customers_particular tbody').on( 'click', 'tr', function () {

        customer = table2.row( this ).data();
        customerId = customer["id"];

        rowId = table2.row( this ).index();

        console.log("rowId: " + rowId);
        console.log("costumerId: " + customerId);

        // retorna as cores caso clique noutra linha
        $(oldThis).css('background', '');
        $(oldThis).css('font-weight', 'normal');

        // nova linha
        rowId = table2.row( this ).index();
        oldThis = this;

        if(this.style.background == "") {
            $(this).css('background', '#ff9f89');
            $(this).css('font-weight', 'bold');
        }
        else {
            $(this).css('background', '');
            $(this).css('font-weight', 'normal');
        }
    } );

    $('#customers_particular tbody').on('dblclick', 'tr', function () {
        $pathTo = '/particularCustomers/';
        $myTable = "#customers_particular";

        return editCustomer(customerId, $pathTo, $myTable);

    } );

});
</script>

@stop