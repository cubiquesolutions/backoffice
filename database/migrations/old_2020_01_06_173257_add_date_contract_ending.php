<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateContractEnding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resale_customer', function ($table) {
            $table->datetime('Date_contract_ending')->after('Date_meeting')->nullable();
        });
        Schema::table('segmented_customer', function ($table) {
            $table->datetime('Date_contract_ending')->after('Date_meeting')->nullable();
        });
        Schema::table('particular_customer', function ($table) {
            $table->datetime('Date_contract_ending')->after('Date_meeting')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
