<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCubiqueTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('resale_customer', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('Cubique_Employee')->nullable();

            $table->datetime('Date_first_approach')->nullable();
            $table->datetime('Date_postponed_approach')->nullable();

            $table->string('Entity_name')->nullable();
            $table->string('Entity_email')->nullable();
            $table->string('Entity_phone_number')->nullable();

            $table->string('Customer_name')->nullable();
            $table->string('Customer_email')->nullable();
            $table->string('Customer_phone_number')->nullable();

            $table->string('Country')->nullable();
            $table->string('District')->nullable();
            $table->string('City')->nullable();
            $table->string('Place')->nullable();
            $table->string('Postal_code')->nullable();

            $table->string('attributes_1')->nullable();
            $table->string('attributes_2')->nullable();
            $table->string('attributes_3')->nullable();
            $table->string('attributes_4')->nullable();
            $table->string('attributes_5')->nullable();
            $table->string('attributes_6')->nullable();
            $table->string('attributes_7')->nullable();
            $table->string('attributes_8')->nullable();

            $table->text('Comments')->nullable();

            $table->timestamps();

        });

        Schema::create('segmented_customer', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('Cubique_Employee')->nullable();

            $table->datetime('Date_first_approach')->nullable();
            $table->datetime('Date_postponed_approach')->nullable();

            $table->string('Entity_name')->nullable();
            $table->string('Entity_email')->nullable();
            $table->string('Entity_phone_number')->nullable();

            $table->string('Customer_name')->nullable();
            $table->string('Customer_email')->nullable();
            $table->string('Customer_phone_number')->nullable();

            $table->string('Country')->nullable();
            $table->string('District')->nullable();
            $table->string('City')->nullable();
            $table->string('Place')->nullable();
            $table->string('Postal_code')->nullable();

            $table->string('attributes_1')->nullable();
            $table->string('attributes_2')->nullable();
            $table->string('attributes_3')->nullable();
            $table->string('attributes_4')->nullable();
            $table->string('attributes_5')->nullable();
            $table->string('attributes_6')->nullable();
            $table->string('attributes_7')->nullable();
            $table->string('attributes_8')->nullable();

            $table->text('Comments')->nullable();

            $table->timestamps();

        });

        Schema::create('particular_customer', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('Cubique_Employee')->nullable();

            $table->datetime('Date_first_approach')->nullable();
            $table->datetime('Date_postponed_approach')->nullable();

            $table->string('Entity_name')->nullable();
            $table->string('Entity_email')->nullable();
            $table->string('Entity_phone_number')->nullable();

            $table->string('Customer_name')->nullable();
            $table->string('Customer_email')->nullable();
            $table->string('Customer_phone_number')->nullable();

            $table->string('Country')->nullable();
            $table->string('District')->nullable();
            $table->string('City')->nullable();
            $table->string('Place')->nullable();
            $table->string('Postal_code')->nullable();

            $table->string('attributes_1')->nullable();
            $table->string('attributes_2')->nullable();
            $table->string('attributes_3')->nullable();
            $table->string('attributes_4')->nullable();
            $table->string('attributes_5')->nullable();
            $table->string('attributes_6')->nullable();
            $table->string('attributes_7')->nullable();
            $table->string('attributes_8')->nullable();

            $table->text('Comments')->nullable();

            $table->timestamps();

        });

        Schema::create('attributes_1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
        });

        Schema::create('attributes_2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
        });

        Schema::create('attributes_3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
        });

        Schema::create('attributes_4', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
        });

        Schema::create('attributes_5', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
        });

        Schema::create('attributes_6', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
        });

        Schema::create('attributes_7', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
        });

        Schema::create('attributes_8', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
        });

        Schema::create('info_countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('code')->nullable();
        });

        Schema::create('products_hermida', function (Blueprint $table) {
            $table->bigIncrements('id');
    
            $table->string('ref_supplier')->nullable();
            $table->string('ref_cubique')->nullable();

            $table->string('code_supplier')->nullable();
            $table->string('ean')->nullable();
    
            $table->string('description')->nullable();
    
            $table->double('stock_old', 20, 2)->nullable();
            $table->double('stock_new', 20, 2)->nullable();
            $table->double('price_old', 20, 2)->nullable();
            $table->double('price_new', 20, 2)->nullable();

            $table->string('name_supplier')->default("Hermida");
    
            $table->timestamps();
    
        });

        Schema::create('products_gmbh', function (Blueprint $table) {
            $table->bigIncrements('id');
    
            $table->string('ref_supplier')->nullable();
            $table->string('ref_cubique')->nullable();

            $table->string('code_supplier')->nullable();
            $table->string('ean')->nullable();
    
            $table->string('description')->nullable();
    
            $table->double('stock_old', 20, 2)->nullable();
            $table->double('stock_new', 20, 2)->nullable();
            $table->double('price_old', 20, 2)->nullable();
            $table->double('price_new', 20, 2)->nullable();

            $table->string('name_supplier')->default("Winterholt & Hering GmbH");
    
            $table->timestamps();
    
        });

        Schema::create('products_exadi', function (Blueprint $table) {
            $table->bigIncrements('id');
    
            $table->string('ref_supplier')->nullable();
            $table->string('ref_cubique')->nullable();

            $table->string('code_supplier')->nullable();
            $table->string('ean')->nullable();
    
            $table->string('description')->nullable();
    
            $table->double('stock_old', 20, 2)->nullable();
            $table->double('stock_new', 20, 2)->nullable();
            $table->double('price_old', 20, 2)->nullable();
            $table->double('price_new', 20, 2)->nullable();

            $table->string('name_supplier')->default("Exadi");
    
            $table->timestamps();
    
        });

        Schema::create('api_providers', function (Blueprint $table) {     // tabela para guardar os dados relativos aos logins nas APIs do fornecedor.
            $table->bigIncrements('id');
    
            $table->string('provider_name')->nullable();
            $table->string('login_token')->nullable();

            $table->timestamps();

        });


        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('resale_customer');
        Schema::dropIfExists('segmented_customer');
        Schema::dropIfExists('particular_customer');

        Schema::dropIfExists('attributes_1');
        Schema::dropIfExists('attributes_2');
        Schema::dropIfExists('attributes_3');
        Schema::dropIfExists('attributes_4');
        Schema::dropIfExists('attributes_5');
        Schema::dropIfExists('attributes_6');
        Schema::dropIfExists('attributes_7');
        Schema::dropIfExists('attributes_8');

        Schema::dropIfExists('info_countries');

        Schema::dropIfExists('products_hermida');
        Schema::dropIfExists('products_gmbh');
        Schema::dropIfExists('products_exadi');

        Schema::dropIfExists('api_providers');

        Schema::enableForeignKeyConstraints();
    }
}

