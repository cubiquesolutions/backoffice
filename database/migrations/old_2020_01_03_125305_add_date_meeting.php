<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateMeeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resale_customer', function ($table) {
            $table->datetime('Date_meeting')->after('Date_postponed_approach')->nullable();
        });
        Schema::table('segmented_customer', function ($table) {
            $table->datetime('Date_meeting')->after('Date_postponed_approach')->nullable();
        });
        Schema::table('particular_customer', function ($table) {
            $table->datetime('Date_meeting')->after('Date_postponed_approach')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
