<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveDateContractEnding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resale_customer', function ($table) {
            $table->dropColumn('Date_contract_ending');
        });
        Schema::table('segmented_customer', function ($table) {
            $table->dropColumn('Date_contract_ending');
        });
        Schema::table('particular_customer', function ($table) {
            $table->dropColumn('Date_contract_ending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
