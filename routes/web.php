<?php

use App\Models\Equipment_key;
use App\Http\Controllers\Equipment_keyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
	return view('auth/login');
}); // quando nao está logado

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'ProfileController@edit');
}); // esta rota é para quando o utilizador está logado e volta a entrar no ./ do site , para nao partir

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home')->middleware('auth'); // esta rota direciona para a Dashboard com os gráficos bonitos
Route::get('/home', 'ProfileController@edit')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('icons', ['as' => 'pages.icons', 'uses' => 'PageController@icons']);
	Route::get('maps', ['as' => 'pages.maps', 'uses' => 'PageController@maps']);
	Route::get('notifications', ['as' => 'pages.notifications', 'uses' => 'PageController@notifications']);
	Route::get('rtl', ['as' => 'pages.rtl', 'uses' => 'PageController@rtl']);
	Route::get('tables', ['as' => 'pages.tables', 'uses' => 'PageController@tables']);
	Route::get('typography', ['as' => 'pages.typography', 'uses' => 'PageController@typography']);
	Route::get('upgrade', ['as' => 'pages.upgrade', 'uses' => 'PageController@upgrade']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});




/* VISTAS E RESPETIVOS MÉTODOS  */
Route::group(['middleware' => 'auth'], function () {
	Route::get('attributes', ['as' => 'pages.attributes', 'uses' => 'Attributes1Controller@index']);

	Route::get('resaleCustomers', ['as' => 'pages.resaleCustomers', 'uses' => 'Resale_customerController@index']);
	Route::get('resaleCustomers/edit/{id}', 'Resale_customerController@getCustomerInfo');
	Route::post('resaleCustomers/create', 'Resale_customerController@create');
	Route::post('resaleCustomers/edit/{id}/update', 'Resale_customerController@update');
	Route::delete('resaleCustomers/delete/{id}', 'Resale_customerController@deleteCustomerResale');
	Route::get('resaleCustomers/filterToCSV', 'Resale_customerController@getFilteredQueryResults');
	Route::get('resaleCustomers/filterToCSV/download', 'Resale_customerController@getDownload');

	Route::get('segmentedCustomers', ['as' => 'pages.segmentedCustomers', 'uses' => 'Segmented_customerController@index']);
	Route::get('segmentedCustomers/edit/{id}', 'Segmented_customerController@getCustomerInfo');
	Route::post('segmentedCustomers/create', 'Segmented_customerController@create');
	Route::post('segmentedCustomers/edit/{id}/update', 'Segmented_customerController@update');
	Route::delete('segmentedCustomers/delete/{id}', 'Segmented_customerController@deleteCustomerSegmented');
	Route::get('segmentedCustomers/filterToCSV', 'Segmented_customerController@getFilteredQueryResults');
	Route::get('segmentedCustomers/filterToCSV/download', 'Segmented_customerController@getDownload');

	Route::get('segmentedCustomers/send_email', 'Segmented_customerController@send_email');

	// Route::get('particularCustomers', ['as' => 'pages.particularCustomers', 'uses' => 'Particular_customerController@index']);
	// Route::get('particularCustomers/edit/{id}', 'Particular_customerController@getCustomerInfo');
	// Route::post('particularCustomers/create', 'Particular_customerController@create');
	// Route::post('particularCustomers/edit/{id}/update', 'Particular_customerController@update');
	// Route::delete('particularCustomers/delete/{id}', 'Particular_customerController@deleteCustomerParticular');
	// Route::get('particularCustomers/filterToCSV', 'Particular_customerController@getFilteredQueryResults');
	// Route::get('particularCustomers/filterToCSV/download', 'Particular_customerController@getDownload');

	Route::get('webservice', ['as' => 'pages.webservice', 'uses' => 'Product_hermidaController@index']);
});

/* DATATABLES */
Route::group(['middleware' => 'auth'], function () {
	Route::get('datatables/customers/resale', 'Resale_customerController@getAllCustomers_Resale')->name('getAllCustomers_Resale');
	Route::get('datatables/customers/segmented', 'Segmented_customerController@getAllCustomers_Segmented')->name('getAllCustomers_Segmented');
	Route::get('datatables/customers/particular', 'Particular_customerController@getAllCustomers_Particular')->name('getAllCustomers_Particular');

	Route::get('datatables/attributes1', 'Attributes1Controller@getAllAttributes_1')->name('getAllAttributes_1');
	Route::get('datatables/attributes2', 'Attributes1Controller@getAllAttributes_2')->name('getAllAttributes_2');
	Route::get('datatables/attributes3', 'Attributes1Controller@getAllAttributes_3')->name('getAllAttributes_3');
	Route::get('datatables/attributes4', 'Attributes1Controller@getAllAttributes_4')->name('getAllAttributes_4');
	Route::get('datatables/attributes5', 'Attributes1Controller@getAllAttributes_5')->name('getAllAttributes_5');
	Route::get('datatables/attributes6', 'Attributes1Controller@getAllAttributes_6')->name('getAllAttributes_6');
	Route::get('datatables/attributes7', 'Attributes1Controller@getAllAttributes_7')->name('getAllAttributes_7');
	Route::get('datatables/attributes8', 'Attributes1Controller@getAllAttributes_8')->name('getAllAttributes_8');

	Route::get('datatables/products/hermida', 'Product_hermidaController@getAllProducts_hermida')->name('getAllProducts_hermida');
	Route::get('datatables/products/gmbh', 'Product_gmbhController@getAllProducts_gmbh')->name('getAllProducts_gmbh');
	Route::get('datatables/products/exadi', 'Product_exadiController@getAllProducts_exadi')->name('getAllProducts_exadi');

	Route::get('datatables/app/equipment_key', 'Equipment_keyController@getAllEquipment_Keys')->name('getAllEquipment_Keys');
});

/* CREATE */
Route::group(['middleware' => 'auth'], function () {
	// Route::get('/settings/attributes', 'Attributes1Controller@index')->name('settings/attributes'); // VISTA
	Route::post('/settings/attributes/createAtr1', 'Attributes1Controller@createAtr1')->name('attributes.createAtr1'); // CRIAR NOVO ATRIBUTO 1
	Route::post('/settings/attributes/createAtr2', 'Attributes1Controller@createAtr2')->name('attributes.createAtr2'); // CRIAR NOVO ATRIBUTO 1
	Route::post('/settings/attributes/createAtr3', 'Attributes1Controller@createAtr3')->name('attributes.createAtr3'); // CRIAR NOVO ATRIBUTO 1
	Route::post('/settings/attributes/createAtr4', 'Attributes1Controller@createAtr4')->name('attributes.createAtr4'); // CRIAR NOVO ATRIBUTO 1
	Route::post('/settings/attributes/createAtr5', 'Attributes1Controller@createAtr5')->name('attributes.createAtr5'); // CRIAR NOVO ATRIBUTO 1
	Route::post('/settings/attributes/createAtr6', 'Attributes1Controller@createAtr6')->name('attributes.createAtr6'); // CRIAR NOVO ATRIBUTO 1
	Route::post('/settings/attributes/createAtr7', 'Attributes1Controller@createAtr7')->name('attributes.createAtr7'); // CRIAR NOVO ATRIBUTO 1
	Route::post('/settings/attributes/createAtr8', 'Attributes1Controller@createAtr8')->name('attributes.createAtr8'); // CRIAR NOVO ATRIBUTO 1
});

/* DELETE */
Route::group(['middleware' => 'auth'], function () {
	Route::delete('/settings/attributes/deleteAtr1/{id}', 'Attributes1Controller@deleteAtr1'); // DELETE
	Route::delete('/settings/attributes/deleteAtr2/{id}', 'Attributes1Controller@deleteAtr2'); // DELETE
	Route::delete('/settings/attributes/deleteAtr3/{id}', 'Attributes1Controller@deleteAtr3'); // DELETE
	Route::delete('/settings/attributes/deleteAtr4/{id}', 'Attributes1Controller@deleteAtr4'); // DELETE
	Route::delete('/settings/attributes/deleteAtr5/{id}', 'Attributes1Controller@deleteAtr5'); // DELETE
	Route::delete('/settings/attributes/deleteAtr6/{id}', 'Attributes1Controller@deleteAtr6'); // DELETE
	Route::delete('/settings/attributes/deleteAtr7/{id}', 'Attributes1Controller@deleteAtr7'); // DELETE
	Route::delete('/settings/attributes/deleteAtr8/{id}', 'Attributes1Controller@deleteAtr8'); // DELETE
});

/* emails */
Route::group(['middleware' => 'auth'], function () {
	Route::get("/send_email/apresentacao_email/{id}", 'MailController@apresentacao_email');
});

/* WEB SERVICE */

/* ***************** API CUBIQUE *************************** */
Route::group(['middleware' => 'auth'], function () {
	/* GET ALL */
	Route::get('/webservice/all/generateCSV/', 'Api_providersController@generateCSV');
	Route::get('/webservice/all/download/', 'Api_providersController@getDownload');
	Route::get('/webservice/all/removefile/', 'Api_providersController@removeFile');

	/* HERMIDA */
	Route::get('/webservice/hermida/bulkUpdate/', 'Product_hermidaController@bulkUpdate');

	/* GMBH */
	Route::get('/webservice/gmbh/bulkUpdate/', 'Product_gmbhController@bulkUpdate');
	Route::post('/webservice/gmbh/bulkUpdate/', 'Product_gmbhController@bulkUpdate');

	/* EXADI */
	Route::get('/webservice/exadi/bulkUpdate/', 'Product_exadiController@update_price_and_stock');
});

/* test OFFICEGEST */
Route::group(['middleware' => 'auth'], function () {
	/* GET ALL */
	Route::get('officegest', [
		'as' => 'pages.OfficeGest_home',
		'uses' => 'OfficeGestController@index'
	]);

	Route::get('officegest/entities', [
		'as' => 'pages.OfficeGest_entities',
		'uses' => 'OfficeGestController@entities_view'
	]);

	Route::get('officegest/crm', [
		'as' => 'pages.OfficeGest_crm',
		'uses' => 'OfficeGestController@crm_view'
	]);

	Route::get('officegest/stocks', [
		'as' => 'pages.OfficeGest_stocks',
		'uses' => 'OfficeGestController@stocks_view'
	]);

	Route::get('officegest/documents', [
		'as' => 'pages.OfficeGest_docs',
		'uses' => 'OfficeGestController@docs_view'
	]);

	Route::get('/get/entities_customers', 	'OfficeGestController@get_entities_customers')->name('get_entities_customers');
	Route::get('/get/entities_prospects', 	'OfficeGestController@get_entities_prospects')->name('get_entities_prospects');
	Route::get('/get/entities_suppliers', 	'OfficeGestController@get_entities_suppliers')->name('get_entities_suppliers');
	Route::get('/get/entities_employees', 	'OfficeGestController@get_entities_employees')->name('get_entities_employees');
	Route::get('/get/entities_commercials', 'OfficeGestController@get_entities_commercials')->name('get_entities_commercials');

	Route::get('/get/stocks_articles', 		'OfficeGestController@get_stocks_articles')->name('get_stocks_articles');
	Route::get('/get/stocks_families', 		'OfficeGestController@get_stocks_families')->name('get_stocks_families');
	Route::get('/get/stocks_subfamilies', 	'OfficeGestController@get_stocks_subfamilies')->name('get_stocks_subfamilies');
	Route::get('/get/stocks_subsubfamilies', 'OfficeGestController@get_stocks_subsubfamilies')->name('get_stocks_subsubfamilies');
	Route::get('/get/stocks_brands', 		'OfficeGestController@get_stocks_brands')->name('get_stocks_brands');
	Route::get('/get/stocks_units', 		'OfficeGestController@get_stocks_units')->name('get_stocks_units');

	Route::get('/get/crm_businessopportunities', 	'OfficeGestController@get_crm_businessopportunities')->name('get_crm_businessopportunities');
	Route::get('/get/crm_businessareas', 			'OfficeGestController@get_crm_businessareas')->name('get_crm_businessareas');
	Route::get('/get/crm_phases', 					'OfficeGestController@get_crm_phases')->name('get_crm_phases');
	Route::get('/get/crm_appointments', 			'OfficeGestController@get_crm_appointments')->name('get_crm_appointments');
	Route::get('/get/crm_campaigns', 				'OfficeGestController@get_crm_campaigns')->name('get_crm_campaigns');

	Route::get('/get/sales_documents', 				'OfficeGestController@get_sales_documents')->name('get_sales_documents');
	Route::get('/get/sales_receipts', 				'OfficeGestController@get_sales_receipts')->name('get_sales_receipts');

	Route::get('/get/purchases_documents', 			'OfficeGestController@get_purchases_documents')->name('get_purchases_documents');

	Route::get('/get/tables_privileges', 			'OfficeGestController@get_tables_privileges')->name('get_tables_privileges');
	Route::get('/get/reports_sales_documents', 		'OfficeGestController@get_reports_sales_documents')->name('get_reports_sales_documents');
	Route::get('/get/reports_sales_articlesbydocument',		'OfficeGestController@get_reports_sales_articlesbydocument')->name('get_reports_sales_articlesbydocument');
});




/************************** ROTAS PUBLICAS **************************/

/*CBQ_PrintingContractEquipmentController - QR Code */
Route::get('/api/get_info_equip/{nif}/{designacao_do_equip}', 				'OfficeGestController@get_this_equipment_information');
Route::get('/api/get_info_equip_unique_key/{unique_key}', 					'OfficeGestController@get_this_equipment_information_by_unique_key');

Route::get('/api/z76KPDDnyqtvZs8G1819', 									'OfficeGestController@get_mapa_comissoes_2018_2019');
Route::get('/api/9pqWxW66WezTDA1920', 										'OfficeGestController@get_mapa_comissoes_2019_2020');

Route::get('/api/2JAjGaBt9aSFLzGa', 										'OfficeGestController@get_product_families');
Route::get('/api/BUrejsXG9vL7fs', 											'OfficeGestController@get_product_subfamilies');
Route::get('/api/R7FgRbphgtSVvB', 											'OfficeGestController@get_product_subsubfamilies');

Route::get('/api/J8PVsKwsvV2UNj', 											'OfficeGestController@get_mapa_comissoes_comerciais_indor_2020');
Route::get('/api/Twgy3x6Ethm5kMHN-co2021', 									'OfficeGestController@get_mapa_comissoes_comerciais_indor_2021');

Route::get('/api/MWF5GQHFz2D9r5yE-VG2020', 									'OfficeGestController@get_relatorio_vendas_geral_documentos_2020');
Route::get('/api/YK7k98QnqwUqqddy-VG2021', 									'OfficeGestController@get_relatorio_vendas_geral_documentos_2021');

Route::get('/api/teste', 									'OfficeGestController@teste');


/************************* fim rotas publicas **************************/


/* Equipment_key - Chaves para Equipamentos */
Route::group(['middleware' => 'auth'], function () {

	Route::get('officegest/equipment_keys', [
		'as' => 'pages.equipmentKeys',
		'uses' => 'Equipment_keyController@index'
	]);

	Route::get('/equipment_keys/create/', 			'Equipment_keyController@create');
	Route::get('/equipment_keys/delete_unused/', 	'Equipment_keyController@clean_unused_local_keys');
	// EXPORTA TODA A TABELA DAS CHAVES
	Route::get('/equipment_keys/exportCSV', 		function () {

		$Equipment_keyController = new Equipment_keyController;
        $Equipment_keyController->set_isUsed_sim();

		$table = Equipment_key::all();
		$filename = "keys.csv";
		$handle = fopen($filename, 'w+');
		fputcsv($handle, array('id', 'unique_key', 'isUsed', 'created_at'));

		foreach ($table as $row) {
			fputcsv($handle, array($row['id'], $row['unique_key'], $row['isUsed'], $row['created_at']));
		}

		fclose($handle);

		$headers = array(
			'Content-Type' => 'text/csv',
		);

		return Response::download($filename, 'keys.csv', $headers);
	});
	// Exporta APENAS AS CHAVES COM QUEBRA DE LINHA & sem cabecalho
	Route::get('/equipment_keys/exportCSV_quebra_de_linha', 		function () {

        $Equipment_keyController = new Equipment_keyController;
        $Equipment_keyController->set_isUsed_sim();

		$table = Equipment_key::all();
		$filename = "keys.csv";
		$handle = fopen($filename, 'w+');
		// fputcsv($handle, array('unique_key'));

		foreach ($table as $row) {
			if ($row['isUsed'] == "Nao") {
				fputcsv($handle, array(''));
				fputcsv($handle, array($row['unique_key']));
				fputcsv($handle, array(''));
				fputcsv($handle, array(''));
			}
		}

		fclose($handle);

		$headers = array(
			'Content-Type' => 'text/csv',
		);

		return Response::download($filename, 'keys.csv', $headers);
	});
	Route::get('/equipment_keys/update_isUsed/', 			'Equipment_keyController@set_isUsed_sim');
});
