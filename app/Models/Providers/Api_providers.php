<?php

namespace App\Models\Providers;

use Illuminate\Database\Eloquent\Model;

class Api_providers extends Model
{
    protected $table = 'api_providers';

    protected $fillable = [
        'provider_name',
        'login_token',
    ];
}
