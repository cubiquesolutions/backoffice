<?php

namespace App\Models\Providers;

use Illuminate\Database\Eloquent\Model;

class Product_hermida extends Model
{
    protected $table = 'products_hermida';

    protected $fillable = [
        'ref_supplier',
        'ref_cubique',
        'code_supplier',
        'ean',
        'description',
        'stock_old',
        'stock_new',
        'price_old',
        'price_new',
        'name_supplier'
    ];
}
