<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resale_customer extends Model
{
    protected $table ="resale_customer";

    protected $fillable = [
        'Cubique_Employee',

        'Date_first_approach',
        'Date_postponed_approach',
        'Date_meeting',
        
        'Entity_name',
        'Entity_email',
        'Entity_phone_number',
        'Customer_name',
        'Customer_email',
        'Customer_phone_number',
        'Country',
        'District',
        'City',
        'Place',
        'Postal_code',

        'attributes_1',
        'attributes_2',
        'attributes_3',
        'attributes_4',
        'attributes_5',
        'attributes_6',
        'attributes_7',
        'attributes_8',
        
        'Comments',
    ];

}
