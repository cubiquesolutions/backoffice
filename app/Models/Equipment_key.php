<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipment_key extends Model
{
    protected $table ="equipment_key";

    protected $fillable = [
        'id',
        'unique_key',
        'isUsed',
        'created_by'
    ];

}
