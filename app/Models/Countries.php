<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table ="info_countries";

    protected $fillable = [
        'name',
        'code'
    ];
}
