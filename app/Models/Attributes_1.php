<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attributes_1 extends Model
{
    protected $table ="Attributes_1";

    protected $fillable = [
        'name',
        'description'
    ];
    
    public $timestamps = false;

    public function Resale_customer()
    {
        return $this->belongsToMany('App\Models\Resale_customer');
    }

}
