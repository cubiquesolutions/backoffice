<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attributes_7 extends Model
{
    protected $table ="Attributes_7";

    protected $fillable = [
        'name',
        'description'
    ];
    
    public $timestamps = false;

    public function Resale_customer()
    {
        return $this->belongsToMany('App\Models\Resale_customer');
    }

}
