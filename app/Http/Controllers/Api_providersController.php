<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Auth\Access\AuthorizationException;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;

use App\Models\Providers\Api_providers;
use App\Models\Providers\Product_exadi;
use App\Models\Providers\Product_gmbh;
use App\Models\Providers\Product_hermida;

use App\Http\Resources\Api_providersResource;
use App\Http\Resources\Product_exadiResource;
use App\Http\Resources\Product_gmbhResource;
use App\Http\Resources\Product_hermidaResource;


use Carbon\Carbon;

use DateTime;

class Api_providersController extends Controller
{

    public function index()
    {
        return view('/providers_views/providers_home');
    }

    public function getAll()
    {
        return Api_providersResource::collection(Api_providers::all());
    }

    public function merge_all_providers_tables()
    {
        $exadi_collection       = Product_exadiResource::collection(Product_exadi::all());
        $gmbh_collection        = Product_gmbhResource::collection(Product_gmbh::all());
        $hermida_collection     = Product_hermidaResource::collection(Product_hermida::all());

        $merged = $exadi_collection->merge($gmbh_collection);
        $merged = $merged->merge($hermida_collection);


        $merged_jsn = $merged->toJson();
        return $merged_jsn;
    }

    public function generateCSV()
    {
        $jsn = $this->merge_all_providers_tables();

        $date_now = Carbon::now();
        $date_now = date_format($date_now,"Y-m-d_H-i-s");

        //Give our CSV file a name.
        $csvFileName = "tabela_produtos_webservice_" . $date_now . ".csv";

        $filePath = public_path() . "\\" . $csvFileName;

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=" . $csvFileName,
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        //Decode the JSON and convert it into an associative array.
        $jsonDecoded = json_decode($jsn, true);

        //Open file pointer.
        $fp = fopen($csvFileName, 'w');

        //Headers 1st row
        fputcsv($fp, array_keys($jsonDecoded[0]));

        //Loop through the associative array.
        foreach ($jsonDecoded as $row) {
            //Write the row to the CSV file.
            fputcsv($fp, $row);
        }

        //Finally, close the file pointer.
        fclose($fp);

        //return Response::download($filePath, $csvFileName, $headers);
        return $csvFileName;
    }

    public function getDownload(Request $request)
    {
        $csvFileName = $request->fileName_from_client;

        $filePath = public_path() . "//" . $csvFileName;

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=" . $csvFileName,
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        return Response::download($filePath, $csvFileName, $headers);
    }

    public function removeFile(Request $request){

        $csvFileName = $request->fileName_from_client;

        if (is_file($csvFileName)) {
            unlink($csvFileName); // delete file
         }

        return $this->index();
    }

    public function getToken($provider_name)
    {
        // tem de ser array para ser o mesmo TIPO do login() no exadi
        // resulta array[i] = "login_token" => "oksdasodkasodsad"
        return Api_providers::select('login_token')->where('provider_name', '=', $provider_name)->get()->first()->toArray();
    }

    public function getUpdatedAt($provider_name)
    {
        return Api_providers::select('updated_at')->where('provider_name', '=', $provider_name)->get('updated_at')->first();
    }

    public function update_provider_token($provider_name, $token)
    {
        // quando o token expira

        $provider           = Api_providers::where('provider_name', '=', $provider_name)->get()->first();

        $updated_at         = Carbon::now();
        $updated_at         = date_format($updated_at, "Y-m-d H:i:s");

        $provider->update(['login_token'    => $token]);
        $provider->update(["updated_at"     => $updated_at]);

        $provider->save();
    }

    public function check_token_expiration_date($provider_name)
    {
        $provider_name              = strtolower($provider_name);               // letras pequenas para coincidir sempre com o ficheiro variables
        $login_token_duration_hours = Config::get('variables.' . $provider_name . '.login_token_duration_hours');

        $time_now           = Carbon::now();
        $time_now           = date_format($time_now, "Y-m-d H:i:s");

        $last_token_time    = $this->getUpdatedAt($provider_name);
        $last_token_time    = $last_token_time->toArray();

        $last_token_time    = Carbon::parse($last_token_time["updated_at"]);       // coloca no formato do Carbon
        $last_token_time    = date_format($last_token_time, "Y-m-d H:i:s");

        $time_now           = new DateTime($time_now);
        $last_token_time    = new DateTime($last_token_time);                       // coloca em formato DateTime para poder uzar a função diff() que calcula a diferente entre as datas



        $diff = $time_now->diff($last_token_time);

        $hours = $diff->h;
        $hours = $hours + ($diff->days * 24);

        if ($hours > $login_token_duration_hours) {
            return "expired";
        } else {
            return "not expired";
        }
    }
}
