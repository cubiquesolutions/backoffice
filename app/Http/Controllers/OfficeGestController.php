<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;

use Mail;
use App\User;

use Illuminate\Support\Facades\Auth;

use Yajra\Datatables\Datatables;
use DB;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Database\QueryException;

use function GuzzleHttp\json_decode;

class OfficeGestController extends Controller
{

    public function entities_view()
    {
        return view('/officeGest_views/OfficeGest_entities');
    }

    public function crm_view()
    {
        return view('/officeGest_views/OfficeGest_crm');
    }

    public function stocks_view()
    {
        return view('/officeGest_views/OfficeGest_stocks');
    }

    public function docs_view()
    {
        return view('/officeGest_views/OfficeGest_docs');
    }



    public function index()
    {
        return view('/officeGest_views/OfficeGest_home');
    }

    public function get_login_hash()
    {
        $username   = Config::get('variables.officegest.username');
        $password   = Config::get('variables.officegest.password');
        $url        = Config::get('variables.officegest.url');

        $login_parameters = $this->call_og_api("GET", $url, ['username' => $username, 'password' => $password]);

        $login_parameters = json_decode($login_parameters);

        // { 
        //     "result":"error",
        //     "code":401,
        //     "code_desc":"Unauthorized",
        //     "method":"get",
        //     "message":"You must be authorized to view this page.",
        //     "call_id":null,
        //     "timestamp":1578924643,
        //     "hash":"35eee27d75ee58335ac0faa040e9c40f01af4ee8",
        //     "lic":"eCRM:cf34d6887d35b09cb44d0e",
        //     "user_id":34
        // }

        $this->call_og_api("GET", $url, ['username' => $username, 'password' => $login_parameters->hash]);

        // { 
        //     "result":"info",
        //     "code":1000,
        //     "code_desc":"Command Done Successfully",
        //     "method":"get",
        //     "call_id":null,
        //     "timestamp":1578926484,
        //     "msg":"Please use any controllers from the list",
        //     "list":[ 
        //         "addon",
        //         "attach",
        //         "crm",
        //         "ecoauto",
        //         "entities",
        //         "graphics",
        //         "purchases",
        //         "reports",
        //         "sales",
        //         "stocks",
        //         "subscriptions",
        //         "tables",
        //         "tickets",
        //         "utils",
        //         "wms"
        //     ]
        // }

        return $login_parameters->hash;
    }

    function call_og_api($method, $url, $auth = [], $data = [])
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        if (isset($auth)) {
            $authentication = $auth['username'] . ':' . $auth['password'];
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $authentication);
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    function guzzle_call_og_api($client, $method, $url, $user, $key, $form_params = [], $params = [])
    {
        try {
            $request = $client->request($method, $url, [
                'auth' => [$user, $key],
                'query' => $params,
                'form_params' => $form_params
            ]);
            $status = $request->getStatusCode();
            $body = $request->getBody();
            return [
                'status' => $status,
                'body' => $body,
            ];
        } catch (\Exception $e) {
            return $e->getResponse()->getReasonPhrase();
        }
    }

    /* ENTITIES */

    function get_entities_customers()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/entities/customers';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                // {
                //     id: 4512,
                //     name: "Acordo Seguro - Mediação de Seguros Unipessoal, Lda.",
                //     address: "Rua General Humberto Delgado, Bloco 5, Cave 8",
                //     city: "Alcobaça",
                //     zipcode: "2460-052 Alcobaça",
                //     country: "PTG",
                //     customertaxid: "509728197",
                //     phone1: "+351.262597080",
                //     phone2: "",
                //     mobilephone: "+351.933425114",
                //     fax: "",
                //     email: "bruno.ferreira@acordoseguro.pt",
                //     webpage: "",
                //     vendorcode: 9,
                //     active: "T",
                //     entitytype: "C",
                //     classifcode: "03",
                //     campaigncode: "",
                //     customertype: "E",
                //     id_client_web: ""
                // },

                $og_total        = $response->total;
                $og_customers    = $response->customers;

                $jsn = json_encode($og_customers, true);
                $jsn = json_decode($jsn, true);

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting entities_customers', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_entities_prospects()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/entities/prospects';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_prospects               = $response->prospects;

                $jsn = json_encode($og_prospects, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: 51,
                //     name: "A. Logos Associação Para O Desenvolvimento de Assessoria e Ensaios Técnicos",
                //     address: "Santarém - Abrantes",
                //     city: "Alferrarede-Abrantes",
                //     zipcode: "2200-062 Alferrarede-Abrantes",
                //     country: "PTG",
                //     customertaxid: "504085840",
                //     phone1: "+351.241372357",
                //     phone2: "",
                //     mobilephone: "+351.967794372",
                //     fax: "241371644",
                //     email: "a-logos@mail.telepac.pt",
                //     webpage: "www.a-logos.com/",
                //     vendorcode: "",
                //     active: "F",
                //     entitytype: "C",
                //     classifcode: "",
                //     campaigncode: ""
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting entities_prospects', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_entities_suppliers()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/entities/suppliers';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_suppliers               = $response->suppliers;

                $jsn = json_encode($og_suppliers, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: 194,
                //     name: "360Imprimir - Binary Subject, S.A.",
                //     address: "Hipólito Center Park, Fração F, 2ª A, Bairro Arenes",
                //     city: "Torres Vedras",
                //     zipcode: "2560-628 Torres Vedras",
                //     country: "PTG",
                //     customertaxid: "509980422",
                //     phone1: "",
                //     phone2: "",
                //     mobilephone: "",
                //     fax: "",
                //     email: "apoio.cliente@360imprimir.pt",
                //     webpage: "",
                //     active: "T",
                //     entitytype: "F",
                //     classifcode: ""
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting entities_suppliers', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_entities_employees()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/entities/employees';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_employees               = $response->employees;

                $jsn = json_encode($og_employees, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: 14,
                //     name: "Administrador",
                //     address: "Leiria",
                //     city: "Leiria",
                //     zipcode: "2410",
                //     country: "PT",
                //     phone1: "",
                //     phone2: "",
                //     mobilephone: "",
                //     email: "bruno.lopes@cubique.pt",
                //     active: 1,
                //     login: "admin",
                //     teamcode: "",
                //     privilegecode: 1,
                //     warehouse_id: 1
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting entities_employees', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_entities_commercials()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/entities/commercials';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_commercials    = $response->commercials;

                $jsn = json_encode($og_commercials, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: 23,
                //     name: "Aline Coelho",
                //     address: "....",
                //     city: "....",
                //     zipcode: "....",
                //     country: "PT",
                //     customertaxid: "....",
                //     phone1: "+351.244837222",
                //     phone2: "",
                //     mobilephone: "",
                //     email: "aline.coelho@cubique.pt"
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting entities_commercials', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }



    /* FIM ENTITIES */

    /* STOCKS */

    function get_stocks_articles()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/stocks/articles';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_articles                = $response->articles;

                $jsn = json_encode($og_articles, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: "MODELO",
                //     description: "",
                //     articletype: "S",
                //     purchasingprice: 0,
                //     sellingprice: 0,
                //     vatid: "N",
                //     unit: "UN",
                //     stock_quantity: 0,
                //     family: "0A",
                //     subfamily: "",
                //     barcode: "P9RYn",
                //     brand: "",
                //     active: "F",
                //     spaces_dimensions: "N",
                //     activeforweb: "F",
                //     alterationdate: "2017-12-31 11:02:47",
                //     re: "",
                //     idforweb: "",
                //     priceforweb: "",
                //     referenceforweb: "MODELO",
                //     long_description: "",
                //     short_description: ""
                // }

                $dataset = [];

                $jsn_families       = json_decode($this->get_stocks_families());
                $jsn_subfamilies    = json_decode($this->get_stocks_subfamilies());
                $jsn_brands         = json_decode($this->get_stocks_brands());

                foreach ($jsn as $arr) { //foreach dataset

                    foreach ($jsn_families as $arr_families) {      // foreaches para encontrar chaves estrangeiras
                        foreach ($arr_families as $arr_family) {
                            if ($arr["family"] == $arr_family->id) {        // procura pela chave estrangeira
                                $arr["family"] = $arr_family->description;  // guarda o valor da descrição
                                break;
                            }
                        }
                        break;
                    }

                    foreach ($jsn_subfamilies as $arr_subfamilies) {      // foreaches para encontrar chaves estrangeiras
                        foreach ($arr_subfamilies as $arr_subfamily) {
                            if ($arr["subfamily"] == $arr_subfamily->id) {        // procura pela chave estrangeira
                                $arr["subfamily"] = $arr_subfamily->description;  // guarda o valor da descrição
                                break;
                            }
                        }
                        break;
                    }

                    foreach ($jsn_brands as $arr_brands) {      // foreaches para encontrar chaves estrangeiras
                        foreach ($arr_brands as $arr_brand) {
                            if ($arr["brand"] == $arr_brand->id) {        // procura pela chave estrangeira
                                $arr["brand"] = $arr_brand->description;  // guarda o valor da descrição
                                break;
                            }
                        }
                        break;
                    }

                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );


                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting stocks_articles', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_stocks_families()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/stocks/families';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_families                = $response->families;

                $jsn = json_encode($og_families, true);
                $jsn = json_decode($jsn, true);

                $dataset = [];

                // {
                //     id: "3D",
                //     description: "Adiantamento"
                // },

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting stocks_families', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_stocks_subfamilies()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/stocks/subfamilies';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_subfamilies             = $response->subfamilies;

                $jsn = json_encode($og_subfamilies, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: "1BE",
                //     description: "Acessórios",
                //     familyid: "1B"
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting stocks_subfamilies', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_stocks_subsubfamilies()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/stocks/subsubfamilies';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_subsubfamilies          = $response->subsubfamilies;

                $jsn = json_encode($og_subsubfamilies, true);
                $jsn = json_decode($jsn, true);

                // {
                //     subsubfamilyid: "8AAK",
                //     description: "02-OU-42",
                //     familyid: "8A",
                //     subfamilyid: "8AA"
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting stocks_subsubfamilies', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_stocks_brands()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/stocks/brands';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_brands                  = $response->brands;

                $jsn = json_encode($og_brands, true);
                $jsn = json_decode($jsn, true);

                $dataset = [];

                // {
                //     id: 85,
                //     description: "Kave Home",
                //     observations: ""
                // },

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting stocks_brands', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_stocks_units()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/stocks/units';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_units                   = $response->units;

                $jsn = json_encode($og_units, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: "UN",
                //     description: "Unidades"
                // }

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting stocks_units', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    /* FIM STOCKS */


    /* CRM */
    function get_crm_businessopportunities()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/crm/businessopportunities';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {
                $og_total                 = $response->total;
                $og_businessopportunities = $response->businessopportunities;

                $jsn = json_encode($og_businessopportunities, true);
                $jsn = json_decode($jsn, true);

                // array = [
                //         "id" => "1505-0006"
                //         "description" => "Fundação Eugénio de Almeida"
                //         "phase" => "CI"
                //         "businessareas" => 1
                //         "date_start" => "2015-05-14"
                //         "date_end_expected" => "2015-10-01"
                //         "employeecode" => 4
                //         "vendorcode" => 9
                //         "entitycode" => 0
                //         "entitytype" => "C"
                //         "campaigncode" => ""
                //         "done" => "T"
                // ]

                $dataset = [];
                // $dataset[0] = ["id", "description", ...]

                $dataset = [];

                $jsn_areas          = json_decode($this->get_crm_businessareas());
                $jsn_employees      = json_decode($this->get_entities_employees());
                $jsn_commercials    = json_decode($this->get_entities_commercials());
                $jsn_phases         = json_decode($this->get_crm_phases());

                foreach ($jsn as $arr) { //foreach dataset

                    foreach ($jsn_areas as $arr_areas) {      // foreaches para encontrar chaves estrangeiras
                        foreach ($arr_areas as $arr_area) {
                            if ($arr["businessareas"] == $arr_area->id) {        // procura pela chave estrangeira
                                $arr["businessareas"] = $arr_area->description;  // guarda o valor da descrição
                                break;
                            }
                        }
                        break;
                    }

                    foreach ($jsn_employees as $arr_employees) {      // foreaches para encontrar chaves estrangeiras
                        foreach ($arr_employees as $arr_employee) {
                            if ($arr["employeecode"] == $arr_employee->id) {        // procura pela chave estrangeira
                                $arr["employeecode"] = $arr_employee->name;  // guarda o valor da descrição
                                break;
                            }
                        }
                        break;
                    }

                    foreach ($jsn_commercials as $arr_commercials) {      // foreaches para encontrar chaves estrangeiras
                        foreach ($arr_commercials as $arr_commercial) {
                            if ($arr["vendorcode"] == $arr_commercial->id) {        // procura pela chave estrangeira
                                $arr["vendorcode"] = $arr_commercial->name;  // guarda o valor da descrição
                                break;
                            }
                        }
                        break;
                    }

                    foreach ($jsn_phases as $arr_phases) {      // foreaches para encontrar chaves estrangeiras
                        foreach ($arr_phases as $arr_phase) {
                            if ($arr["phase"] == $arr_phase->id) {        // procura pela chave estrangeira
                                $arr["phase"] = $arr_phase->description;  // guarda o valor da descrição
                                break;
                            }
                        }
                        break;
                    }

                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting business opportunities', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_crm_businessareas()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/crm/tables/businessareas';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_businessareas           = $response->businessareas;

                $jsn = json_encode($og_businessareas, true);
                $jsn = json_decode($jsn, true);

                $dataset = [];

                // {
                //     id: 1,
                //     description: "Printing - MPS"
                // },

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting business areas', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_crm_phases()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/crm/tables/phases';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_phases                  = $response->phases;

                $jsn = json_encode($og_phases, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: "AA",
                //     description: "Perdido S/ Responsabilidade"
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting business phases', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_crm_appointments()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/crm/appointments';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {
                $og_total                   = $response->total;
                $og_businessappointments    = $response->appointments;

                $jsn = json_encode($og_businessappointments, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: 10,
                //     description: "Seguimento",
                //     local: "",
                //     entitytype: "C",
                //     entitycode: 432,
                //     employeecode: 5,
                //     campaigncode: "",
                //     businessopportunity: "",
                //     done: "T",
                //     equipment: "",
                //     priority: "",
                //     startdate: "2014-02-04 12:44:00",
                //     enddate: "2014-02-04 17:44:00"
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting business appointments', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_crm_campaigns()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/crm/campaigns';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_campaigns               = $response->campaigns;

                $jsn = json_encode($og_campaigns, true);
                $jsn = json_decode($jsn, true);

                // {
                //     error: "Fail getting business campaigns",
                //     code_desc: "Command Done Successfully, but without rows",
                //     code: 1001
                // }

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting business campaigns', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    /* FIM CRM */

    /* SALES */

    function get_sales_documents()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/sales/documents';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                //$og_total                   = $response->total;
                $og_documents    = $response->documents;

                $jsn = json_encode($og_documents, true);
                $jsn = json_decode($jsn, true);

                // {
                //     type: "NC",
                //     description: "Credit Note",
                //     payment_document: ""
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting sales documents', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    function get_sales_receipts()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/sales/receipts';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                //$og_total                   = $response->total;
                $og_receipts                = $response->receipts;

                $jsn = json_encode($og_receipts, true);
                $jsn = json_decode($jsn, true);

                // {
                //     type: "R",
                //     description: "Receipt"
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting sales receipts', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    /* FIM SALES */

    /* PURCHASES */

    function get_purchases_documents()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/purchases/documents';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                //$og_total                   = $response->total;
                $og_documents    = $response->documents;

                $jsn = json_encode($og_documents, true);
                $jsn = json_decode($jsn, true);

                // {
                //     type: "CD",
                //     description: "Supplier Invoice / Receipt"
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting purchases documents', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    /* FIM PURCHASES */

    /* TABLES */

    function get_tables_privileges()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/tables/privileges';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_privileges    = $response->privileges;

                $jsn = json_encode($og_privileges, true);
                $jsn = json_decode($jsn, true);

                // {
                //     id: 16,
                //     description: "Administrativo Nivel 1"
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting tables privileges', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    /* FIM TABLES */

    /* REPORTS */

    function get_reports_sales_documents()
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/reports/sales/documents';

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->code == 1000) {

                $og_total                   = $response->total;
                $og_documents               = $response->documents;

                $jsn = json_encode($og_documents, true);
                $jsn = json_decode($jsn, true);

                // {
                //     serie: 191,
                //     employeecode: 1,
                //     vendorcode: 3,
                //     customertaxid: "503996726",
                //     documentnumber: "ENC 191/8242",
                //     canceled: "F",
                //     updatedstocks: "F",
                //     updatedcurrentaccount: "F",
                //     debit: "F",
                //     credit: "F",
                //     documentname: "Encomenda",
                //     documenttype: "E",
                //     generaltype: "WNE",
                //     number: 19108242,
                //     idcustomer: 2509,
                //     customername: "Bobinadora BJS S.A.",
                //     employeename: "Bruno Lopes",
                //     vendorname: "Bruno Lopes",
                //     date: "2020-01-01 00:00:00",
                //     datedue: "2020-01-31 00:00:00",
                //     total: 79.173501,
                //     totalwithoutvat: 64.3687,
                //     valuevat: 14.804801,
                //     valuepaid: 0,
                //     valuediscount: 0
                // },

                $dataset = [];

                foreach ($jsn as $arr) {
                    array_push($dataset, $arr);
                }

                $dataset = array(
                    'data' => $dataset
                );

                return json_encode($dataset, true); // informação para a vista para preencher a tabela com dados


            } else {
                return response()->json(['error' => 'Fail getting reports_sales_documents', 'code_desc' => $response->code_desc, 'code' => $response->code]);
            }
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
    }

    /* FIM REPORTS */

    /* Formulários Cubique.pt */

    // Este método serve o cliente para Obter Dados Relativos ao Equipamento com o uso do NIF e Designacao do Equipamento
    public function get_this_equipment_information($nif, $designacao_do_equip)
    {

        /*
        NIF: 513836250
        Numero de Equipamento: 22/2016_01 
        
        http://backoffice.test/api/get_info_equip/507839870/01*2019_01
        http://backoffice.test/api/get_info_equip/513836250/22*2016_01 
        */

        // tive de converter a barra em * porque fazer encode() nao resolvia o problema de passar como parametro
        $designacao_do_equip = str_replace('*', '/', $designacao_do_equip);

        $statusCode = 303;
        $url_redirect_main = "https://cubique.pt/cubitech/suporte/cliente/";

        try {
            $equipment = DB::connection('mysql_officegest')->select('
            
            SELECT 
            `eceq`.`designacao`     AS `designacao_do_equip`,
            `eceq`.`artigo_serial`  AS `numero_de_serie`,
            `cli`.`nome`            AS `nome_do_cliente`,
            `cli`.`telefone1`       AS `telefone_do_cliente`,
            `cli`.`email`           AS `email_do_cliente`,
            `cli`.`ncontrib`        AS `nif_do_cliente`,
            `ccontr`.`num_contrato` AS `num_contrato`,
            `emmod`.`designacao`    AS `modelo_designacao`,
            `emmod`.`tip_cor`       AS `tipo_cor`,
            `eceq`.`id`             AS `equipamento_id`,
            `contrato_id`           AS `contrato_id`
            FROM   `a_cub_eqequipamentos` `eceq`
                JOIN `a_cub_eqcontratos` `ccontr`
                    ON `ccontr`.`id` = `eceq`.`contrato_id`
                JOIN `a_cub_eqmodelos` `emmod`
                    ON `eceq`.`modelo_id` = `emmod`.`id`
                LEFT JOIN `clientes` `cli`
                        ON `cli`.`codterc` = `ccontr`.`clfinal_id`
            WHERE  `eceq`.`activo` = "T"

            AND     `eceq`.`designacao`  LIKE "%' . $designacao_do_equip . '%"
            AND     `cli`.`NContrib`        = ' . $nif . '
    
            ORDER BY `eceq`.`id` desc;
                
                ', [1]);

        } catch (QueryException $ex) {
            report($ex);

            header('Location: ' . $url_redirect_main . '#invalid_parameters', true, $statusCode);
            die();
        }


        // se não encontrou o equipamento devolve erro
        if ($equipment == null) {
            header('Location: ' . $url_redirect_main . '#invalid_parameters', true, $statusCode);
            die();
        }

        if (strpos($equipment[0]->modelo_designacao, 'Canon') !== false) {
            if ($equipment[0]->tipo_cor == "MONO") {
                $brand = "Canon - Preto";
            }

            if ($equipment[0]->tipo_cor == "COR") {
                $brand = "Canon - Cor";
            }
        }

        if (strpos($equipment[0]->modelo_designacao, 'Develop') !== false) {
            $brand = "Develop";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Epson') !== false) {
            $brand = "Epson";
        }

        if (strpos($equipment[0]->modelo_designacao, 'HP') !== false) {
            $brand = "HP";
        }

        // hp managed color?

        if (strpos($equipment[0]->modelo_designacao, 'Konica') !== false) {
            $brand = "Konica";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Kyocera') !== false) {
            if ($equipment[0]->tipo_cor == "MONO") {
                $brand = "Kyocera - Preto";
            }

            if ($equipment[0]->tipo_cor == "COR") {
                $brand = "Kyocera - Cor";
            }
        }

        if (strpos($equipment[0]->modelo_designacao, 'Lexmark') !== false) {
            $brand = "Lexmark";
        }

        if (strpos($equipment[0]->modelo_designacao, 'OKI') !== false) {
            $brand = "OKI";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Olivetti') !== false) {
            $brand = "Olivetti";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Ricoh') !== false) {
            $brand = "Ricoh";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Samsung') !== false) {
            $brand = "Samsung";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Toshiba') !== false) {
            $brand = "Toshiba";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Xerox') !== false) {
            $brand = "Xerox";
        }

        // ele partia caso nao tivesse o caracter espaço
        if (strpos($equipment[0]->designacao_do_equip, ' ') !== false) {
            $num_equip = substr($equipment[0]->designacao_do_equip, 0, strpos($equipment[0]->designacao_do_equip, " "));
        } else {
            $num_equip = $equipment[0]->designacao_do_equip;
        }

        $url_form_filled =

            "?nif=" .               $equipment[0]->nif_do_cliente .

            "&nome_empresa=" .      $equipment[0]->nome_do_cliente .
            "&email=" .             $equipment[0]->email_do_cliente .

            "&telefone=" .          $equipment[0]->telefone_do_cliente .

            "&marca=" .             $brand .
            "&contrato=Sim" .

            "&num_contrato=" .      $equipment[0]->num_contrato .
            "&num_equipamento=" .   $num_equip .

            "&id_equip=" .    $equipment[0]->equipamento_id .
            "&id_contr=" .    $equipment[0]->contrato_id .

            "#valid_parameters";

        $url = $url_redirect_main . $url_form_filled;

        // nao dá(va)?
        // 503906573
        // 100/2019_02

        header('Location: ' . $url, true, $statusCode);
        die();
    }

    public function get_this_equipment_information_by_unique_key_old($unique_key)
    {

        /*
        Unique Key: 28453168
        http://backoffice.test/api/get_info_equip_unique_key/28453168/
        */

        $statusCode = 303;
        $url_redirect_main = "https://cubique.pt/cubitech/suporte/cliente/";

        try {
            $equipment = DB::connection('mysql_officegest')->select('
            
            SELECT 
            `eceq`.`designacao`     AS `designacao_do_equip`,
            `eceq`.`artigo_serial`  AS `numero_de_serie`,
            `cli`.`nome`            AS `nome_do_cliente`,
            `cli`.`telefone1`       AS `telefone_do_cliente`,
            `cli`.`email`           AS `email_do_cliente`,
            `cli`.`ncontrib`        AS `nif_do_cliente`,
            `ccontr`.`num_contrato` AS `num_contrato`,
            `emmod`.`designacao`    AS `modelo_designacao`,
            `emmod`.`tip_cor`       AS `tipo_cor`,
            `eceq`.`id`             AS `equipamento_id`,
            `contrato_id`           AS `contrato_id`
            FROM   `a_cub_eqequipamentos` `eceq`
                JOIN `a_cub_eqcontratos` `ccontr`
                    ON `ccontr`.`id` = `eceq`.`contrato_id`
                JOIN `a_cub_eqmodelos` `emmod`
                    ON `eceq`.`modelo_id` = `emmod`.`id`
                LEFT JOIN `clientes` `cli`
                        ON `cli`.`codterc` = `ccontr`.`clfinal_id`
            WHERE  `eceq`.`activo` = "T"

            AND     `eceq`.`designacao`  LIKE "%' . $unique_key . '%"
    
            ORDER BY `eceq`.`id` desc;
                
                ', [1]);
        
        } catch (QueryException $ex) {
            report($ex);

            header('Location: ' . $url_redirect_main . '#invalid_parameters', true, $statusCode);
            die();
        }


        // se não encontrou o equipamento devolve erro
        if ($equipment == null) {
            header('Location: ' . $url_redirect_main . '#invalid_parameters', true, $statusCode);
            die();
        }

        if (strpos($equipment[0]->modelo_designacao, 'Canon') !== false) {
            if ($equipment[0]->tipo_cor == "MONO") {
                $brand = "Canon - Preto";
            }

            if ($equipment[0]->tipo_cor == "COR") {
                $brand = "Canon - Cor";
            }
        }

        if (strpos($equipment[0]->modelo_designacao, 'Develop') !== false) {
            $brand = "Develop";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Epson') !== false) {
            $brand = "Epson";
        }

        if (strpos($equipment[0]->modelo_designacao, 'HP') !== false) {
            $brand = "HP";
        }

        // hp managed color?

        if (strpos($equipment[0]->modelo_designacao, 'Konica') !== false) {
            $brand = "Konica";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Kyocera') !== false) {
            if ($equipment[0]->tipo_cor == "MONO") {
                $brand = "Kyocera - Preto";
            }

            if ($equipment[0]->tipo_cor == "COR") {
                $brand = "Kyocera - Cor";
            }
        }

        if (strpos($equipment[0]->modelo_designacao, 'Lexmark') !== false) {
            $brand = "Lexmark";
        }

        if (strpos($equipment[0]->modelo_designacao, 'OKI') !== false) {
            $brand = "OKI";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Olivetti') !== false) {
            $brand = "Olivetti";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Ricoh') !== false) {
            $brand = "Ricoh";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Samsung') !== false) {
            $brand = "Samsung";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Toshiba') !== false) {
            $brand = "Toshiba";
        }

        if (strpos($equipment[0]->modelo_designacao, 'Xerox') !== false) {
            $brand = "Xerox";
        }

        // ele partia caso nao tivesse o caracter espaço
        if (strpos($equipment[0]->designacao_do_equip, ' ') !== false) {
            $num_equip = substr($equipment[0]->designacao_do_equip, 0, strpos($equipment[0]->designacao_do_equip, " "));
        } else {
            $num_equip = $equipment[0]->designacao_do_equip;
        }

        $url_form_filled =

            "?nif=" .               $equipment[0]->nif_do_cliente .

            "&nome_empresa=" .      $equipment[0]->nome_do_cliente .
            "&email=" .             $equipment[0]->email_do_cliente .

            "&telefone=" .          $equipment[0]->telefone_do_cliente .

            "&marca=" .             $brand .
            "&contrato=Sim" .

            "&num_contrato=" .      $equipment[0]->num_contrato .
            "&num_equipamento=" .   $num_equip .

            "&id_equip=" .    $equipment[0]->equipamento_id .
            "&id_contr=" .    $equipment[0]->contrato_id .

            "#valid_parameters";

        $url = $url_redirect_main . $url_form_filled;

        // nao dá(va)?
        // 503906573
        // 100/2019_02

        header('Location: ' . $url, true, $statusCode);
        die();
    }

    public function get_this_equipment_information_by_unique_key($unique_key)
    {
        $url            = Config::get('variables.officegest.url');
        $url            = $url . '/addon/printing/printer_data/'. $unique_key;

        $og_user        = Config::get('variables.officegest.username');

        $og_key         = $this->get_login_hash();          // FAZ O LOGIN


        $guzzle_client = new Client([
            'verify' => false
        ]);

        $request = $this->guzzle_call_og_api(
            $guzzle_client,
            'GET',
            $url,
            $og_user,
            $og_key
        );
        
        if ($request['status'] == 200) {
            $response = json_decode($request['body']);

            if ($response->link) {
                header('Location: ' . $response->link, true, 303);
                die();
            } else {
                header('Location: ' . 'https://cubique.pt/suporte/cliente/', true, 303);
                die();
            }
            
        } else {
            return response()->json(['error' => 'error', 'code' => $response['status']]);
        }
        
    }
    
    public function get_mapa_comissoes_2018_2019()
    {

        ini_set('memory_limit', '-1');

        $inicio_ano = date("2018-01-01");
        $fim_ano = date("2019-12-31");

        try {
            $map_com = DB::connection('mysql_officegest')->select('
            
            SELECT `a`.*, `est`.`lang`, `est`.`designacao` as `desig_estado`, `est`.`class_label`
            FROM (SELECT (IFNULL(AR.MargemLucro,0) * DV.docorDevol) AS MARGENLUCRO, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as VALORLUCROUNITA, ((((DV.PrecoUni * DV.qtd) - (ValorDesc)) - (IFNULL(AR.PrecoCusto,0) * DV.qtd)) * DV.docorDevol) as VALORLUCROTOTAL, `DV`.`num` as `NUMDOC`, `DV`.`TipoOp` as `TIPODOC`, `DV`.`Data` as `DATAFACTURA`, (DV.qtd * DV.docorDevol) as CANTIDAD, IFNULL(AR.PerComiss,0) AS COMISION, IFNULL(DV.PrecoUni,0) AS PRECIOUNIDAD, `DV`.`IVA` as `IVA`, (IFNULL(DV.TotalLinha,0) * DV.docorDevol) AS TOTALIVA, (IFNULL(DV.TotalSIVA,0) * DV.docorDevol) AS TOTALSIVA, CONCAT("[",DV.CodArtigo,"] ",DV.Designacao) as DESIGNA, `DV`.`CodArtigo` as `codartigo`, `DV`.`Designacao` as `desigartigo`, ((DV.PrecoUni * IFNULL(DV.qtd,0))-(ValorDesc)) as VALORVENTA, IFNULL(ValorDesc,0), IFNULL((DV.PrecoUni * DV.qtd),0) AS VALORVENDSEMDESC, IFNULL(AR.PrecoCusto,0) AS PrecoCusto, IFNULL(AR.PrecoCustoMMovel,0) AS PRECIOCOSTEMEDIO, IFNULL(AR.PrecoCustoUlt,0) AS PRECIOCOSTEULT, `AR`.`CodFam` as `CODFAMILIA`, `AR`.`CodSFam` as `CODSFAMILIA`, `AR`.`CodSSFam` as `CODSSFAMILIA`, `DV`.`CodTerc` as `CODTERC`, `DV`.`CodVend`, (DV.PCMLDoc * DV.docorDevol) AS PrecoCustoMedLinha, `DV`.`documentnumber`, `dt`.`firstdata` AS `DataRegCliente`, `cli`.`Nome` AS `NomeCliente`, `vend`.`Nome` AS `NomeVendedor`, `DV`.`CodEmpr`, `emp`.`nome` AS `nomeempregado`, `cli`.`gestorcliente` as `CodGestorcliente`, `ges`.`nome` as `NomeGestorcliente`, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as ValorLucroPC, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoUlt,0))) * DV.docorDevol) as ValorLucroPCU, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoMMovel,0))) * DV.docorDevol) as ValorLucroPCM, IFNULL(DV.pcmldoc,0) AS pcm_documento, (DV.MargemLDoc * DV.docorDevol) AS ValorLucroPCMLinha, CASE WHEN DV.Estado in("F","FAC","ESP","MAR","EXE","RAS","APR","ANU") THEN DV.Estado ELSE CASE WHEN (DV.Estado = "FEC") THEN "F" ELSE CASE WHEN ROUND(DV.Total, 2) = round(DV.valorpago,2) AND round(DV.valorpago,2) != 0 THEN "LIQ" ELSE  CASE WHEN round(DV.valorpago,2) > 0 THEN "LQP" ELSE  DV.Estado END END END END AS Estado
            FROM `docsvenda` `DV`
            LEFT JOIN `artigos` `AR` ON `DV`.`CodArtigo` = `AR`.`CodArtigo`
            LEFT JOIN (SELECT `codterc` as `cliente`, MIN(data) as firstdata
            FROM `dumpdocumentstable`
            WHERE `compras` = "F" AND `actualizacc` = "T"
            GROUP BY `codterc`) dt ON `dt`.`cliente`=`DV`.`codterc`
            LEFT JOIN (SELECT `codterc`, `nome`, `datareg`, `codclassif`, `gestorcliente`
            FROM `clientes`) cli ON `cli`.`codterc`=`DV`.`codterc`
            LEFT JOIN `represnt` `vend` ON `vend`.`codrepres`=`DV`.`codvend`
            LEFT JOIN `empreg` `emp` ON `emp`.`codempr`=`DV`.`codempr`
            LEFT JOIN `empreg` `ges` ON `cli`.`gestorcliente` = `ges`.`CodEmpr`
            WHERE `Data` >= "'.$inicio_ano.'" AND `Data` <= "'.$fim_ano.'" AND `dt`.`firstdata` >= "2000-01-01 00:00:00" AND `dt`.`firstdata` <= "'.$fim_ano.' 23:59:59" AND `AR`.`CodArtigo` != "." AND `DV`.`CodVend` IN("", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35")) a
            LEFT JOIN `estados` `est` ON `a`.`estado`=`est`.`codestado`
                
            ', [1]);

            return $map_com;


        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

    public function get_mapa_comissoes_2019_2020()
    {

        ini_set('memory_limit', '-1');
        
        $inicio_ano = date("2019-01-01");
        $fim_ano = date("2020-12-31");

        try {
            $map_com = DB::connection('mysql_officegest')->select('
            
            SELECT `a`.*, `est`.`lang`, `est`.`designacao` as `desig_estado`, `est`.`class_label`
            FROM (SELECT (IFNULL(AR.MargemLucro,0) * DV.docorDevol) AS MARGENLUCRO, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as VALORLUCROUNITA, ((((DV.PrecoUni * DV.qtd) - (ValorDesc)) - (IFNULL(AR.PrecoCusto,0) * DV.qtd)) * DV.docorDevol) as VALORLUCROTOTAL, `DV`.`num` as `NUMDOC`, `DV`.`TipoOp` as `TIPODOC`, `DV`.`Data` as `DATAFACTURA`, (DV.qtd * DV.docorDevol) as CANTIDAD, IFNULL(AR.PerComiss,0) AS COMISION, IFNULL(DV.PrecoUni,0) AS PRECIOUNIDAD, `DV`.`IVA` as `IVA`, (IFNULL(DV.TotalLinha,0) * DV.docorDevol) AS TOTALIVA, (IFNULL(DV.TotalSIVA,0) * DV.docorDevol) AS TOTALSIVA, CONCAT("[",DV.CodArtigo,"] ",DV.Designacao) as DESIGNA, `DV`.`CodArtigo` as `codartigo`, `DV`.`Designacao` as `desigartigo`, ((DV.PrecoUni * IFNULL(DV.qtd,0))-(ValorDesc)) as VALORVENTA, IFNULL(ValorDesc,0), IFNULL((DV.PrecoUni * DV.qtd),0) AS VALORVENDSEMDESC, IFNULL(AR.PrecoCusto,0) AS PrecoCusto, IFNULL(AR.PrecoCustoMMovel,0) AS PRECIOCOSTEMEDIO, IFNULL(AR.PrecoCustoUlt,0) AS PRECIOCOSTEULT, `AR`.`CodFam` as `CODFAMILIA`, `AR`.`CodSFam` as `CODSFAMILIA`, `AR`.`CodSSFam` as `CODSSFAMILIA`, `DV`.`CodTerc` as `CODTERC`, `DV`.`CodVend`, (DV.PCMLDoc * DV.docorDevol) AS PrecoCustoMedLinha, `DV`.`documentnumber`, `dt`.`firstdata` AS `DataRegCliente`, `cli`.`Nome` AS `NomeCliente`, `vend`.`Nome` AS `NomeVendedor`, `DV`.`CodEmpr`, `emp`.`nome` AS `nomeempregado`, `cli`.`gestorcliente` as `CodGestorcliente`, `ges`.`nome` as `NomeGestorcliente`, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as ValorLucroPC, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoUlt,0))) * DV.docorDevol) as ValorLucroPCU, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoMMovel,0))) * DV.docorDevol) as ValorLucroPCM, IFNULL(DV.pcmldoc,0) AS pcm_documento, (DV.MargemLDoc * DV.docorDevol) AS ValorLucroPCMLinha, CASE WHEN DV.Estado in("F","FAC","ESP","MAR","EXE","RAS","APR","ANU") THEN DV.Estado ELSE CASE WHEN (DV.Estado = "FEC") THEN "F" ELSE CASE WHEN ROUND(DV.Total, 2) = round(DV.valorpago,2) AND round(DV.valorpago,2) != 0 THEN "LIQ" ELSE  CASE WHEN round(DV.valorpago,2) > 0 THEN "LQP" ELSE  DV.Estado END END END END AS Estado
            FROM `docsvenda` `DV`
            LEFT JOIN `artigos` `AR` ON `DV`.`CodArtigo` = `AR`.`CodArtigo`
            LEFT JOIN (SELECT `codterc` as `cliente`, MIN(data) as firstdata
            FROM `dumpdocumentstable`
            WHERE `compras` = "F" AND `actualizacc` = "T"
            GROUP BY `codterc`) dt ON `dt`.`cliente`=`DV`.`codterc`
            LEFT JOIN (SELECT `codterc`, `nome`, `datareg`, `codclassif`, `gestorcliente`
            FROM `clientes`) cli ON `cli`.`codterc`=`DV`.`codterc`
            LEFT JOIN `represnt` `vend` ON `vend`.`codrepres`=`DV`.`codvend`
            LEFT JOIN `empreg` `emp` ON `emp`.`codempr`=`DV`.`codempr`
            LEFT JOIN `empreg` `ges` ON `cli`.`gestorcliente` = `ges`.`CodEmpr`
            WHERE `Data` >= "'.$inicio_ano.'" AND `Data` <= "'.$fim_ano.'" AND `dt`.`firstdata` >= "2000-01-01 00:00:00" AND `dt`.`firstdata` <= "'.$fim_ano.' 23:59:59" AND `AR`.`CodArtigo` != "." AND `DV`.`CodVend` IN("", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35")) a
            LEFT JOIN `estados` `est` ON `a`.`estado`=`est`.`codestado`
                
            ', [1]);

            return $map_com;


        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }
    public function get_product_families()
    {


        try {
            $var = DB::connection('mysql_officegest')->select('
            
            select distinct `CodFam`, `Designacao`
            from familias
            where `CodFamMae` = "^"
            order by 1 asc;
                
            ', [1]);

            return $var;


        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

    public function get_product_subfamilies()
    {


        try {
            $var = DB::connection('mysql_officegest')->select('
            
            select distinct `CSFamília`, `Sub-Família`, `CFamília`, `Família`
            from famílias_nova
            where `Sub-Família` <> ""
            order by 1 asc;

            ', [1]);

            return $var;


        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

    public function get_product_subsubfamilies()
    {


        try {
            $var = DB::connection('mysql_officegest')->select('
            
            select distinct `CodAux`, `Designacao`
            from familias
            where length(`CodAux`) = 4
            order by 1 asc;

            ', [1]);

            return $var;


        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

    public function get_mapa_comissoes_comerciais_indor_2020()
    {

        ini_set('memory_limit', '-1');

        $inicio_ano = date("2020-01-01");
        $fim_ano = date("2020-12-31");

        try {
            $map_com = DB::connection('mysql_officegest')->select('
            
            SELECT `a`.*, `est`.`lang`, `est`.`designacao` as `desig_estado`, `est`.`class_label`
            FROM (SELECT (IFNULL(AR.MargemLucro,0) * DV.docorDevol) AS MARGENLUCRO, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as VALORLUCROUNITA, ((((DV.PrecoUni * DV.qtd) - (ValorDesc)) - (IFNULL(AR.PrecoCusto,0) * DV.qtd)) * DV.docorDevol) as VALORLUCROTOTAL, `DV`.`num` as `NUMDOC`, `DV`.`TipoOp` as `TIPODOC`, `DV`.`Data` as `DATAFACTURA`, (DV.qtd * DV.docorDevol) as CANTIDAD, IFNULL(AR.PerComiss,0) AS COMISION, IFNULL(DV.PrecoUni,0) AS PRECIOUNIDAD, `DV`.`IVA` as `IVA`, (IFNULL(DV.TotalLinha,0) * DV.docorDevol) AS TOTALIVA, (IFNULL(DV.TotalSIVA,0) * DV.docorDevol) AS TOTALSIVA, CONCAT("[",DV.CodArtigo,"] ",DV.Designacao) as DESIGNA, `DV`.`CodArtigo` as `codartigo`, `DV`.`Designacao` as `desigartigo`, ((DV.PrecoUni * IFNULL(DV.qtd,0))-(ValorDesc)) as VALORVENTA, IFNULL(ValorDesc,0), IFNULL((DV.PrecoUni * DV.qtd),0) AS VALORVENDSEMDESC, IFNULL(AR.PrecoCusto,0) AS PrecoCusto, IFNULL(AR.PrecoCustoMMovel,0) AS PRECIOCOSTEMEDIO, IFNULL(AR.PrecoCustoUlt,0) AS PRECIOCOSTEULT, `AR`.`CodFam` as `CODFAMILIA`, `AR`.`CodSFam` as `CODSFAMILIA`, `AR`.`CodSSFam` as `CODSSFAMILIA`, `DV`.`CodTerc` as `CODTERC`, `DV`.`CodVend`, (DV.PCMLDoc * DV.docorDevol) AS PrecoCustoMedLinha, `DV`.`documentnumber`, `dt`.`firstdata` AS `DataRegCliente`, `cli`.`Nome` AS `NomeCliente`, `vend`.`Nome` AS `NomeVendedor`, `DV`.`CodEmpr`, `emp`.`nome` AS `nomeempregado`, `cli`.`gestorcliente` as `CodGestorcliente`, `ges`.`nome` as `NomeGestorcliente`, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as ValorLucroPC, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoUlt,0))) * DV.docorDevol) as ValorLucroPCU, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoMMovel,0))) * DV.docorDevol) as ValorLucroPCM, IFNULL(DV.pcmldoc,0) AS pcm_documento, (DV.MargemLDoc * DV.docorDevol) AS ValorLucroPCMLinha, CASE WHEN DV.Estado in("F","FAC","ESP","MAR","EXE","RAS","APR","ANU") THEN DV.Estado ELSE CASE WHEN (DV.Estado = "FEC") THEN "F" ELSE CASE WHEN ROUND(DV.Total, 2) = round(DV.valorpago,2) AND round(DV.valorpago,2) != 0 THEN "LIQ" ELSE  CASE WHEN round(DV.valorpago,2) > 0 THEN "LQP" ELSE  DV.Estado END END END END AS Estado
            FROM `docsvenda` `DV`
            LEFT JOIN `artigos` `AR` ON `DV`.`CodArtigo` = `AR`.`CodArtigo`
            LEFT JOIN (SELECT `codterc` as `cliente`, MIN(data) as firstdata
            FROM `dumpdocumentstable`
            WHERE `compras` = "F" AND `actualizacc` = "T"
            GROUP BY `codterc`) dt ON `dt`.`cliente`=`DV`.`codterc`
            LEFT JOIN (SELECT `codterc`, `nome`, `datareg`, `codclassif`, `gestorcliente`
            FROM `clientes`) cli ON `cli`.`codterc`=`DV`.`codterc`
            LEFT JOIN `represnt` `vend` ON `vend`.`codrepres`=`DV`.`codvend`
            LEFT JOIN `empreg` `emp` ON `emp`.`codempr`=`DV`.`codempr`
            LEFT JOIN `empreg` `ges` ON `cli`.`gestorcliente` = `ges`.`CodEmpr`
            WHERE `Data` >= "'.$inicio_ano.'" AND `Data` <= "'.$fim_ano.'" AND `dt`.`firstdata` >= "2000-01-01 00:00:00" AND `dt`.`firstdata` <= "'.$fim_ano.' 23:59:59" AND `AR`.`CodArtigo` != "." AND `DV`.`CodVend` IN("", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35")) a
            LEFT JOIN `estados` `est` ON `a`.`estado`=`est`.`codestado`
                
            ', [1]);

            return $map_com;


        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

    public function get_mapa_comissoes_comerciais_indor_2021_old_broken()
    {

        ini_set('memory_limit', '-1');

        $inicio_ano = date("2021-01-01");
        $fim_ano = date("2021-12-31");

        try {
            $map_com = DB::connection('mysql_officegest')->select('
            
            SELECT `a`.*, `est`.`lang`, `est`.`designacao` as `desig_estado`, `est`.`class_label`
            FROM (SELECT (IFNULL(AR.MargemLucro,0) * DV.docorDevol) AS MARGENLUCRO, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as VALORLUCROUNITA, ((((DV.PrecoUni * DV.qtd) - (ValorDesc)) - (IFNULL(AR.PrecoCusto,0) * DV.qtd)) * DV.docorDevol) as VALORLUCROTOTAL, `DV`.`num` as `NUMDOC`, `DV`.`TipoOp` as `TIPODOC`, `DV`.`Data` as `DATAFACTURA`, (DV.qtd * DV.docorDevol) as CANTIDAD, IFNULL(AR.PerComiss,0) AS COMISION, IFNULL(DV.PrecoUni,0) AS PRECIOUNIDAD, `DV`.`IVA` as `IVA`, (IFNULL(DV.TotalLinha,0) * DV.docorDevol) AS TOTALIVA, (IFNULL(DV.TotalSIVA,0) * DV.docorDevol) AS TOTALSIVA, CONCAT("[",DV.CodArtigo,"] ",DV.Designacao) as DESIGNA, `DV`.`CodArtigo` as `codartigo`, `DV`.`Designacao` as `desigartigo`, ((DV.PrecoUni * IFNULL(DV.qtd,0))-(ValorDesc)) as VALORVENTA, IFNULL(ValorDesc,0), IFNULL((DV.PrecoUni * DV.qtd),0) AS VALORVENDSEMDESC, IFNULL(AR.PrecoCusto,0) AS PrecoCusto, IFNULL(AR.PrecoCustoMMovel,0) AS PRECIOCOSTEMEDIO, IFNULL(AR.PrecoCustoUlt,0) AS PRECIOCOSTEULT, `AR`.`CodFam` as `CODFAMILIA`, `AR`.`CodSFam` as `CODSFAMILIA`, `AR`.`CodSSFam` as `CODSSFAMILIA`, `DV`.`CodTerc` as `CODTERC`, `DV`.`CodVend`, (DV.PCMLDoc * DV.docorDevol) AS PrecoCustoMedLinha, `DV`.`documentnumber`, `dt`.`firstdata` AS `DataRegCliente`, `cli`.`Nome` AS `NomeCliente`, `vend`.`Nome` AS `NomeVendedor`, `DV`.`CodEmpr`, `emp`.`nome` AS `nomeempregado`, `cli`.`gestorcliente` as `CodGestorcliente`, `ges`.`nome` as `NomeGestorcliente`, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as ValorLucroPC, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoUlt,0))) * DV.docorDevol) as ValorLucroPCU, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoMMovel,0))) * DV.docorDevol) as ValorLucroPCM, IFNULL(DV.pcmldoc,0) AS pcm_documento, (DV.MargemLDoc * DV.docorDevol) AS ValorLucroPCMLinha, CASE WHEN DV.Estado in("F","FAC","ESP","MAR","EXE","RAS","APR","ANU") THEN DV.Estado ELSE CASE WHEN (DV.Estado = "FEC") THEN "F" ELSE CASE WHEN ROUND(DV.Total, 2) = round(DV.valorpago,2) AND round(DV.valorpago,2) != 0 THEN "LIQ" ELSE  CASE WHEN round(DV.valorpago,2) > 0 THEN "LQP" ELSE  DV.Estado END END END END AS Estado
            FROM `docsvenda` `DV`
            LEFT JOIN `artigos` `AR` ON `DV`.`CodArtigo` = `AR`.`CodArtigo`
            LEFT JOIN (SELECT `codterc` as `cliente`, MIN(data) as firstdata
            FROM `dumpdocumentstable`
            WHERE `compras` = "F" AND `actualizacc` = "T"
            GROUP BY `codterc`) dt ON `dt`.`cliente`=`DV`.`codterc`
            LEFT JOIN (SELECT `codterc`, `nome`, `datareg`, `codclassif`, `gestorcliente`
            FROM `clientes`) cli ON `cli`.`codterc`=`DV`.`codterc`
            LEFT JOIN `represnt` `vend` ON `vend`.`codrepres`=`DV`.`codvend`
            LEFT JOIN `empreg` `emp` ON `emp`.`codempr`=`DV`.`codempr`
            LEFT JOIN `empreg` `ges` ON `cli`.`gestorcliente` = `ges`.`CodEmpr`
            WHERE `Data` >= "'.$inicio_ano.'" AND `Data` <= "'.$fim_ano.'" AND `dt`.`firstdata` >= "2000-01-01 00:00:00" AND `dt`.`firstdata` <= "'.$fim_ano.' 23:59:59" 
                AND `AR`.`CodArtigo` != ".") a
            LEFT JOIN `estados` `est` ON `a`.`estado`=`est`.`codestado`
                
            ', [1]);

                // AND `AR`.`CodArtigo` != "." AND `DV`.`CodVend` IN("", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35")) a
            return $map_com;


        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

    public function teste()
    {

        ini_set('memory_limit', '-1');

        $inicio_ano = date("2020-01-01");
        $fim_ano = date("2020-12-31");

        try {
            $map_com = DB::connection('mysql_officegest')->select('
            SELECT `a`.*, `est`.`lang`, `est`.`designacao` as `desig_estado`, `est`.`class_label`
            FROM (SELECT (IFNULL(AR.MargemLucro,0) * DV.docorDevol) AS MARGENLUCRO, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as VALORLUCROUNITA, ((((DV.PrecoUni * DV.qtd) - (ValorDesc)) - (IFNULL(AR.PrecoCusto,0) * DV.qtd)) * DV.docorDevol) as VALORLUCROTOTAL, `DV`.`num` as `NUMDOC`, `DV`.`TipoOp` as `TIPODOC`, `DV`.`Data` as `DATAFACTURA`, (DV.qtd * DV.docorDevol) as CANTIDAD, IFNULL(AR.PerComiss,0) AS COMISION, IFNULL(DV.PrecoUni,0) AS PRECIOUNIDAD, `DV`.`IVA` as `IVA`, (IFNULL(DV.TotalLinha,0) * DV.docorDevol) AS TOTALIVA, (IFNULL(DV.TotalSIVA,0) * DV.docorDevol) AS TOTALSIVA, CONCAT("[",DV.CodArtigo,"] ",DV.Designacao) as DESIGNA, `DV`.`CodArtigo` as `codartigo`, `DV`.`Designacao` as `desigartigo`, ((DV.PrecoUni * IFNULL(DV.qtd,0))-(ValorDesc)) as VALORVENTA, IFNULL(ValorDesc,0), IFNULL((DV.PrecoUni * DV.qtd),0) AS VALORVENDSEMDESC, IFNULL(AR.PrecoCusto,0) AS PrecoCusto, IFNULL(AR.PrecoCustoMMovel,0) AS PRECIOCOSTEMEDIO, IFNULL(AR.PrecoCustoUlt,0) AS PRECIOCOSTEULT, `AR`.`CodMarca` as `CODMARCA`, `AR`.`CodFam` as `CODFAMILIA`, `AR`.`CodSFam` as `CODSFAMILIA`, `AR`.`CodSSFam` as `CODSSFAMILIA`, `DV`.`CodTerc` as `CODTERC`, `DV`.`CodVend`, (DV.PCMLDoc * DV.docorDevol) AS PrecoCustoMedLinha, `DV`.`documentnumber`, `dt`.`firstdata` AS `DataRegCliente`, `cli`.`Nome` AS `NomeCliente`, `vend`.`Nome` AS `NomeVendedor`, `DV`.`CodEmpr`, `emp`.`nome` AS `nomeempregado`, `cli`.`gestorcliente` as `CodGestorcliente`, `class`.`designacao` as `CodClassif`, `ges`.`nome` as `NomeGestorcliente`, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCusto,0))) * DV.docorDevol) as ValorLucroPC, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoUlt,0))) * DV.docorDevol) as ValorLucroPCU, (((DV.PrecoUni) - (ValorDesc) - (IFNULL(AR.PrecoCustoMMovel,0))) * DV.docorDevol) as ValorLucroPCM, IFNULL(DV.pcmldoc,0) AS pcm_documento, (DV.MargemLDoc * DV.docorDevol) AS ValorLucroPCMLinha, CASE WHEN DV.Estado in("F","FAC","ESP","MAR","EXE","RAS","APR","ANU") THEN DV.Estado ELSE CASE WHEN (DV.Estado = "FEC") THEN "F" ELSE CASE WHEN ROUND(DV.Total, 2) = round(DV.valorpago,2) AND round(DV.valorpago,2) != 0 THEN "LIQ" ELSE CASE WHEN round(DV.valorpago,2) > 0 THEN "LQP" ELSE DV.Estado END END END END AS Estado
            FROM `docsvenda` `DV`
            LEFT JOIN `artigos` `AR` ON `DV`.`CodArtigo` = `AR`.`CodArtigo`
            LEFT JOIN (SELECT `codterc` as `cliente`, MIN(data) as firstdata
            FROM `dumpdocumentstable`
            WHERE `compras` = "F" AND `actualizacc` = "T"
            GROUP BY `codterc`) dt ON `dt`.`cliente`=`DV`.`codterc`
            LEFT JOIN (SELECT `codterc`, `nome`, `datareg`, `codclassif`, `gestorcliente`
            FROM `clientes`) cli ON `cli`.`codterc`=`DV`.`codterc`
            LEFT JOIN `classif` `class` ON `cli`.`codclassif`=`class`.`codclassif`
            LEFT JOIN `represnt` `vend` ON `vend`.`codrepres`=`DV`.`codvend`
            LEFT JOIN `empreg` `emp` ON `emp`.`codempr`=`DV`.`codempr`
            LEFT JOIN `empreg` `ges` ON `cli`.`gestorcliente` = `ges`.`CodEmpr`
            WHERE `Data` >= "2021-01-01" AND `Data` <= "2021-12-31" 
            AND `DV`.`CodVend` IN("", "1""38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60")) a
            LEFT JOIN `estados` `est` ON `a`.`estado`=`est`.`codestado`                
            ', [1]);

            return $map_com;

        } catch (QueryException $ex) {
            report($ex);
            die();
        }
        die();
    }

    public function get_relatorio_vendas_geral_documentos_2020()
    {

        ini_set('memory_limit', '-1');

        $inicio_ano = date("2020-01-01");
        $fim_ano = date("2020-12-31");

        try {
            $map_com = DB::connection('mysql_officegest')->select('
            
            SELECT `a`.*, `est`.`lang`, `est`.`designacao` as `desig_estado`, `est`.`class_label`
            FROM (SELECT `l`.`coddiario`, `l`.`numrddiario`, `TD`.`DocNome`, `TD`.`Debita`, `TD`.`Credita`, `TD`.`typesaft`, `TD`.`DocumentNumber`, ( CASE WHEN TD.CodAbreviado = "NGT" THEN forn.Nome ELSE cli.Nome END ) as NomeCli, ( CASE WHEN TD.CodAbreviado = "NGT" THEN forn.NContrib ELSE cli.NContrib END ) as NContrib, `emp`.`Nome` as `NomeEmp`, `repr`.`Nome` as `NomeRepr`, `TD`.`CodTerc`, `TD`.`CodEmpr`, `TD`.`CodVend`, `TD`.`Num`, `TD`.`Data`, `TD`.`valordesc` as `valordesc`, `TD`.`Total` as `Total`, `TD`.`TipoOp`, `TD`.`ValorIVA` as `ValorIVA`, `TD`.`TotalSIVA` as `TotalSIVA`, `TD`.`Anulado`, `TD`.`recibo`, `cli`.`codzona`, `zon`.`designacao` as `zona_designacao`, CASE WHEN TD.Estado in("F","FAC","ESP","MAR","EXE","RAS","APR","ANU") THEN TD.Estado ELSE CASE WHEN ((TD.Estado = "FEC") OR (TD.Anulado="F" AND TD.Estado="ABR" AND TD.recibo = "T")) THEN "F" ELSE CASE WHEN ROUND(TD.Total, 2) = round(TD.valorpago,2) AND round(TD.valorpago,2) != 0 THEN "LIQ" ELSE  CASE WHEN round(TD.valorpago,2) > 0 THEN "LQP" ELSE  TD.Estado END END END END AS Estado, `serie`.`designacao` as `serie_designation`, `td`.`nserie`, `td`.`datareg`, `forn`.`Nome` as `NomeForn`, `forn`.`NContrib` as `NContribForn`, `cli`.`Nome` as `NomeCliente`, `cli`.`NContrib` as `NContribCliente`, `moti`.`description` as `desig_motivo`, `moti_anu`.`description` as `desig_motivo_anulacao`, `ges`.`nome` as `NomeGestorcliente`, `cli`.`gestorcliente` as `CodGestorcliente`
            FROM `dumpdocumentstable` `TD`
            LEFT JOIN `Lancamento` `l` ON `l`.`tipoopori`=`TD`.`tipoop` AND `l`.`numdocori`=`TD`.`num`
            LEFT JOIN `clientes` `cli` ON `TD`.`CodTerc` = `cli`.`CodTerc`
            LEFT JOIN `forn` `forn` ON `TD`.`CodTerc` = `forn`.`CodTerc`
            LEFT JOIN `zonas` `zon` ON `zon`.`CodZona` = `cli`.`CodZona`
            LEFT JOIN `empreg` `emp` ON `TD`.`CodEmpr` = `emp`.`CodEmpr`
            LEFT JOIN `represnt` `repr` ON `TD`.`CodVend` = `repr`.`CodRepres`
            LEFT JOIN `seriesdoc` `serie` ON `TD`.`nserie` = `serie`.`serie`
            LEFT JOIN `motivos_docs` `moti` ON `TD`.`id_motivo` = `moti`.`id`
            LEFT JOIN `motivos_docs` `moti_anu` ON `TD`.`id_motivo_anulacao` = `moti_anu`.`id`
            LEFT JOIN `empreg` `ges` ON `cli`.`gestorcliente` = `ges`.`CodEmpr`
            WHERE `td`.`compras` = "F" AND `td`.`tipoop` NOT IN("MA", "OS", "OSV") 
                AND `TD`.`Data` >= "'.$inicio_ano.'" AND `TD`.`Data` <= "'.$fim_ano.'" 
                AND `Td`.`anulado` = "F" 
                AND `Td`.`adiantamento` = "F" 
                AND `TD`.`CodAbreviado` IN("E", "OR")) a
            LEFT JOIN `estados` `est` ON `a`.`estado`=`est`.`codestado`
            ORDER BY `a`.`data` ASC, `a`.`tipoop` ASC, `a`.`NUM` ASC
                
            ', [1]);
            return $map_com;

            // AND `td`.`nserie` IN("0", "1", "15", "16", "17", "181", "182", "183", "184", "191", "192", "193", "194", "201", "202", "204", "2011", "2012", "2013", "2014", "2015", "8015", "9001", "9002", "9999", "211", "212", "214") AND `TD`.`CodAbreviado` IN("E", "OR")) a
        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

    public function get_relatorio_vendas_geral_documentos_2021_old_broken()
    {

        ini_set('memory_limit', '-1');

        $inicio_ano = date("2021-01-01");
        $fim_ano = date("2021-12-31");

        try {
            $map_com = DB::connection('mysql_officegest')->select('
            
            SELECT `a`.*, `est`.`lang`, `est`.`designacao` as `desig_estado`, `est`.`class_label`
            FROM (SELECT `l`.`coddiario`, `l`.`numrddiario`, `TD`.`DocNome`, `TD`.`Debita`, `TD`.`Credita`, `TD`.`typesaft`, `TD`.`DocumentNumber`, ( CASE WHEN TD.CodAbreviado = "NGT" THEN forn.Nome ELSE cli.Nome END ) as NomeCli, ( CASE WHEN TD.CodAbreviado = "NGT" THEN forn.NContrib ELSE cli.NContrib END ) as NContrib, `emp`.`Nome` as `NomeEmp`, `repr`.`Nome` as `NomeRepr`, `TD`.`CodTerc`, `TD`.`CodEmpr`, `TD`.`CodVend`, `TD`.`Num`, `TD`.`Data`, `TD`.`valordesc` as `valordesc`, `TD`.`Total` as `Total`, `TD`.`TipoOp`, `TD`.`ValorIVA` as `ValorIVA`, `TD`.`TotalSIVA` as `TotalSIVA`, `TD`.`Anulado`, `TD`.`recibo`, `cli`.`codzona`, `zon`.`designacao` as `zona_designacao`, CASE WHEN TD.Estado in("F","FAC","ESP","MAR","EXE","RAS","APR","ANU") THEN TD.Estado ELSE CASE WHEN ((TD.Estado = "FEC") OR (TD.Anulado="F" AND TD.Estado="ABR" AND TD.recibo = "T")) THEN "F" ELSE CASE WHEN ROUND(TD.Total, 2) = round(TD.valorpago,2) AND round(TD.valorpago,2) != 0 THEN "LIQ" ELSE  CASE WHEN round(TD.valorpago,2) > 0 THEN "LQP" ELSE  TD.Estado END END END END AS Estado, `serie`.`designacao` as `serie_designation`, `td`.`nserie`, `td`.`datareg`, `forn`.`Nome` as `NomeForn`, `forn`.`NContrib` as `NContribForn`, `cli`.`Nome` as `NomeCliente`, `cli`.`NContrib` as `NContribCliente`, `moti`.`description` as `desig_motivo`, `moti_anu`.`description` as `desig_motivo_anulacao`, `ges`.`nome` as `NomeGestorcliente`, `cli`.`gestorcliente` as `CodGestorcliente`
            FROM `dumpdocumentstable` `TD`
            LEFT JOIN `Lancamento` `l` ON `l`.`tipoopori`=`TD`.`tipoop` AND `l`.`numdocori`=`TD`.`num`
            LEFT JOIN `clientes` `cli` ON `TD`.`CodTerc` = `cli`.`CodTerc`
            LEFT JOIN `forn` `forn` ON `TD`.`CodTerc` = `forn`.`CodTerc`
            LEFT JOIN `zonas` `zon` ON `zon`.`CodZona` = `cli`.`CodZona`
            LEFT JOIN `empreg` `emp` ON `TD`.`CodEmpr` = `emp`.`CodEmpr`
            LEFT JOIN `represnt` `repr` ON `TD`.`CodVend` = `repr`.`CodRepres`
            LEFT JOIN `seriesdoc` `serie` ON `TD`.`nserie` = `serie`.`serie`
            LEFT JOIN `motivos_docs` `moti` ON `TD`.`id_motivo` = `moti`.`id`
            LEFT JOIN `motivos_docs` `moti_anu` ON `TD`.`id_motivo_anulacao` = `moti_anu`.`id`
            LEFT JOIN `empreg` `ges` ON `cli`.`gestorcliente` = `ges`.`CodEmpr`
            WHERE `td`.`compras` = "F" AND `td`.`tipoop` NOT IN("MA", "OS", "OSV") 
                AND `TD`.`Data` >= "'.$inicio_ano.'" AND `TD`.`Data` <= "'.$fim_ano.'" 
                AND `Td`.`anulado` = "F" 
                AND `Td`.`adiantamento` = "F" 
                AND `TD`.`CodAbreviado` IN("E", "OR")) a
            LEFT JOIN `estados` `est` ON `a`.`estado`=`est`.`codestado`
            ORDER BY `a`.`data` ASC, `a`.`tipoop` ASC, `a`.`NUM` ASC
                
            ', [1]);
            return $map_com;

            // AND `td`.`nserie` IN("0", "1", "15", "16", "17", "181", "182", "183", "184", "191", "192", "193", "194", "201", "202", "204", "2011", "2012", "2013", "2014", "2015", "8015", "9001", "9002", "9999", "211", "212", "214") AND `TD`.`CodAbreviado` IN("E", "OR")) a
        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

    public function get_relatorio_vendas_geral_documentos_2021()
    {

        ini_set('memory_limit', '-1');

        $inicio_ano = date("2021-01-01");
        $fim_ano = date("2021-12-31");

        try {
            $map_com = DB::connection('mysql_officegest')->select('
            
            SELECT `a`.*, `est`.`lang`, `est`.`designacao` as `desig_estado`, `est`.`class_label`
            FROM (SELECT `l`.`coddiario`, `l`.`numrddiario`, `TD`.`DocNome`, `TD`.`Debita`, `TD`.`Credita`, `TD`.`typesaft`, `TD`.`DocumentNumber`, ( CASE WHEN TD.CodAbreviado = "NGT" THEN forn.Nome ELSE cli.Nome END ) as NomeCli, ( CASE WHEN TD.CodAbreviado = "NGT" THEN forn.NContrib ELSE cli.NContrib END ) as NContrib, `emp`.`Nome` as `NomeEmp`, `repr`.`Nome` as `NomeRepr`, `TD`.`CodTerc`, `TD`.`CodEmpr`, `TD`.`CodVend`, `TD`.`Num`, `TD`.`Data`, `TD`.`valordesc` as `valordesc`, `TD`.`Total` as `Total`, `TD`.`TipoOp`, `TD`.`ValorIVA` as `ValorIVA`, `TD`.`TotalSIVA` as `TotalSIVA`, `TD`.`Anulado`, `TD`.`recibo`, `cli`.`codzona`, `zon`.`designacao` as `zona_designacao`, CASE WHEN TD.Estado in("F","FAC","ESP","MAR","EXE","RAS","APR","ANU") THEN TD.Estado ELSE CASE WHEN ((TD.Estado = "FEC") OR (TD.Anulado="F" AND TD.Estado="ABR" AND TD.recibo = "T")) THEN "F" ELSE CASE WHEN ROUND(TD.Total, 2) = round(TD.valorpago,2) AND round(TD.valorpago,2) != 0 THEN "LIQ" ELSE  CASE WHEN round(TD.valorpago,2) > 0 THEN "LQP" ELSE  TD.Estado END END END END AS Estado, `serie`.`designacao` as `serie_designation`, `td`.`nserie`, `td`.`datareg`, `forn`.`Nome` as `NomeForn`, `forn`.`NContrib` as `NContribForn`, `cli`.`Nome` as `NomeCliente`, `cli`.`NContrib` as `NContribCliente`, `moti`.`description` as `desig_motivo`, `moti_anu`.`description` as `desig_motivo_anulacao`, `ges`.`nome` as `NomeGestorcliente`, `cli`.`gestorcliente` as `CodGestorcliente`
            FROM `dumpdocumentstable` `TD`
            LEFT JOIN `Lancamento` `l` ON `l`.`tipoopori`=`TD`.`tipoop` AND `l`.`numdocori`=`TD`.`num`
            LEFT JOIN `clientes` `cli` ON `TD`.`CodTerc` = `cli`.`CodTerc`
            LEFT JOIN `forn` `forn` ON `TD`.`CodTerc` = `forn`.`CodTerc`
            LEFT JOIN `zonas` `zon` ON `zon`.`CodZona` = `cli`.`CodZona`
            LEFT JOIN `empreg` `emp` ON `TD`.`CodEmpr` = `emp`.`CodEmpr`
            LEFT JOIN `represnt` `repr` ON `TD`.`CodVend` = `repr`.`CodRepres`
            LEFT JOIN `seriesdoc` `serie` ON `TD`.`nserie` = `serie`.`serie`
            LEFT JOIN `motivos_docs` `moti` ON `TD`.`id_motivo` = `moti`.`id`
            LEFT JOIN `motivos_docs` `moti_anu` ON `TD`.`id_motivo_anulacao` = `moti_anu`.`id`
            LEFT JOIN `empreg` `ges` ON `cli`.`gestorcliente` = `ges`.`CodEmpr`
            WHERE `td`.`compras` = "F" AND `td`.`tipoop` NOT IN("MA", "OS", "OSV") 
            AND `TD`.`Data` >= "'. $inicio_ano .'" AND `TD`.`Data` <= "'. $fim_ano.'" 
            AND `Td`.`anulado` = "F" 
            AND `Td`.`adiantamento` = "F" 
            AND `TD`.`CodAbreviado` IN("ORI", "E", "FA", "FP", "FR", "FS", "GC", "GA", "GR", "GT", "NC", "ND", "NV", "NGT", "OR", "OP")) a
            LEFT JOIN `estados` `est` ON `a`.`estado`=`est`.`codestado`
            ORDER BY `a`.`data` ASC, `a`.`tipoop` ASC, `a`.`NUM` ASC
                
            ', [1]);
            return $map_com;

        } catch (QueryException $ex) {
            report($ex);
            die();
        }

        die();
    }

}
