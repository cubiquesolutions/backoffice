<?php

namespace App\Http\Controllers;

use Exception;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Config;

use App\Models\Providers\Product_exadi;
use App\Models\Providers\Api_providers;

use App\Http\Controllers\Api_providersController;

use App\Http\Resources\Product_exadiResource;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;

class Product_exadiController extends Controller
{

    protected $size_of_each_group = 45; // máximo de 50

    public function getAllProducts_exadi()
    {
        return Datatables::of(Product_exadi::query())
            ->setRowClass('{{ $id % 2 == 0 ? "text-white" : "bg-dark text-white"}}')
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->toJson();
    }

    public function getAllreferences()
    {
        //atualiza primeiro os mais velhos
        return Product_exadi::select('ref_supplier')->orderBy('updated_at', 'ASC')->get(['ref_supplier']);
    }

    public function getAll_codes_for_body()
    {
        //primeiro os mais velhos
        return Product_exadi::select('code_supplier as id_artic', 'code_supplier as tipo_id')->orderBy('updated_at', 'ASC')->get();
    }

    public function getAllcode_supplier()
    {
        //primeiro os mais velhos
        return Product_exadi::select('code_supplier')->orderBy('updated_at', 'ASC')->get();
    }


    public function get_N_products_data_from_exadi($body, $ticket)
    {

        // array para JSON !!!!

        $body = json_encode($body);

        // [
        //     {
        //         "id_artic":"20667",
        //         "tipo_id":"20667"
        //     },
        // ]

        $url        = Config::get('variables.exadi.url');
        $url        = $url . "/stock_prec";

        $cliente    = Config::get('variables.exadi.cliente');
        $api_key    = Config::get('variables.exadi.api_key');


        $client = new Client([
            'verify' => false
        ]);

        $request = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',

            ],
            'query' =>
            [
                'codcli'    => $cliente,
                'ticket'    => $ticket,
                'api_key'   => $api_key
            ],
            'body'  => $body,
        ]);

        $res = $request->getBody()->getContents();
        return json_decode($res)->items;

        // return json_decode($res)->items[1]->id_artic;

        // "id_artic":"21203",
        // "tipo_id":"2",
        // "id_articulo":"21203",
        // "precio":41.58,
        // "lpi":0,
        // "raee":0,
        // "porc_dto":0,
        // "porc_dtow":0.5,
        // "precio_neto":41.58,
        // "precio_netow":41.37,
        // "precio_neto_lpi":41.58,
        // "precio_netow_lpi":41.37,
        // "precio_neto_lpi_raee":41.58,
        // "precio_netow_lpi_raee":41.37,
        // "stock":89,
        // "fec_recep":"",
        // "reg_iva_vta":"G"



    }

    public function check_my_login_token()
    {
        $Api_providersController = new Api_providersController;

        // verifica se o token guardado na Base de Dados expirou. Caso expirou então gera um novo e guarda na BD.
        $login_token_status = $Api_providersController->check_token_expiration_date('Exadi');

        if ($login_token_status == "expired") {
            return $this->login();
        } else {
            return $Api_providersController->getToken('Exadi');
        }
    }

    public function update_my_login_token($provider_name, $token)
    {
        $Api_providersController = new Api_providersController;

        return $Api_providersController->update_provider_token($provider_name, $token);
    }

    public function update_price_and_stock()
    {
        ini_set('memory_limit', '-1');

        // verifica o prazo de validade do token do login guardado desde a ultima vez que foi atualizado
        $ticket = $this->check_my_login_token();
        $ticket = $ticket["login_token"];

        $my_code_suppliers          = $this->getAllcode_supplier()->toArray();
        $size_of_my_table           = $this->getAll_codes_for_body()->count();                          // devolve o total de entradas na bd
        $num_of_groups              = ceil($size_of_my_table / $this->size_of_each_group);              // como a exadi recebe apenas N produtos por cada pedido, chamamos $num_of_groups vezes. ceil() arredonda por excesso

        $body                       = $this->getAll_codes_for_body()->split($num_of_groups)->toArray(); // então dividimos o body por $num_of_groups partes.

        for ($k = 0; $k < $num_of_groups; $k++) {

            sleep(1); // não abusar do servidor ....

            $api_N_products            = $this->get_N_products_data_from_exadi($body[$k], $ticket);    // enviar pedido a api por cada grupo de N

            foreach ($api_N_products as $api_row) {
                for ($i = 0; $i < count($my_code_suppliers); $i++) {

                    if ($api_row->id_artic == $my_code_suppliers[$i]["code_supplier"]) {

                        $code_supplier      = $api_row->id_artic;
                        $stock_new          = (float) $api_row->stock;
                        $price_new          = (float) $api_row->precio;
                        $updated_at         = Carbon::now();

                        $updated_at = date_format($updated_at, "Y-m-d H:i:s");

                        $this->update_product($code_supplier, $stock_new, $price_new, $updated_at);

                        break;  // sai deste ciclo porque já atualizou o produto e não precisa de procurar mais
                    }
                }
            }

            // echo "Atualizou " . $this->size_of_each_group . "!";
        }
    }

    public function update_product($code_supplier, $stock_new, $price_new, $updated_at)
    {

        $product = Product_exadi::where('code_supplier', '=', $code_supplier)->get()->first();

        $original = $product->getOriginal();

        $product->update(['stock_old'   => (float) $original['stock_new']]);
        $product->update(['stock_new'   => $stock_new]);

        $product->update(['price_old'   => (float) $original['price_new']]);
        $product->update(['price_new'   => $price_new]);

        $product->update(["updated_at"  => $updated_at]);

        $product->save();
    }

    public function login()
    {
        $url        = Config::get('variables.exadi.url');
        $url        = $url . "/ticket";

        $usuario    = Config::get('variables.exadi.usuario');
        $password   = Config::get('variables.exadi.password');
        $cliente    = Config::get('variables.exadi.cliente');
        $api_key    = Config::get('variables.exadi.api_key');

        $client = new Client([
            'verify' => false
        ]);

        $request = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',

            ],
            'query' =>
            [
                'api_key'   => $api_key,
                'user'      => $usuario,
                'codcli'    => $cliente,
                'password'  => $password,
            ],
        ]);

        // name_clte
        // ticket
        // time_ini
        // time_fin
        // errores []

        $res = json_decode($request->getBody()->getContents());

        if ($res->ticket != '' && $res->ticket != null) {
            $this->update_my_login_token('Exadi', $res->ticket);        // quando chama o Login, guarda o token na BD
            return $res->ticket;
        } else {
            dd($res); // nao deu ticket bugou!
        }
    }

}
