<?php

namespace App\Http\Controllers;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Config;

use App\Models\Providers\Product_hermida;
use App\Http\Resources\Product_hermidaResource;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class Product_hermidaController extends Controller
{
	
	public function index()
	{
		return view('/providers_views/providers_home');
	}

	public function getAllProducts_hermida()
	{
		return Datatables::of(Product_hermida::query())
			->setRowClass('{{ $id % 2 == 0 ? "text-white" : "bg-dark text-white"}}')
			->setRowId('id')
			->setRowAttr([
				'align' => 'center',
			])

			->toJson();
	}

	public function getAllreferences()
	{
		//return Product_hermida::distinct()->get(['ref_supplier'])->toArray();
		//atualiza primeiro os mais velhos
		return Product_hermida::select('ref_supplier')->orderBy('updated_at', 'ASC')->get(['ref_supplier']);
	}

	public function getPrecioStock($ref_supplier, $login_token)
	{
		$empresa 	= Config::get('variables.hermida.empresa');
		$url 	= Config::get('variables.hermida.url');

		$client = new Client();

		$response = $client->request('POST', $url, [
			'headers' => [
				'Content-Type' => 'text/xml; charset=UTF8',
			],

			'body' => '<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<soap:Body>
			<getPrecioStock xmlns="http://tempuri.org/">
			<Token>' . $login_token . '</Token>
			<Cliente>' . $empresa . '</Cliente>
			<Producto>' . $ref_supplier . '</Producto>
			</getPrecioStock>
			</soap:Body>
			</soap:Envelope>',

		]);


		//guarda a resposta do servidor
		$data = $response->getBody()->getContents();

		//remover depois
		$reference = strstr($data, '</Producto>', true);
		//removes antes
		$reference = substr($reference, strpos($reference, "<Producto>") + 10);

		//remover depois
		$price = strstr($data, '</Precio>', true);
		//removes antes
		$price = substr($price, strpos($price, "<Precio>") + 8);

		//remover depois
		$stock = strstr($data, '</Stock>', true);
		//removes antes
		$stock = substr($stock, strpos($stock, "<Stock>") + 7);


		$obj = array(
			'item' => array(
				'reference' => $reference,
				'price' => $price,
				'stock' => $stock,
			)
		);

		return $obj;
	}

	public function updateProductStockAndPrice($ref_supplier, $stock, $price, $updated_at)
	{

		$product = Product_hermida::where('ref_supplier', '=', $ref_supplier)
			->get()->first();

		$original = $product->getOriginal();

		$product->update(['stock_old' => $original['stock_new']]);
		$product->update(['stock_new' => $stock]);

		$product->update(['price_old' => $original['price_new']]);
		$product->update(['price_new' => $price]);

		$product->update(["updated_at"  => $updated_at]);

		$product->save();
	}

	public function bulkUpdate()
	{
		ini_set('memory_limit', '-1');

		$login_token = $this->loginHermida();

		$referencesArray = $this->getAllreferences();

		foreach ($referencesArray as $row) {
			sleep(1); // não abusar do servidor ....

			$obj = $this->getPrecioStock($row['ref_supplier'], $login_token);

			$ref_supplier = $obj['item']['reference'];
			$price = $obj['item']['price'];
			$stock = $obj['item']['stock'];

			$updated_at         = Carbon::now();

			$updated_at = date_format($updated_at, "Y-m-d H:i:s");

			$this->updateProductStockAndPrice($ref_supplier, $stock, $price, $updated_at);
		}
	}




	public function loginHermida()
	{
		$empresa 	= Config::get('variables.hermida.empresa');
		$usuario 	= Config::get('variables.hermida.usuario');
		$password 	= Config::get('variables.hermida.password');
		$url 		= Config::get('variables.hermida.url');

		$login_xml = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<Login xmlns="http://tempuri.org/">
		<Empresa>'.$empresa.'</Empresa>
		<Usuario>'.$usuario.'</Usuario>
		<Password>'.$password.'</Password>
		</Login>
		</soap:Body>
		</soap:Envelope>';

		$client = new Client();

		$response = $client->request('POST', $url, [
			'headers' => [
				'Content-Type' => 'text/xml; charset=UTF8',
			],
			'body' => $login_xml,
		]);


		//guarda a resposta do servidor
		$data = $response->getBody()->getContents();

		//remover depois
		$login_token = strstr($data, '</LoginResult>', true);

		//removes antes
		$login_token = substr($login_token, strpos($login_token, "<LoginResult>") + 13);


		return $login_token;
	}
}