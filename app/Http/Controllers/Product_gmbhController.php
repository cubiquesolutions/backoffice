<?php

namespace App\Http\Controllers;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;

use App\Models\Providers\Product_gmbh;
use App\Http\Resources\Product_gmbhResource;
use Illuminate\Support\Facades\Config;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Yajra\Datatables\Datatables;
use Carbon\Carbon;


class Product_gmbhController extends Controller
{

    public function index()
    {
        return view('/providers_views/providers_home');
    }

    public function getAllProducts_gmbh()
    {
        return Datatables::of(Product_gmbh::query())
            ->setRowClass('{{ $id % 2 == 0 ? "text-white" : "bg-dark text-white"}}')
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->toJson();
    }

    public function getAllreferences()
    {
        //atualiza primeiro os mais velhos
        return Product_gmbh::select('ref_supplier')->orderBy('updated_at', 'ASC')->get(['ref_supplier']);
    }

    public function downloadCSV_from_FTP()
    {
        // connect and login to FTP server
        $ftp_username = Config::get('variables.gmbh.ftp_username');
        $ftp_userpass = Config::get('variables.gmbh.ftp_userpass');
        $ftp_server =   Config::get('variables.gmbh.ftp_server');

        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);

        // turn passive mode on
        ftp_pasv($ftp_conn, true);

        $local_file = "gmbh_price_stock_list.csv";
        $server_file = "preislisteCOP.csv";

        // download server file
        ftp_get($ftp_conn, $local_file, $server_file, FTP_ASCII);

        // close connection
        ftp_close($ftp_conn);
    }

    public function csv_to_array()
    {
        $local_file = "gmbh_price_stock_list.csv";

        $rows = array_map(function ($v) {
            return str_getcsv($v, ";");
        }, file($local_file));

        $header = array_shift($rows);
        $csv_to_associative_array    = [];

        foreach ($rows as $row) {
            $csv_to_associative_array[] = array_combine($header, $this->convert_from_latin1_to_utf8_recursively($row));
        }

        //dd($csv_to_associative_array[0]["article number"]);

        return $csv_to_associative_array;
    }

    public function bulkUpdate()
    {
        ini_set('memory_limit', '-1');
        
        $found = false;

        $this->downloadCSV_from_FTP();
        $csv_to_associative_array = $this->csv_to_array();

        $referencesArray = $this->getAllreferences()->toArray();

        //dd($referencesArray[0]["ref_supplier"]);

        foreach ($csv_to_associative_array as $csv_row) {

            foreach ($referencesArray as $db_row) {

                // Se o artigo existir na BD local, atualiza
                if ($csv_row["article number"] == $db_row["ref_supplier"]) {

                    $found = true;

                    $ref_supplier       = $csv_row["article number"];
                    $ean                = $csv_row["EANNummer"];
                    $description        = $csv_row["description I"];
                    $stock_new          = (float) $csv_row["stock"];
                    $price_new          = (float) $csv_row["price EUR"];
                    $updated_at         = Carbon::now();

                    $updated_at = date_format($updated_at, "Y-m-d H:i:s");

                    $this->update_product($ref_supplier, $ean, $description, $stock_new, $price_new, $updated_at);
                }
            }

            if (!$found) {
                // Se o artigo não exisitir na BD local, cria novo
                    $newProduct = array(
                        'ref_supplier'  => $csv_row["article number"],
                        'ref_cubique'   => '',
                        'ean'           => $csv_row["EANNummer"],
                        'description'   => $csv_row["description I"],
                        'stock_old'     => 0,
                        'stock_new'     => (float) $csv_row["stock"],
                        'price_old'     => 0,
                        'price_new'     => (float) $csv_row["price EUR"],
                    );
                    Product_gmbh::create($newProduct);
            }

            // caso tenha encontrado coloca a falso para a próxima comparação
            $found = false;
        }
    }

    public function update_product($ref_supplier, $ean, $description, $stock_new, $price_new, $updated_at)
    {

        $product = Product_gmbh::where('ref_supplier', '=', $ref_supplier)->get()->first();

        $original = $product->getOriginal();

        $product->update(['ean'         => $ean]);
        $product->update(['description' => $description]);

        $product->update(['stock_old'   => (float) $original['stock_new']]);
        $product->update(['stock_new'   => $stock_new]);

        $product->update(['price_old'   => (float) $original['price_new']]);
        $product->update(['price_new'   => $price_new]);

        $product->update(["updated_at"  => $updated_at]);

        $product->save();
    }


    /**
     * Encode array from latin1 to utf8 recursively
     * @param $dat
     * @return array|string
     */
    public static function convert_from_latin1_to_utf8_recursively($dat)
    {
        if (is_string($dat)) {
            return utf8_encode($dat);
        } elseif (is_array($dat)) {
            $ret = [];
            foreach ($dat as $i => $d) $ret[$i] = self::convert_from_latin1_to_utf8_recursively($d);

            return $ret;
        } elseif (is_object($dat)) {
            foreach ($dat as $i => $d) $dat->$i = self::convert_from_latin1_to_utf8_recursively($d);

            return $dat;
        } else {
            return $dat;
        }
    }
}
