<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Segmented_customerController;
use Illuminate\Support\Facades\Auth;
use Mail;
use Carbon\Carbon;

use Illuminate\Http\Request;

class MailController extends Controller
{
    public function apresentacao_email($id)
    {
        /* Envia um email de apresentação com um anexo */

        $current_hours_Time = Carbon::now()->format('H');

        $Segmented_customerController = new Segmented_customerController;
        $segmented_customer = $Segmented_customerController->getCustomerInfo($id);

        $cliente = json_decode($segmented_customer, true);

        $to_name        = $segmented_customer->Entity_name;
        $to_email       = $segmented_customer->Entity_email;

        if (strpos(Auth::user()->email, 'estagio') !== false) {
            // Caso seja estagiário,
            // o User não tem nome Verdadeiro logo o nome no cabeçalho vai como Comercial
            // como também não tem email publico, no caso estagio1@cubique.pt ... vai como webmaster@cubique.pt
            $from_name      = 'Comercial - Cubique Solutions';

            $from_email     = 'webmaster@cubique.pt';

            $signature_path     = public_path() . "/email_signatures/assinatura_dep_comercial.png";
            $attachment_path    = public_path() . "/email_signatures/" . 'CUBIQUE- Apresentação Geral.pdf';
        } else {
            // Caso seja comercial Cubique então á partida tem tudo na Base de Dados OK ...
            // Troca @printreutil.pt pelo equivalente @cubique.pt
            // Acrescenta a assinatura associada pelo id na pasta /email_signatures/
            $from_name      = Auth::user()->name . ' - Cubique Solutions';
            $from_id        = Auth::user()->id;

            $from_email     = Auth::user()->email;
            $from_email     = str_replace("@printreutil.com", "@cubique.pt", $from_email);

            $signature_path     = public_path() . "/email_signatures/" . $from_id . ".png";
            $attachment_path    = public_path() . "/email_signatures/" . 'CUBIQUE- Apresentação Geral.pdf';
        }


        $pdf_icon     = public_path() . "/email_signatures/pdf_icon.png";

        $data = array(

            'cliente'           => $cliente,
            'to_name'           => $to_name,
            'to_email'          => $to_email,
            'from_name'         => $from_name,
            'from_email'        => $from_email,
            'signature_path'    => $signature_path,

            'pdf_icon'          => $pdf_icon,

            'current_hours_Time' => $current_hours_Time,

        );

        Mail::send('emails.apresentacao', $data, function ($message) use ($to_name, $to_email, $from_name, $from_email, $attachment_path) {
            $message->to($to_email, $to_name)->subject('Apresentação Cubique Solutions');

            $message->from('webmaster@cubique.pt', $from_name);

            $message->replyTo($from_email, $from_name);

            $message->bcc($from_email); // envia uma cópia para o próprio

            $message->attach($attachment_path);
        });
    }
}
