<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;

use Mail;
use App\Http\Resources\Segmented_customerResource;
use App\Models\Segmented_customer;
use App\User;

use Illuminate\Support\Facades\Auth;

use App\Models\Attributes_1;
use App\Models\Attributes_2;
use App\Models\Attributes_3;
use App\Models\Attributes_4;
use App\Models\Attributes_5;
use App\Models\Attributes_6;
use App\Models\Attributes_7;
use App\Models\Attributes_8;

use App\Models\Countries;

use Yajra\Datatables\Datatables;
use DB;

class Segmented_customerController extends Controller
{
    /* Variáveis para uso nos filtros */
    protected $array_of_selected_checkboxes;

    protected $array_of_selected_checkboxes_sizeCountries;
    protected $array_of_selected_checkboxes_sizeDistricts;
    protected $array_of_selected_checkboxes_sizeCities;
    protected $array_of_selected_checkboxes_sizeCubiqueEmployees;

    protected $array_of_selected_checkboxes_sizeAttributes1;
    protected $array_of_selected_checkboxes_sizeAttributes2;
    protected $array_of_selected_checkboxes_sizeAttributes3;
    protected $array_of_selected_checkboxes_sizeAttributes4;
    protected $array_of_selected_checkboxes_sizeAttributes5;
    protected $array_of_selected_checkboxes_sizeAttributes6;
    protected $array_of_selected_checkboxes_sizeAttributes7;
    protected $array_of_selected_checkboxes_sizeAttributes8;

    protected $array_of_selected_checkboxes_sizeContains_these_words;
    /* ****************************** */


    public function index()
    {
        $attributesList['atr1'] = $this->getAllAttributes1(); // para popular o modal de exportar para csv
        $attributesList['atr2'] = $this->getAllAttributes2(); // para popular o modal de exportar para csv
        $attributesList['atr3'] = $this->getAllAttributes3(); // para popular o modal de exportar para csv
        $attributesList['atr4'] = $this->getAllAttributes4(); // para popular o modal de exportar para csv
        $attributesList['atr5'] = $this->getAllAttributes5(); // para popular o modal de exportar para csv
        $attributesList['atr6'] = $this->getAllAttributes6(); // para popular o modal de exportar para csv
        $attributesList['atr7'] = $this->getAllAttributes7(); // para popular o modal de exportar para csv
        $attributesList['atr8'] = $this->getAllAttributes8(); // para popular o modal de exportar para csv

        $filterCustomerData = $this->getFilterDataDistinct(); // contem DISTINTOS: país, distrito, concelho, localidade, atributos

        $cubiqueUsersList = $this->getAllCubiqueUsers();

        $countriesList = $this->getAllCountries();

        $logged_user_name = Auth::user()->name;

        return view(
            '/customers_views/segmented_customers',
            compact('attributesList', 'filterCustomerData', 'cubiqueUsersList', 'countriesList', 'logged_user_name')
        );
    }

    public function getCustomerInfo($id)
    {
        $customerInfo = Segmented_customer::findOrFail($id);
        return $customerInfo;
    }

    public function update(Request $request, $id)
    {
        $cst = Segmented_customer::findOrFail($id);

        $data = $request->all();

        $cst->update($request->only([
            'Cubique_Employee',
            'Date_first_approach',
            'Date_postponed_approach',
            'Date_meeting',
            'Entity_name',
            'Entity_email',
            'Entity_phone_number',
            'Customer_name',
            'Customer_email',
            'Customer_phone_number',
            'Country',
            'District',
            'City',
            'Place',
            'Postal_code',
            'Contact_is_in_process',
            'Comments',
            'attributes_1',
            'attributes_2',
            'attributes_3',
            'attributes_4',
            'attributes_5',
            'attributes_6',
            'attributes_7',
            'attributes_8',
        ]));

        //return new Segmented_customerResource($cst);

        return $this->index();
    }

    public function deleteCustomerSegmented($id)
    {
        $cst = Segmented_customer::findOrFail($id);
        $cst->delete();
        return $this->index();
    }

    public function getAllCustomers_Segmented()
    {
        ini_set('memory_limit', '-1');

        return Datatables::of(Segmented_customer::query())
            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Segmented_customer $cst) {
                $pathTo = '/segmentedCustomers/';
                $myTable = 'customers_segmented';
                $entity_name = "'" . $cst->Entity_name . "'";

                return '
                <button class="btn btn-sm btn-success"  onclick="editCustomer(' . $cst->id . ', ' . $pathTo . ', ' . $myTable . ')"><i class="fa fa-edit"></i></button>
                <button class="btn btn-sm btn-danger"   onclick="deleteCustomer(' . $cst->id . ', ' . $pathTo . ')"><i class="fa fa-trash"></i></button>
                <button class="btn btn-sm btn-info"     onclick="menu_email_templates(' . $cst->id . ', ' . $entity_name . ')"><i class="fas fa-paper-plane"></i></button>';

            })
            ->rawColumns(['actions'])

            ->toJson();
    }

    public function getFilteredQueryResults(Request $request)
    {
        $arr = json_decode($request->dataFromClient, true);

        $selection = $this->queryBuilderFromCheckboxSelection($arr);
        $jsn = $selection->toJson();

        return $this->generateCSV($jsn);
    }

    public function queryBuilderFromCheckboxSelection($arr)
    {
        // ->orWhere() para OR
        // ->where() para AND

        ini_set('memory_limit', '-1');

        $this->array_of_selected_checkboxes = $arr;

        $this->array_of_selected_checkboxes_sizeCountries = count($this->array_of_selected_checkboxes["Countries"]);
        $this->array_of_selected_checkboxes_sizeDistricts = count($this->array_of_selected_checkboxes["Districts"]);
        $this->array_of_selected_checkboxes_sizeCities = count($this->array_of_selected_checkboxes["Cities"]);
        $this->array_of_selected_checkboxes_sizeCubiqueEmployees = count($this->array_of_selected_checkboxes["Cubique_Employees"]);

        $this->array_of_selected_checkboxes_sizeAttributes1 = count($this->array_of_selected_checkboxes["Attributes_1"]);
        $this->array_of_selected_checkboxes_sizeAttributes2 = count($this->array_of_selected_checkboxes["Attributes_2"]);
        $this->array_of_selected_checkboxes_sizeAttributes3 = count($this->array_of_selected_checkboxes["Attributes_3"]);
        $this->array_of_selected_checkboxes_sizeAttributes4 = count($this->array_of_selected_checkboxes["Attributes_4"]);
        $this->array_of_selected_checkboxes_sizeAttributes5 = count($this->array_of_selected_checkboxes["Attributes_5"]);
        $this->array_of_selected_checkboxes_sizeAttributes6 = count($this->array_of_selected_checkboxes["Attributes_6"]);
        $this->array_of_selected_checkboxes_sizeAttributes7 = count($this->array_of_selected_checkboxes["Attributes_7"]);
        $this->array_of_selected_checkboxes_sizeAttributes8 = count($this->array_of_selected_checkboxes["Attributes_8"]);

        $this->array_of_selected_checkboxes_sizeContains_these_words = count($this->array_of_selected_checkboxes["Contains_these_words"]);

        $selection = DB::table('segmented_customer')

            ->Where(function ($query) {
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeCountries; $i++) {
                    $query->orWhere('Country', $this->array_of_selected_checkboxes["Countries"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeDistricts; $i++) {
                    $query->orWhere('District', $this->array_of_selected_checkboxes["Districts"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeCities; $i++) {
                    $query->orWhere('City', $this->array_of_selected_checkboxes["Cities"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeCubiqueEmployees; $i++) {
                    $query->orWhere('Cubique_Employee', $this->array_of_selected_checkboxes["Cubique_Employees"][$i]);
                }

                // Atributos
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeAttributes1; $i++) {
                    $query->orWhere('attributes_1', $this->array_of_selected_checkboxes["Attributes_1"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeAttributes2; $i++) {
                    $query->orWhere('attributes_2', $this->array_of_selected_checkboxes["Attributes_2"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeAttributes3; $i++) {
                    $query->orWhere('attributes_3', $this->array_of_selected_checkboxes["Attributes_3"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeAttributes4; $i++) {
                    $query->orWhere('attributes_4', $this->array_of_selected_checkboxes["Attributes_4"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeAttributes5; $i++) {
                    $query->orWhere('attributes_5', $this->array_of_selected_checkboxes["Attributes_5"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeAttributes6; $i++) {
                    $query->orWhere('attributes_6', $this->array_of_selected_checkboxes["Attributes_6"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeAttributes7; $i++) {
                    $query->orWhere('attributes_7', $this->array_of_selected_checkboxes["Attributes_7"][$i]);
                }
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeAttributes8; $i++) {
                    $query->orWhere('attributes_8', $this->array_of_selected_checkboxes["Attributes_8"][$i]);
                }

                // Contem estas palavras
                for ($i = 0; $i < $this->array_of_selected_checkboxes_sizeContains_these_words; $i++) {
                    if (strcmp('"Email Geral"', $this->array_of_selected_checkboxes["Contains_these_words"][$i])) {
                        $query->orWhere('Entity_email', 'LIKE', '%geral%');
                        $query->orWhere('Customer_email', 'LIKE', '%geral%');
                    }
                }
            })

            ->get();

        return $selection;
    }

    public function generateCSV($jsn)
    {
        $filePath = public_path() . "\\file.csv";

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        //Decode the JSON and convert it into an associative array.
        $jsonDecoded = json_decode($jsn, true);

        //Give our CSV file a name.
        $csvFileName = 'file.csv';

        //Open file pointer.
        $fp = fopen($csvFileName, 'w');

        //Headers 1st row
        fputcsv($fp, array_keys($jsonDecoded[0]));

        //Loop through the associative array.
        foreach ($jsonDecoded as $row) {
            //Write the row to the CSV file.
            fputcsv($fp, $row);
        }

        //Finally, close the file pointer.
        fclose($fp);

        //return Response::download($filePath, $csvFileName, $headers);
        echo 'Ficheiro CSV gerado!';
    }

    public function getDownload()
    {
        $filePath = public_path() . "/file.csv";
        $csvFileName = 'file.csv';

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        return Response::download($filePath, $csvFileName, $headers);
    }

    public function getFilterDataDistinct()
    {
        $filterData['Cubique_Employee'] = Segmented_customer::distinct()->whereNotNull('Cubique_Employee')->orderBy('Cubique_Employee', 'ASC')->get(['Cubique_Employee']);
        $filterData['District'] = Segmented_customer::distinct()->whereNotNull('District')->orderBy('District', 'ASC')->get(['District']);
        $filterData['Country'] = Segmented_customer::distinct()->whereNotNull('Country')->orderBy('Country', 'ASC')->get(['Country']);
        $filterData['City'] = Segmented_customer::distinct()->whereNotNull('City')->orderBy('City', 'ASC')->get(['City']);

        $filterData['Attributes_1'] = Segmented_customer::distinct()->whereNotNull('attributes_1')->orderBy('attributes_1', 'ASC')->get(['attributes_1']);
        $filterData['Attributes_2'] = Segmented_customer::distinct()->whereNotNull('attributes_2')->orderBy('attributes_2', 'ASC')->get(['attributes_2']);
        $filterData['Attributes_3'] = Segmented_customer::distinct()->whereNotNull('attributes_3')->orderBy('attributes_3', 'ASC')->get(['attributes_3']);
        $filterData['Attributes_4'] = Segmented_customer::distinct()->whereNotNull('attributes_4')->orderBy('attributes_4', 'ASC')->get(['attributes_4']);
        $filterData['Attributes_5'] = Segmented_customer::distinct()->whereNotNull('attributes_5')->orderBy('attributes_5', 'ASC')->get(['attributes_5']);
        $filterData['Attributes_6'] = Segmented_customer::distinct()->whereNotNull('attributes_6')->orderBy('attributes_6', 'ASC')->get(['attributes_6']);
        $filterData['Attributes_7'] = Segmented_customer::distinct()->whereNotNull('attributes_7')->orderBy('attributes_7', 'ASC')->get(['attributes_7']);
        $filterData['Attributes_8'] = Segmented_customer::distinct()->whereNotNull('attributes_8')->orderBy('attributes_8', 'ASC')->get(['attributes_8']);

        return $filterData;
    }

    public function getAllCubiqueUsers()
    {
        $allCubiqueUsers = User::distinct()->get(['name']);

        return $allCubiqueUsers;
    }

    public function getAllCountries()
    {
        $allCountries = Countries::distinct()->get(['name']);
        return $allCountries;
    }

    public function getAllAttributes1()
    {
        return Attributes_1::all();
    }

    public function getAllAttributes2()
    {
        return Attributes_2::all();
    }

    public function getAllAttributes3()
    {
        return Attributes_3::all();
    }

    public function getAllAttributes4()
    {
        return Attributes_4::all();
    }

    public function getAllAttributes5()
    {
        return Attributes_5::all();
    }

    public function getAllAttributes6()
    {
        return Attributes_6::all();
    }

    public function getAllAttributes7()
    {
        return Attributes_7::all();
    }

    public function getAllAttributes8()
    {
        return Attributes_8::all();
    }

    public function test()
    {
        // $to_name = 'Fabio Pedro';

        // $user = User::where('name', '=', $to_name)->firstOrFail()->toArray();

        // dd($user["email"]);

    }

    public function send_email(Request $request)
    {
        // recebe todos os dados da linha do cliente e guarda no $arr
        $arr = json_decode($request->dataFromClient, true);

        $to_name    = $request->dataFromClient_comercial;
        $user       = User::where('name', '=', $to_name)->firstOrFail()->toArray();
        $to_email   = $user["email"];
        $from_name  = 'Backoffice | Cubique';

        $sender_email = Auth::user()->email; // email do utilizador logado

        $data = array(
            'from_name'     => $from_name,
            'sender_email'  => $sender_email,

            'to_name'       => $to_name,
            'to_email'      => $to_email,

            'arr'           =>   $arr,            
        );

        Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email, $sender_email) {
            $message->to($to_email, $to_name)->subject('Novo Agendamento com Cliente');
            $message->from('webmaster@cubique.pt', 'Backoffice | Cubique');
            $message->cc($sender_email);
            $message->bcc('no-reply@mkt.cubique.pt');
        });
    }























    public function create(Request $request)
    {

        $segmented_customer = new Segmented_customer();

        $segmented_customer->Cubique_Employee = $request->Cubique_Employee;
        $segmented_customer->Date_first_approach = $request->Date_first_approach;
        $segmented_customer->Date_postponed_approach = $request->Date_postponed_approach;

        $segmented_customer->Date_meeting = $request->Date_meeting;

        $segmented_customer->Entity_name = $request->Entity_name;
        $segmented_customer->Entity_email = $request->Entity_email;
        $segmented_customer->Entity_phone_number = $request->Entity_phone_number;
        $segmented_customer->Customer_name = $request->Customer_name;
        $segmented_customer->Customer_email = $request->Customer_email;
        $segmented_customer->Customer_phone_number = $request->Customer_phone_number;
        $segmented_customer->Country = $request->Country;
        $segmented_customer->District = $request->District;
        $segmented_customer->City = $request->City;
        $segmented_customer->Place = $request->Place;
        $segmented_customer->Postal_code = $request->Postal_code;
        $segmented_customer->Comments = $request->Comments;

        $segmented_customer->attributes_1 = $request->attributes_1;
        $segmented_customer->attributes_2 = $request->attributes_2;
        $segmented_customer->attributes_3 = $request->attributes_3;
        $segmented_customer->attributes_4 = $request->attributes_4;
        $segmented_customer->attributes_5 = $request->attributes_5;
        $segmented_customer->attributes_6 = $request->attributes_6;
        $segmented_customer->attributes_7 = $request->attributes_7;
        $segmented_customer->attributes_8 = $request->attributes_8;

        $segmented_customer->created_at = Carbon::now();
        $segmented_customer->updated_at = Carbon::now();

        $segmented_customer->save();

        // $attributes_1 = Attributes_1::find([3, 4]);
        // $segmented_customer->attributes_1()->attach($attributes_1);

        return new Segmented_customerResource($segmented_customer);
    }
}
