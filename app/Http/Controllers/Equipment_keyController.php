<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;

use Illuminate\Support\Facades\Auth;

use App\Models\Equipment_key;
use DB;
use Illuminate\Database\QueryException;

use Yajra\Datatables\Datatables;

class Equipment_keyController extends Controller
{
    public function index()
    {
        return view('/officeGest_views/app_equipment_key');
    }

    public function getAllEquipment_Keys()
    {

        return Datatables::of(Equipment_key::query())
            ->setRowAttr([
                'align' => 'center',
            ])

            // ->addColumn('actions', function (Equipment_key $key) {
            //     return '
            //     <button class="btn btn-sm btn-danger"   onclick="deleteEquipKey(' . $key->id . ')"><i class="fa fa-trash"></i></button>
            //     ';
            // })
            // ->rawColumns(['actions'])

            ->toJson();
    }

    // public function deleteEquipmentKey($id)
    // {
    //     $key = Equipment_key::findOrFail($id);
    //     $key->delete();
    //     return $this->index();
    // }

    public function deleteEquipmentKey_by_key($key)
    { }

    public function create(Request $request)
    {
        $number_of_keys = $request->dataFromClient;

        $counter = 0;

        do {
            $key = new Equipment_key();

            $key->unique_key = $this->create_unique_key(8);

            $key->created_by = Auth::user()->name;

            $key->created_at = Carbon::now();
            $key->updated_at = Carbon::now();

            $key->save();

            $counter++;
        } while ($counter < 100);
    }

    public function create_unique_key($length)
    {

        $randomString = $this->generateRandomKey($length);

        $key = DB::table('equipment_key')->where('unique_key', $randomString)->first();

        if ($key) {
            // se existe chave na BD então cria novamente
            $this->create_unique_key($length);
        } else {
            // se é uma nova chave então devolve a nova chave
            return $randomString;
        }
    }

    public function generateRandomKey($length)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {

            $randomString .= $characters[rand(0, $charactersLength - 1)];

            if ($i == 0 && $randomString == "0")
                do {
                    $randomString = '';
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                } while ($randomString == "0");
        }
        return $randomString;
    }

    public function clean_unused_local_keys()
    {
        $local_keys         = $this->get_local_equipment_keys();
        $officegest_keys    = $this->get_officegest_equipment_keys();

        $existe = 0;

        foreach ($local_keys as $local_key) {
            $existe = 0;

            foreach ($officegest_keys as $officegest_key) {
                // se a $officegest_key conter a $local_key
                if (strpos($officegest_key, $local_key) !== false) {
                    $existe = 1;

                    $id     = DB::table('equipment_key')->where('unique_key', $local_key)->value('id');
                    Equipment_key::where('id', $id)->update(array('isUsed' => 'Sim'));

                    break;
                }
            }

            if ($existe == 0) {
                $id     = DB::table('equipment_key')->where('unique_key', $local_key)->value('id');

                $key_to_remove    = Equipment_key::findOrFail($id);
                $key_to_remove->delete();
            }
        }
    }

    public function set_isUsed_sim()
    {
        $local_keys         = $this->get_local_equipment_keys();
        $officegest_keys    = $this->get_officegest_equipment_keys();

        foreach ($local_keys as $local_key) {

            foreach ($officegest_keys as $officegest_key) {
                // se a $officegest_key conter a $local_key
                if (strpos($officegest_key, $local_key) !== false) {

                    $id     = DB::table('equipment_key')->where('unique_key', $local_key)->value('id');
                    Equipment_key::where('id', $id)->update(array('isUsed' => 'Sim'));

                    break;
                }
            }
        }
    }

    public function get_officegest_equipment_keys()
    {
        $all_officegest_keys = DB::connection('mysql_officegest')->select('select designacao from a_cub_eqequipamentos;', [1]);

        $dataset = [];

        foreach ($all_officegest_keys as $key) {
            array_push($dataset, $key->designacao);
        }

        return $dataset;
    }

    public function get_local_equipment_keys()
    {
        $all_local_keys = Equipment_key::all();

        $dataset = [];

        foreach ($all_local_keys as $key) {
            array_push($dataset, $key->unique_key);
        }

        return $dataset;
    }
}
