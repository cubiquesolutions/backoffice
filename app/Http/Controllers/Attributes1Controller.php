<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\Attributes1Resource;

use App\Models\Attributes_1;
use App\Models\Attributes_2;
use App\Models\Attributes_3;
use App\Models\Attributes_4;
use App\Models\Attributes_5;
use App\Models\Attributes_6;
use App\Models\Attributes_7;
use App\Models\Attributes_8;

use App\Models\Resale_customer;

use Yajra\Datatables\Datatables;

class Attributes1Controller extends Controller
{
    public function index()
    {
        return view('/attributes_views/attributes');
    }

    public function getAllAttributes_1(Request $request)
    {

        return Datatables::of(Attributes_1::query())
            
            ->setRowId('id')

            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Attributes_1 $atr) {
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteAtr1Modal(' . $atr->id . ')">Delete</button> ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['actions'])
            
            ->make(true);
    }

    public function getAllAttributes_2()
    {
        return Datatables::of(Attributes_2::query())
            
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Attributes_2 $atr) {
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteAtr2Modal(' . $atr->id . ')">Delete</button> ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['actions'])

            ->toJson();
    }

    public function getAllAttributes_3()
    {
        return Datatables::of(Attributes_3::query())
            
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Attributes_3 $atr) {
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteAtr3Modal(' . $atr->id . ')">Delete</button> ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['actions'])

            ->toJson();
    }

    public function getAllAttributes_4()
    {
        return Datatables::of(Attributes_4::query())
            
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Attributes_4 $atr) {
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteAtr4Modal(' . $atr->id . ')">Delete</button> ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['actions'])

            ->toJson();
    }

    public function getAllAttributes_5()
    {
        return Datatables::of(Attributes_5::query())
            
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Attributes_5 $atr) {
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteAtr5Modal(' . $atr->id . ')">Delete</button> ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['actions'])

            ->toJson();
    }

    public function getAllAttributes_6()
    {
        return Datatables::of(Attributes_6::query())
            
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Attributes_6 $atr) {
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteAtr6Modal(' . $atr->id . ')">Delete</button> ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['actions'])

            ->toJson();
    }

    public function getAllAttributes_7()
    {
        return Datatables::of(Attributes_7::query())
            
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Attributes_7 $atr) {
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteAtr7Modal(' . $atr->id . ')">Delete</button> ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['actions'])

            ->toJson();
    }

    public function getAllAttributes_8()
    {
        return Datatables::of(Attributes_8::query())
            
            ->setRowId('id')
            ->setRowAttr([
                'align' => 'center',
            ])

            ->addColumn('actions', function (Attributes_8 $atr) {
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteAtr8Modal(' . $atr->id . ')">Delete</button> ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['actions'])

            ->toJson();
    }

    public function createAtr1(Request $request)
    {
        $this->validate(
            request(),
            [
                'Name' => 'required|min:3|max:50|unique:Attributes_1',
            ],
            [
                'Name.required' => ' The name field is required.',
                'Name.min' => ' The name must be at least 3 characters.',
                'Name.max' => ' The name may not be greater than 50 characters.',
                'Name.unique' => ' The name already exists.',
            ]
        );

        $data = $request->all();

        Attributes_1::create([
            'name' => $data['Name'],
            'description' => $data['Description']
        ]);

        return $this->index();
    }

    public function createAtr2(Request $request)
    {
        $this->validate(
            request(),
            [
                'Name' => 'required|min:3|max:50|unique:Attributes_1',
            ],
            [
                'Name.required' => ' The name field is required.',
                'Name.min' => ' The name must be at least 3 characters.',
                'Name.max' => ' The name may not be greater than 50 characters.',
                'Name.unique' => ' The name already exists.',
            ]
        );

        $data = $request->all();

        Attributes_2::create([
            'name' => $data['Name'],
            'description' => $data['Description']
        ]);

        return $this->index();
    }

    public function createAtr3(Request $request)
    {
        $this->validate(
            request(),
            [
                'Name' => 'required|min:3|max:50|unique:Attributes_1',
            ],
            [
                'Name.required' => ' The name field is required.',
                'Name.min' => ' The name must be at least 3 characters.',
                'Name.max' => ' The name may not be greater than 50 characters.',
                'Name.unique' => ' The name already exists.',
            ]
        );

        $data = $request->all();

        Attributes_3::create([
            'name' => $data['Name'],
            'description' => $data['Description']
        ]);

        return $this->index();
    }

    public function createAtr4(Request $request)
    {
        $this->validate(
            request(),
            [
                'Name' => 'required|min:3|max:50|unique:Attributes_1',
            ],
            [
                'Name.required' => ' The name field is required.',
                'Name.min' => ' The name must be at least 3 characters.',
                'Name.max' => ' The name may not be greater than 50 characters.',
                'Name.unique' => ' The name already exists.',
            ]
        );

        $data = $request->all();

        Attributes_4::create([
            'name' => $data['Name'],
            'description' => $data['Description']
        ]);

        return $this->index();
    }

    public function createAtr5(Request $request)
    {
        $this->validate(
            request(),
            [
                'Name' => 'required|min:3|max:50|unique:Attributes_1',
            ],
            [
                'Name.required' => ' The name field is required.',
                'Name.min' => ' The name must be at least 3 characters.',
                'Name.max' => ' The name may not be greater than 50 characters.',
                'Name.unique' => ' The name already exists.',
            ]
        );

        $data = $request->all();

        Attributes_5::create([
            'name' => $data['Name'],
            'description' => $data['Description']
        ]);

        return $this->index();
    }

    public function createAtr6(Request $request)
    {
        $this->validate(
            request(),
            [
                'Name' => 'required|min:3|max:50|unique:Attributes_1',
            ],
            [
                'Name.required' => ' The name field is required.',
                'Name.min' => ' The name must be at least 3 characters.',
                'Name.max' => ' The name may not be greater than 50 characters.',
                'Name.unique' => ' The name already exists.',
            ]
        );

        $data = $request->all();

        Attributes_6::create([
            'name' => $data['Name'],
            'description' => $data['Description']
        ]);

        return $this->index();
    }

    public function createAtr7(Request $request)
    {
        $this->validate(
            request(),
            [
                'Name' => 'required|min:3|max:50|unique:Attributes_1',
            ],
            [
                'Name.required' => ' The name field is required.',
                'Name.min' => ' The name must be at least 3 characters.',
                'Name.max' => ' The name may not be greater than 50 characters.',
                'Name.unique' => ' The name already exists.',
            ]
        );

        $data = $request->all();

        Attributes_7::create([
            'name' => $data['Name'],
            'description' => $data['Description']
        ]);

        return $this->index();
    }

    public function createAtr8(Request $request)
    {
        $this->validate(
            request(),
            [
                'Name' => 'required|min:3|max:50|unique:Attributes_1',
            ],
            [
                'Name.required' => ' The name field is required.',
                'Name.min' => ' The name must be at least 3 characters.',
                'Name.max' => ' The name may not be greater than 50 characters.',
                'Name.unique' => ' The name already exists.',
            ]
        );

        $data = $request->all();

        Attributes_8::create([
            'name' => $data['Name'],
            'description' => $data['Description']
        ]);

        return $this->index();
    }

    public function deleteAtr1($id)
    {

        $atr = Attributes_1::findOrFail($id);
        $atr->delete();
        //return new Attributes1Resource($atr);
        return $this->index();
    }

    public function deleteAtr2($id)
    {

        $atr = Attributes_2::findOrFail($id);
        $atr->delete();
        //return new Attributes1Resource($atr);
        return $this->index();
    }

    public function deleteAtr3($id)
    {

        $atr = Attributes_3::findOrFail($id);
        $atr->delete();
        //return new Attributes1Resource($atr);
        return $this->index();
    }

    public function deleteAtr4($id)
    {

        $atr = Attributes_4::findOrFail($id);
        $atr->delete();
        //return new Attributes1Resource($atr);
        return $this->index();
    }

    public function deleteAtr5($id)
    {

        $atr = Attributes_5::findOrFail($id);
        $atr->delete();
        //return new Attributes1Resource($atr);
        return $this->index();
    }

    public function deleteAtr6($id)
    {

        $atr = Attributes_6::findOrFail($id);
        $atr->delete();
        //return new Attributes1Resource($atr);
        return $this->index();
    }

    public function deleteAtr7($id)
    {

        $atr = Attributes_7::findOrFail($id);
        $atr->delete();
        //return new Attributes1Resource($atr);
        return $this->index();
    }

    public function deleteAtr8($id)
    {

        $atr = Attributes_8::findOrFail($id);
        $atr->delete();
        //return new Attributes1Resource($atr);
        return $this->index();
    }
}
